var gulp = require('gulp');

var plugins = {
    strip     : require('gulp-strip-debug'),
    stripCssComments : require('gulp-strip-css-comments'),
    header    : require('gulp-header'),
    size      : require('gulp-size'),
    rename    : require('gulp-rename'),
    less      : require('gulp-rename'),
    concat    : require('gulp-concat'),
    cleanCss : require('gulp-clean-css'),
    uglify    : require('gulp-uglify'),
    zip        : require('gulp-zip'),
    copy      : require('gulp-copy'),
    glob      : require('glob'),
    clean     : require('gulp-clean'),
    sourcemaps: require('gulp-sourcemaps'),
    debug: require('gulp-debug'),
    ngAnnotate: require('gulp-ng-annotate'),
    ngTemplates: require('gulp-angular-templatecache'),
    replace : require('gulp-replace'),
    lineEnding : require('gulp-line-ending-corrector')

};

var del = require('del');
var exec = require('child_process').exec;
var addStream = require('add-stream');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var fs = require('fs');

function _command (cmd, cb) {
    exec(cmd, { cwd: __dirname }, function (err, stdout, stderr) {
        if(cb)
            cb( stdout);
    } );

}

var git={
    short:  function(cb){ return _command('git rev-parse --short HEAD', cb);}
    , long:   function(cb){ return _command('git rev-parse  HEAD', cb) }
    , branch: function(cb){ return _command('git rev-parse  --abbrev-ref HEAD', cb) }
    , tag:    function(cb){ return _command('git describe --always --tag --abbrev=0', cb) }
    , msg:    function(cb){ return _command('git show -s --format=%B HEAD', cb) }
    , changes:function(cb){ return _command('git status --porcelain', cb) }
};



gulp.task('theme', function(){
    "use strict";

    gulp.src([
        'plugin/**/*'
    ])
        .pipe(gulp.dest('wordpress/wp-content/plugins/'));

    gulp.src([
        'theme/**/*'
    ])
        .pipe(gulp.dest('wordpress/wp-content/themes/hypno/'));

});



gulp.task('js', function(){
    "use strict";
    gulp.src([
        'non_bower/preloader.js',
        'bower_components/bootstrap/dist/js/bootstrap.js',
        'bower_components/wow/dist/wow.js',
        'non_bower/waypoints.js',
        'non_bower/jquery.easypiechart.js',
        'non_bower/imagesloaded.pkgd.min.js',
        'non_bower/masonry.pkgd.min.js',
        'bower_components/jquery-prettyPhoto/js/jquery.prettyPhoto.js',
        'non_bower/owl.carousel.js',
        'bower_components/flexslider/jquery.flexslider.js',
        'bower_components/fitvids/jquery.fitvids.js',
        'theme/js/*.js',
        '!theme/js/admin.js'
    ])
        .pipe(plugins.lineEnding({verbose:true, eolc: 'LF', encoding:'utf8'}))
        .pipe(gulp.dest('theme/dev/js/'))
        .pipe(plugins.concat('scripts.js'))
        .pipe(gulp.dest('theme/build/js/'))
        .pipe(plugins.strip())
        .pipe(plugins.uglify())
        .pipe(plugins.concat('scripts.min.js'))
        .pipe(gulp.dest('theme/build/js/'))

    gulp.src([
        'theme/js/admin.js'
    ])
        .pipe(plugins.lineEnding({verbose:true, eolc: 'LF', encoding:'utf8'}))
        .pipe(plugins.strip())
        .pipe(plugins.uglify())
        .pipe(gulp.dest('theme/build/js/'))
})


gulp.task('fonts', function(){
    "use strict";

    gulp.src([
        'bower_components/font-awesome/fonts/*',
        'bower_components/flexslider/fonts/*',
    ])
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest('theme/build/fonts/'))
        .pipe(gulp.dest('theme/dev/fonts/'))

});


gulp.task('images', function(){
    "use strict";

    gulp.src([
        'bower_components/jquery-prettyPhoto/images/prettyPhoto/**/*',
        'bower_components/jquery-prettyPhoto/images/prettyPhoto/**/*',
    ])
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest('theme/build/images/prettyPhoto/'))

});



gulp.task('css', function(){
    "use strict";

    gulp.src([
        'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'bower_components/animate.css/animate.css',
        'non_bower/owl.carousel.css',
        'bower_components/jquery-prettyPhoto/css/prettyPhoto.css',
        'bower_components/flexslider/flexslider.css',
    ])
        .pipe(plugins.lineEnding({verbose:true, eolc: 'LF', encoding:'utf8'}))
        .pipe(gulp.dest('theme/dev/css/'))

    gulp.src([
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'non_bower/owl.carousel.css',
        'bower_components/font-awesome/css/font-awesome.css',
        'bower_components/animate.css/animate.css',
        'bower_components/jquery-prettyPhoto/css/prettyPhoto.css',
        'bower_components/flexslider/flexslider.css',
    ])
        .pipe(plugins.lineEnding({verbose:true, eolc: 'LF', encoding:'utf8'}))
        .pipe(plugins.stripCssComments())
        .pipe(plugins.concat("style.css"))
        .pipe(plugins.stripCssComments())
        .pipe(gulp.dest('theme/build/css/'))

        .pipe(plugins.cleanCss({keepSpecialComments : 0}))
        .pipe(plugins.stripCssComments())
        .pipe(plugins.rename({extname:'.min.css'}))
        .pipe(gulp.dest('theme/build/css/'))


    gulp.src([
        'theme/less/style.css',
    ])
        .pipe(plugins.lineEnding({verbose:true, eolc: 'LF', encoding:'utf8'}))
        .pipe(plugins.concat("theme.css"))
        .pipe(gulp.dest('theme/build/css/'))
        .pipe(plugins.stripCssComments())
        .pipe(plugins.cleanCss())
        .pipe(plugins.stripCssComments())
        .pipe(plugins.cleanCss({keepSpecialComments : 0}))
        .pipe(plugins.stripCssComments())
        .pipe(plugins.rename({extname:'.min.css'}))
        .pipe(gulp.dest('theme/build/css/'))
});


gulp.task('build',['css', 'fonts', 'js', 'images'], function(){
    "use strict";
});

gulp.task('make',['theme'], function(){
    "use strict";
});