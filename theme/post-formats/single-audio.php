<?php
if (get_post_meta(get_the_ID(), 'synergy_audio_code', true) != "") {
    echo '<div class="post-feature padding-bottom-20 fitvid">';
    echo get_post_meta(get_the_ID(), 'synergy_audio_code', true);
    echo '</div>';
}
?>