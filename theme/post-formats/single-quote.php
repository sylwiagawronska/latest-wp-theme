<?php
if (get_post_meta(get_the_ID(), 'synergy_quote_content', true) != "") {
    echo '<div class="post-feature padding-bottom-20">';
    echo '<blockquote class="wow fadeIn dark-border colored-bg">';
    echo '<p>' . (get_post_meta(get_the_ID(), 'synergy_quote_content', true)) . '</p>';
        if (get_post_meta(get_the_ID(), 'synergy_quote_author', true) != "") {
            echo '<footer>' . (get_post_meta(get_the_ID(), 'synergy_quote_author', true)) . '</footer>';
        }
    echo '</blockquote>';
    echo '</div>';
}
?>