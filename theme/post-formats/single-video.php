<?php
if ((get_post_meta(get_the_ID(), 'synergy_video_type', true) == 'id') && (get_post_meta(get_the_ID(), 'synergy_video_id', true) != "")) {
    echo '<div class="post-feature padding-bottom-20 fitvid">';
    echo '<iframe class="wow fadeIn" width="1280" height="720" src="https://www.youtube.com/embed/' . get_post_meta(get_the_ID(), 'synergy_video_id', true) . '?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
    echo '</div>';
}  else if ((get_post_meta(get_the_ID(), 'synergy_video_type', true) == 'code') && (get_post_meta(get_the_ID(), 'synergy_video_code', true) != "")) {
    echo '<div class="post-feature padding-bottom-20 fitvid">';
    echo get_post_meta(get_the_ID(), 'synergy_video_code', true);
    echo '</div>';
}
?>