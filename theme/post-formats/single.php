<?php if ( has_post_thumbnail() ) {
    ?>
    <div class="post-feature padding-bottom-20">
        <a data-rel="prettyPhoto" href="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post -> ID)); ?>" title="<?php esc_attr(the_title()); ?>">
            <?php the_post_thumbnail('full', array( 'class' => "imagepost wow fadeIn")); ?>
        </a>
    </div>
<?php }?>