/**
 * Load more button handler
 * @param $
 * @param loadMoreContainer
 * @param loadMoreButton
 * @constructor
 */
function LoadMore($, loadMoreContainer, loadMoreButton, portfolioLoadMore) {
    "use strict";

    var loading = {

        hideLoadMoreButton : function(){
            $(portfolioLoadMore).fadeOut('slow');
        },

        hookHandlers : function(){

            $(loadMoreContainer).each(function(){

                var me = this;
                var initialItems = $(this).attr('data-initial');
                var increment = $(this).attr('data-increment');

                $(this).children().hide();
                $(this).children().slice(0, initialItems).css('display', 'inline-block');

                if ($(me).children(':hidden').length == 0) {
                    $(loadMoreButton).css('display', 'none');
                }

                $(loadMoreButton).on('click', function (e) {
                    e.preventDefault();
                    $(me).children(':hidden').slice(0, increment).css('display', 'inline-block').slideDown();
                    if ($(me).children(':hidden').length == 0) {
                        $(loadMoreButton).fadeOut('slow');
                    }
                    $('html,body').animate({
                        scrollTop: $(this).offset().top
                    }, 1500);
                    $(document).trigger('loadMore:activated');
                });


            })

        }


    }


    loading.hookHandlers();

    $(document).on('portfolio:filtered',function(){
        loading.hideLoadMoreButton();
    })


}