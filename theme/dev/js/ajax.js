/**
 * Ajax loading for portfolio
 * @param $
 * @param ajaxLoaders
 * @param ajaxContainer
 * @param ajaxPortfolioItem
 * @constructor
 */
function AjaxLoading($, ajaxLoaders, ajaxContainer, ajaxPortfolioItem) {

    var ajax = {

        load: function(link){

            var me = this;

            var post_link = $(link).attr("href") + " " + ajaxPortfolioItem;

            $(ajaxContainer).load(post_link ,function(responseTxt, statusTxt, xhr){
                if (statusTxt == "success"){
                    $(document).trigger('portfolioItem:loaded');
                    me.hookHandlers();
                } else {
                    console.log("Error: Ajax loading will not work locally. Please upload the page on a hosting account");
                }
            })

        },

        hookHandlers: function() {

            var me=this;


            $(ajaxLoaders.next).off('click').on('click', function (event) {
                // Avoid following the href location when clicking
                event.preventDefault();
                event.stopPropagation();
                // load item
                ajax.load($(this));
            });

            $(ajaxLoaders.prev).off('click').on('click', function (event) {
                // Avoid following the href location when clicking
                event.preventDefault();
                event.stopPropagation();
                // load item
                ajax.load($(this));
            });

            $(ajaxLoaders.load).off('click').on('click', function (event) {
                // Avoid following the href location when clicking
                event.preventDefault();
                event.stopPropagation();
                // load item
                ajax.load($(this));
            });

            $(ajaxLoaders.exit).off('click').on('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                // close item
                me.clear();
            });
        },

        clear : function(){
            $(ajaxContainer).html("");
        }



    }

    ajax.hookHandlers();

    $(document).on('portfolio:filtered',function(){
        ajax.clear();
    })





}