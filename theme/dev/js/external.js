/**
 * External scripts handler
 * @param $
 * @param flexSliderContainer
 * @param fitVidContainer
 * @param masonryGrid
 * @param masonryItem
 * @constructor
 */
function ExternalScripts($, flexSliderContainer, fitVidContainer, masonryGrid, masonryItem) {

    $.ajax({
        cache: false,
        error: function () {
        }
    });



    var masonry = {

        activateMasonry : function() {

            var $grid = $(masonryGrid).imagesLoaded( function() {
                $grid.masonry({
                    columnWidth: masonryItem,
                    itemSelector: masonryItem,
                    gutter: 0
                });
            });
        }

    }

    var prettyPhoto = {

        hookHandlers : function() {
            // initialize pretty photo
            $("a[data-rel^='prettyPhoto']").prettyPhoto();
            $('a[data-rel]').each(function() {
                $(this).attr('rel', $(this).data('rel'));
            });
        }
    };

    var flexSlider = {

        hookHandlers : function() {
            $(flexSliderContainer).flexslider({
                animation : "fade",
                slideDirection : "horizontal",
                slideshow : true,
                slideshowSpeed : 3500,
                animationDuration : 500,
                directionNav : true,
                controlNav : false
            });
        }
    };

    var fitVid = {

        hookHandlers : function() {
            $(fitVidContainer).fitVids();
        }
    };

    var toolTip = {
        hookHandlers : function() {
            $('[data-toggle="tooltip"]').tooltip();
        }
    };

    var woo_commerce = {
        moveStarRaring : function(){
            $(".products .star-rating").each(function(){
                var stars = $(this);
                var price = stars.siblings('.price');
                price.remove();
                price.insertBefore(stars);
            });
        },

        appendOverlay : function() {
            $(".woo-imagewrapper").each(function () {
                var add = $(this).parent().siblings().attr('href');
                var link = $(this).parents().attr('href');

                var overlay = '<div class="woo-overlay"><a href="'+link+'"><i class="fa fa-link"></i></a>'
                        + '<a rel="nofollow" href="'+add+'" data-quantity="1" data-product_id="'+add.substr(add.lastIndexOf("=")+1)+'" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart"><i class="fa fa-shopping-bag"></i></a></div>';

                $(this).append($(overlay));

                $(this).parent().siblings().remove();
            });
        },
        addSearchIcon : function() {
            $(".widget_product_search input[type='submit']").each(function () {
                $(this).val("");
            });
        }
    };


    prettyPhoto.hookHandlers();
    flexSlider.hookHandlers();
    fitVid.hookHandlers();
    toolTip.hookHandlers();
    woo_commerce.moveStarRaring();
    woo_commerce.appendOverlay();
    woo_commerce.addSearchIcon();
    masonry.activateMasonry();

    $(document).on('portfolioItem:loaded',function(){
        prettyPhoto.hookHandlers();
        flexSlider.hookHandlers();
        fitVid.hookHandlers();
    });

    $(document).on('loadMore:activated',function(){
        masonry.activateMasonry();
    });

    $(document).on('preloader:finished',function(){
        $('.preloader').fadeOut();
    });



}