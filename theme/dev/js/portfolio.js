/**
 * Portfolio items filtering
 * @param $
 * @param portfolioValues
 * @param portfolioContainer
 * @param portfolioStandalone
 * @constructor
 */
function Portfolio($, portfolioValues, portfolioContainer, portfolioStandalone) {
    "use strict";

    var portfolio = {

        filter: function (filterValue) {

            $(document).trigger('portfolio:filtered');

            var me = this;
            var previous = me.state.active;

            // reset filters if reset button is clicked
            if (filterValue == me.state.reset) {
                me.reset();
                return;
            // if the filter is already active, do nothing
            } else if (filterValue == previous) {
                return;
            }

            //fadeout
            $(portfolioContainer).add(portfolioStandalone).fadeOut("slow", function () {
                // filter
                $(portfolioValues.portfolioItem).each(function () {
                    me.filterItems(this, filterValue);
                });
            });

            //fadein
            $(portfolioContainer).add(portfolioStandalone).fadeIn("slow");

            // set active filter
            me.state.active = filterValue;

            // set active button
            me.deactivate(previous);
            me.activate(filterValue)
            // reset portfolio carousel
            $(portfolioValues.portfolioCarousel).trigger('owl.goTo', 0)
        },

        filterItems: function (self, filterValue) {

            if ($(self).attr('data-filter')) {
                // show items we want to display
                if ($(self).attr('data-filter').indexOf(filterValue) !== -1) {
                        $(self).css('display', 'block');
                }
                // hide items we want to filter out
                else {
                        $(self).css('display', 'none');
                }
            }
        },

        reset: function () {
            //fadeout
            $(portfolioContainer).add(portfolioStandalone).fadeOut("slow", function () {
                // display all items
                $(portfolioValues.portfolioItem).each(function () {
                    $(this).css('display', 'block');
                });
            });
            //fadein
            $(portfolioContainer).add(portfolioStandalone).fadeIn("slow");

            // set active button
            this.deactivate(this.state.active);
            this.activate(this.state.reset);

            this.state.active = this.state.reset;

        },

        activate: function(filter) {
            $(portfolioValues.portfolioFilter+"[data-filter-by*='"+filter+"']").addClass('active-filter');
        },

        deactivate: function(filter) {
            $(portfolioValues.portfolioFilter+"[data-filter-by*='"+filter+"']").removeClass('active-filter');
        },

        state: {
            reset: "filter-all",
            active: "filter-all"
        }


    }

    $(portfolioValues.portfolioFilter).on('click', function (event) {
        var filter = $(this).attr('data-filter-by');
        // Avoid following the href location when clicking
        event.preventDefault();
        event.stopPropagation();
        // filter items
        portfolio.filter(filter);


    });

   // return portfolio;
}