/**
 * Navigation handler
 * @param $
 * @param dropdownDesktop
 * @param toggleDropdown
 * @constructor
 */
function Navigation($, dropdownDesktop, toggleDropdown) {
    "use strict";

    /**
     * Handle 2nd and 3rd level desktop navigation
     */
    var dropdown = $(dropdownDesktop);

    $(dropdown).hover(
        function() {
            $(this).parent().css('overflow','visible');
        },
        function() {
            $(this).parent().css('overflow','hidden');
        }
    );


    /**
     * Handle 2nd and 3rd level mobile navigation
     */
    var dropdowns = {


        hookHandlers : function() {
            $('.toggleDropdown').on('click', function(event) {
                var parent = $(this).parent();
                // Avoid following the href location when clicking
                event.preventDefault();
                // Avoid having the menu to close when clicking
                event.stopPropagation();
                // If a menu is already open we close it, else we open
                ($(parent).hasClass('open'))?($(parent).removeClass('open')):($(parent).addClass('open'));
            });
        }
    }

    dropdowns.hookHandlers();

}
