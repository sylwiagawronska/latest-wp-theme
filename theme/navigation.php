<!--navigation start-->
<?php
    global $hypno_options;

   $navigation_position = "";
    if ( is_admin_bar_showing() ) {
        // do something
        $navigation_position = "admin-bar-showing";
    }
?>
<nav class="navbar navbar-fixed-top sticky <?php echo $navigation_position ?>" role="navigation">

    <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <?php
        if ((array_key_exists('logo', $hypno_options) && $hypno_options['logo'])) {
            echo '<a href="' . esc_url(home_url()) . '" id="logo">' .
                wp_get_attachment_image( $hypno_options['logo']['id'], 'full' ) .
                '</a>';
        }
        ?>
    </div>

    <?php
        if (((array_key_exists('upper-block', $hypno_options) && array_key_exists('lower-block', $hypno_options)))
        && ($hypno_options['upper-block'] || $hypno_options['lower-block'])) {
            ?>
            <div class="navbar-header navheader">

                <?php
                if ($hypno_options['upper-block']) {
                    echo '<h5 class="upper-name block">' .
                        $hypno_options['upper-block'] .
                        '</h5>';
                }
                if ($hypno_options['lower-block']) {
                    echo '<span class="footnote block">' .
                        $hypno_options['lower-block'] .
                        '</span>';
                }
                ?>

            </div>

        <?php
        }
    ?>

    <?php if (((array_key_exists('enable-header-social-bar', $hypno_options)) && $hypno_options['enable-header-social-bar'] == 1)){
        if (array_key_exists('social-header', $hypno_options)) {
            echo '<div class="navbar-panel social-header"><div id="social-bar">';
            foreach ($hypno_options['social-header'] as &$value) {
                if ((array_key_exists($value, $hypno_options)) && $hypno_options[$value] !== "") {
                    echo '<a href="'.$hypno_options[$value].'"><i class="fa '.$value.'"></i></a>';
                }
            }
            echo '</div></div>';
        }



    } ?>
    <?php if ((array_key_exists('woocommerce-header-cart', $hypno_options)) && $hypno_options['woocommerce-header-cart'] == 1 && in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
        <div class="navbar-panel cart-header">
            <a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart', 'hypno' ); ?>"><i class="fa  fa-shopping-bag"></i>[ <?php echo WC()->cart->get_cart_contents_count()?> ] - <?php echo WC()->cart->get_cart_total(); ?></a>
        </div>
    <?php } ?>


    <div class="navbar-collapse collapse" id="navbar">

        <?php
        if (has_nav_menu( 'primary-menu' )) {

            // if primary menu is configured and megamenu is enabled
            if ( function_exists('max_mega_menu_is_enabled') && max_mega_menu_is_enabled('primary-menu') ) { ?>
                <ul class="nav navbar-nav navbar-right">
                    <li id="synergy-navigation">
                        <?php
                        wp_nav_menu(array('container' => '', 'menu_id' => 'primary-menu', 'menu_class' => 'nav navbar-nav navbar-right', 'theme_location' => 'primary-menu'));
                        ?>
                    </li>
                </ul>
            <?php
            }
            // if primary menu is configured and megamenu is disabled
            else {
                wp_nav_menu(array('container' => '', 'menu_id' => 'primary-menu', 'menu_class' => 'nav navbar-nav navbar-right', 'theme_location' => 'primary-menu', 'walker' => new Synergy_Menu_Walker()));
            }

        } else {
            // if no primary menu is configured
            echo '<ul class="nav navbar-nav navbar-right"><li><a href="'.esc_url(home_url()).'/wp-admin/nav-menus.php">Configure Menu</a></li></ul>';
        }
        ?>

    </div>

</nav>
<!-- navigaqtion end-->