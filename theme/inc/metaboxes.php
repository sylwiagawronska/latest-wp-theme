<?php


$prefix = 'synergy_';

global $synergy_meta_boxes;

$synergy_meta_boxes = array();

global $hypno_options;

/**
 * Get first level navigation items for "active menu item" option
 * @return array
 */
function getMenuItems(){
    $menu_name = 'primary-menu';
    $menu_array = array();
    if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

        $menu_items = wp_get_nav_menu_items($menu->term_id);

        foreach ( (array) $menu_items as $key => $menu_item ) {
            if ($menu_item->menu_item_parent != 0 ) continue;
            $title = $menu_item->title;
            $id = $menu_item->ID;
            $menu_array[$id] = $title;
        }
    }
    return $menu_array;
}



/* ----------------------------------------------------- */
// PORTFOLIO SETTINGS
/* ----------------------------------------------------- */

$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'page-portfolio',
    'title'		=> __('Portfolio Settings', 'hypno' ),
    'pages'		=> array( 'page' ),
    'context' => 'normal',


    'fields' => array(

        array(
            'name'		=> __('Active Menu Item', 'hypno' ),
            'id'		=> $prefix . 'portfolio_active_menu_item',
            'type' => 'select',
            'options' => getMenuItems(),
            'clone'		=> false,
            'multiple'	=> false
        ),

        array(
            'name'		=> __('Portfolio Display Type', 'hypno' ),
            'id'		=> $prefix . "portfolio_type",
            'clone'		=> false,
            'type'		=> 'select',
            'options'	=> array(
                'images_up'		=> __('Images up, content down', 'hypno' ),
                'images_down'		=> __('Content up, images down', 'hypno' ),
                'mixed'		=> __('Mixed', 'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'		=> __('Alternate portfolio page title?', 'hypno' ),
            'id'		=> $prefix . "portfolio_enable_alternative_title",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Alternative portfolio page title', 'hypno' ),
            'id'		=> $prefix . 'portfolio_alternative_title',
            'desc'		=> __('HTML is allowed. Box above must be set to "yes" in order to enable the alternative title', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Portfolio <span class="medium-thin-font">page</span>'
        ),

        array(
            'name'		=> __('All items filter text', 'hypno' ),
            'id'		=> $prefix . 'portfolio_show_all',
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Show all'
        ),

        array(
            'name'	=> __('Page Background Color', 'hypno' ),
            'id'	=> $prefix . 'portfolio_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),

        array(
            'name'		=> __('Enable load more?', 'hypno' ),
            'id'		=> $prefix . "portfolio_enable_load_more",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 1,
        ),

        array(
            'name'	=> __('Items to show initially', 'hypno' ),
            'id'	=> $prefix . 'portfolio_initial_number',
            'desc'		=> __('How many items to show on initial load. Suggested values are 6 or 9 (if enable load more option is checked)', 'hypno' ),
            'type'	=> 'number',
            'std'		=> 6
        ),

        array(
            'name'	=> __('Items to add on load more', 'hypno' ),
            'id'	=> $prefix . 'portfolio_increment_number',
            'desc'		=> __('How many items to add to display on load more button click. Suggested value is 3 (if enable load more option is checked)', 'hypno' ),
            'type'	=> 'number',
            'std'		=> 6
        ),

        array(
            'name'		=> __('Load more button text', 'hypno' ),
            'id'		=> $prefix . 'portfolio_load_more',
            'desc'		=> __('If enable load more option is checked', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Load more'
        ),

    )
);


/* ----------------------------------------------------- */
// BLOG SETTINGS
/* ----------------------------------------------------- */

$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'page-blog',
    'title'		=> __('Blog Settings', 'hypno' ),
    'pages'		=> array( 'page' ),
    'context' => 'normal',


    'fields' => array(

        array(
            'name'		=> __('Blog Display Type', 'hypno' ),
            'id'		=> $prefix . "blog_type",
            'clone'		=> false,
            'type'		=> 'select',
            'options'	=> array(
                'sidebar'		=> __('Standard view with sidebar', 'hypno' ),
                'full'		=> __('Standard view full width', 'hypno' ),
                'masonry_four'		=> __('Masonry view - four columns', 'hypno' ),
                'masonry_three'		=> __('Masonry view - three columns', 'hypno' ),
                'masonry_two'		=> __('Masonry view - two columns', 'hypno' ),
                'masonry_three_feat'		=> __('Masonry view - three columns with features', 'hypno' ),
                'masonry_two_feat'		=> __('Masonry view - two columns with features', 'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'		=> __('Active Menu Item', 'hypno' ),
            'id'		=> $prefix . 'blog_active_menu_item',
            'type' => 'select',
            'options' => getMenuItems(),
            'clone'		=> false,
            'multiple'	=> false
        ),

        array(
            'name'		=> __('Alternate page title?', 'hypno' ),
            'id'		=> $prefix . "blog_enable_alternative_title",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Alternative page title', 'hypno' ),
            'id'		=> $prefix . 'blog_alternative_title',
            'desc'		=> __('HTML is allowed. Box above must be set to "yes" in order to enable the alternative title', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'This is <span class="medium-thin-font">blog</span> page'
        ),


        array(
            'name'		=> __('Include Full Width Slider', 'hypno' ),
            'id'		=> $prefix . "blog_fw_slider",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Fullwidth Slider Alias (optional)', 'hypno' ),
            'id'		=> $prefix . 'blog_slider_shortcode',
            'desc'		=> __('Alias is a part of rev_slider shortcode. Eg. alias of [rev_slider alias="hypno-slider"] is hypno-slider.', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'hypno-slider'
        ),


        array(
            'name'	=> __('Page Background Color', 'hypno' ),
            'id'	=> $prefix . 'blog_bg_color',
            'type'	=> 'color',
            'std'		=> '#f8f8f8'
        ),

        array(
            'name'	=> __('Masonry item Background Color', 'hypno' ),
            'id'	=> $prefix . 'blog_masonry_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),

        array(
            'name'		=> __('Page Header Type', 'hypno' ),
            'id'		=> $prefix . "blog_header_type",
            'clone'		=> false,
            'type'		=> 'select',
            'options'	=> array(
                'color'		=> __('Color', 'hypno' ),
                'image'		=> __('Image', 'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'	=> __('Header Background Image (Optional)', 'hypno' ),
            'id'	=> $prefix . 'blog_header_image',
            'type'	=> 'plupload_image',
            'max_file_uploads' => 1,
            'std'		=> ''
        ),

        array(
            'name'	=> __('Header Background Color', 'hypno' ),
            'id'	=> $prefix . 'blog_header_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),
        array(
            'name'	=> __('Heading Font Color', 'hypno' ),
            'id'	=> $prefix . 'blog_heading_font_color',
            'type'	=> 'color',
            'std'		=> '#000000'
        ),

        array(
            'name'		=> __('Enable breadcrums', 'hypno' ),
            'id'		=> $prefix . "blog_breadcrums",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Breadcrums Home label', 'hypno' ),
            'id'		=> $prefix . 'blog_breadcrums_home',
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Home'
        ),

        array(
            'name'	=> __('Breadcrums Color #1', 'hypno' ),
            'id'	=> $prefix . 'blog_breadcrums1_color',
            'type'	=> 'color',
            'std'		=> '#4f4f4f'
        ),

        array(
            'name'	=> __('Breadcrums Color #2', 'hypno' ),
            'id'	=> $prefix . 'blog_breadcrums2_color',
            'type'	=> 'color',
            'std'		=> '#777777'
        ),

        array(
            'name'		=> __('Display breadcrums border?', 'hypno' ),
            'id'		=> $prefix . "blog_breadcrums_border",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 1,
        ),



    )
);




/* ----------------------------------------------------- */
// PAGE SETTINGS
/* ----------------------------------------------------- */

$synergy_meta_boxes[] = array(
    'id' => $prefix . 'page-def',
    'title' => __('Page Settings', 'hypno' ),
    'pages' => array( 'page' ),
    'context' => 'normal',
    'priority' => 'high',

    'fields' => array(

        array(
            'name'		=> __('Active Menu Item', 'hypno' ),
            'id'		=> $prefix . 'page_active_menu_item',
            'type' => 'select',
            'options' => getMenuItems(),
            'clone'		=> false,
            'multiple'	=> false
        ),
        array(
            'name'		=> __('Include sidebar?', 'hypno' ),
            'id'		=> $prefix . "page_sidebar",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Page Sidebar', 'hypno' ),
            'id'		=> $prefix . 'page_sidebar_id',
            'type'		=> 'select',
            'options'	=> array(
                'page-sidebar'		=> __('Page sidebar', 'hypno' ),
                'store-sidebar'		=> __('Store sidebar', 'hypno' ),
            ),
        ),

        array(
            'name'		=> __('Alternate page title?', 'hypno' ),
            'id'		=> $prefix . "page_enable_alternative_title",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Alternative page title', 'hypno' ),
            'id'		=> $prefix . 'page_alternative_title',
            'desc'		=> __('HTML is allowed. Box above must be set to "yes" in order to enable the alternative title', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'This is <span class="medium-thin-font">about us</span> page'
        ),


        array(
            'name'		=> __('Include Full Width Slider', 'hypno' ),
            'id'		=> $prefix . "page_fw_slider",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Fullwidth Slider Alias (optional)', 'hypno' ),
            'id'		=> $prefix . 'page_slider_shortcode',
            'desc'		=> __('Alias is a part of rev_slider shortcode. Eg. alias of [rev_slider alias="hypno-slider"] is hypno-slider.', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'hypno-slider'
        ),


        array(
            'name'	=> __('Page Background Color', 'hypno' ),
            'id'	=> $prefix . 'page_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),

        array(
            'name'		=> __('Page Header Type', 'hypno' ),
            'id'		=> $prefix . "page_header_type",
            'clone'		=> false,
            'type'		=> 'select',
            'options'	=> array(
                'color'		=> __('Color', 'hypno' ),
                'image'		=> __('Image', 'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'		=> __('Make header smaller', 'hypno' ),
            'id'		=> $prefix . "page_small_header",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'	=> __('Header Background Image (Optional)', 'hypno' ),
            'id'	=> $prefix . 'page_header_image',
            'type'	=> 'plupload_image',
            'max_file_uploads' => 1,
            'std'		=> ''
        ),

        array(
            'name'	=> __('Header Background Color', 'hypno' ),
            'id'	=> $prefix . 'page_header_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),
        array(
            'name'	=> __('Heading Font Color', 'hypno' ),
            'id'	=> $prefix . 'page_heading_font_color',
            'type'	=> 'color',
            'std'		=> '#000000'
        ),

        array(
            'name'		=> __('Enable breadcrums', 'hypno' ),
            'id'		=> $prefix . "page_breadcrums",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Breadcrums Home label', 'hypno' ),
            'id'		=> $prefix . 'page_breadcrums_home',
            'clone'		=> false,
            'desc'		=> __('Home label for Woocommerce can only be customized via Hypno Options > Woocommerce panel', 'hypno' ),
            'type'		=> 'text',
            'std'		=> 'Home'
        ),

        array(
            'name'	=> __('Breadcrums Color #1', 'hypno' ),
            'id'	=> $prefix . 'page_breadcrums1_color',
            'desc'		=> __('Breadcrums colors for Woocommerce can only be customized via Hypno Options > Woocommerce panel', 'hypno' ),
            'type'	=> 'color',
            'std'		=> '#4f4f4f'
        ),

        array(
            'name'	=> __('Breadcrums Color #2', 'hypno' ),
            'id'	=> $prefix . 'page_breadcrums2_color',
            'desc'		=> __('Breadcrums colors for Woocommerce can only be customized via Hypno Options > Woocommerce panel', 'hypno' ),
            'type'	=> 'color',
            'std'		=> '#777777'
        ),

        array(
            'name'		=> __('Display breadcrums border?', 'hypno' ),
            'id'		=> $prefix . "page_breadcrums_border",
            'desc'		=> __('Breadcrums colors for Woocommerce can only be customized via Hypno Options > Woocommerce panel', 'hypno' ),
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),



    )
);





/* ----------------------------------------------------- */
// POST SETTINGS
/* ----------------------------------------------------- */


/*  FEATURED SLIDER */
$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'blog-gallery',
    'title'		=> 'Featured Slider Settings',
    'pages'		=> array( 'post' ),
    'context' => 'normal',


    'fields'	=> array(

        array(
            'name'	=> 'Slider Imaes',
            'id'	=> $prefix . 'featured_slides',
            'type'	=> 'plupload_image',
            'max_file_uploads' => 10,
        )

    )
);

/*  FEATURED QUOTE */
$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'blog-quote',
    'title'		=> __('Featured Quote Settings', 'hypno' ),
    'pages'		=> array( 'post' ),
    'context' => 'normal',


    'fields'	=> array(

        array(
            'name'		=> __('Quote Content', 'hypno' ),
            'id'		=> $prefix . "quote_content",
            'clone'		=> false,
            'type'		=> 'textarea',
            'std'		=> '',
        ),
        array(
            'name'		=> __('Quote Author', 'hypno' ),
            'id'		=> $prefix . "quote_author",
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> '',
        ),
    )
);

/*  FEATURED AUDIO */
$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'blog-audio',
    'title'		=> __('Featured Audio Settings', 'hypno' ),
    'pages'		=> array( 'post' ),
    'context' => 'normal',


    'fields'	=> array(


        array(
            'name'		=> __('Embeded code (iframe)', 'hypno' ),
            'id'		=> $prefix . "audio_code",
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> '',
            'desc'		=> __('Provide iframe code', 'hypno' ),
        ),

    )
);


/*  FEATURED VIDEO */
$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'blog-video',
    'title'		=> __('Featured Video Settings', 'hypno' ),
    'pages'		=> array( 'post' ),
    'context' => 'normal',


    'fields'	=> array(

        array(
            'name'		=> __('Type', 'hypno' ),
            'id'		=> $prefix . "video_type",
            'clone'		=> false,
            'type'		=> 'select',
            'desc'		=> __('Select id, if you want to provide youtube ID (e.g. https://www.youtube.com/watch?v=<strong>Jn6-TItCazo</strong>) or Embeded code, if you want to provide full <strong>iframe code</strong>', 'hypno' ),
            'options'	=> array(
                'id'		=> __('ID', 'hypno' ),
                'code'		=> __('Embeded Code', 'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'		=> __('ID', 'hypno' ),
            'id'		=> $prefix . "video_id",
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Jn6-TItCazo',
            'desc'		=> __('Provide youtube video id (if you have selected "id" option above)', 'hypno' ),
        ),

        array(
            'name'		=> __('Embeded code (iframe)', 'hypno' ),
            'id'		=> $prefix . "video_code",
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> '',
            'desc'		=> __('Provide iframe code (if you have selected "Embeded code" option above)', 'hypno' ),
        ),

    )
);


/* ----------------------------------------------------- */
/* COMMON POST SETTINGS
/* ----------------------------------------------------- */

$synergy_meta_boxes[] = array(
    'id' => 'post_settings',
    'title' => __('Post Settings', 'hypno' ),
    'pages' => array( 'post' ),
    'context' => 'normal',
    'priority' => 'high',

    'fields' => array(

        array(
            'name'		=>__('Active Menu Item', 'hypno' ),
            'id'		=> $prefix . 'active_menu_item',
            'type' => 'select',
            'options' => getMenuItems(),
            'clone'		=> false,
            'multiple'	=> false
        ),

        array(
            'name'		=> __('Display post header?', 'hypno' ),
            'id'		=> $prefix . "post_header",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Alternate post title in page header?', 'hypno' ),
            'id'		=> $prefix . "enable_alternative_title",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Alternative post title (page header)', 'hypno' ),
            'id'		=> $prefix . 'alternative_title',
            'desc'		=> __('HTML is allowed. Box above must be set to "yes" in order to enable the alternative title', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'This is <span class="medium-thin-font">about us</span> page'
        ),

        array(
            'name'		=> __('Display sidebar?', 'hypno' ),
            'id'		=> $prefix . "sidebar",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 1,
        ),

        array(
            'name'		=> __('Enable comments?', 'hypno' ),
            'id'		=> $prefix . "enable_comments",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 1,
        ),

        array(
            'name'		=> __('Include Full Width Slider', 'hypno' ),
            'id'		=> $prefix . "fw_slider",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Fullwidth Slider Alias (optional)', 'hypno' ),
            'id'		=> $prefix . 'slider_shortcode',
            'desc'		=> __('Alias is a part of rev_slider shortcode. Eg. alias of [rev_slider alias="hypno-slider"] is hypno-slider.', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'hypno-slider'
        ),



        array(
            'name'		=> __('Page Header Type', 'hypno' ),
            'id'		=> $prefix . "header_type",
            'clone'		=> false,
            'type'		=> 'select',
            'options'	=> array(
                'color'		=> __('Color', 'hypno' ),
                'image'		=> __('Image', 'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'	=> __('Header Background Image (Optional)', 'hypno' ),
            'id'	=> $prefix . 'header_image',
            'type'	=> 'plupload_image',
            'max_file_uploads' => 1,
            'std'		=> ''
        ),

        array(
            'name'	=> __('Header Background Color', 'hypno' ),
            'id'	=> $prefix . 'header_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),
        array(
            'name'	=> __('Heading Font Color', 'hypno' ),
            'id'	=> $prefix . 'heading_font_color',
            'type'	=> 'color',
            'std'		=> '#000000'
        ),

        array(
            'name'		=> __('Enable breadcrums', 'hypno' ),
            'id'		=> $prefix . "breadcrums",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Breadcrums Home label', 'hypno' ),
            'id'		=> $prefix . 'breadcrums_home',
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Home'
        ),

        array(
            'name'	=> __('Breadcrums Color #1', 'hypno' ),
            'id'	=> $prefix . 'breadcrums1_color',
            'type'	=> 'color',
            'std'		=> '#4f4f4f'
        ),

        array(
            'name'	=> __('Breadcrums Color #2', 'hypno' ),
            'id'	=> $prefix . 'breadcrums2_color',
            'type'	=> 'color',
            'std'		=> '#777777'
        ),

        array(
            'name'		=> __('Display breadcrums border?', 'hypno' ),
            'id'		=> $prefix . "breadcrums_border",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 1,
        ),



    )
);







/* ----------------------------------------------------- */
/* PORTFOLIO ITEM SETTINGS
/* ----------------------------------------------------- */

// Portfolio Main Item settings

$synergy_meta_boxes[] = array(
    'id' => 'portfolio_single',
    'title' => __('Portfolio Item Options', 'hypno' ),
    'pages' => array( 'portfolio' ),
    'context' => 'normal',

    'fields' => array(



        array(
            'name'		=> __('Active Menu Item', 'hypno' ),
            'id'		=> $prefix . 'portfolio_active_menu_item',
            'type' => 'select',
            'options' => getMenuItems(),
            'clone'		=> false,
            'multiple'	=> false

        ),

        array(
            'name'		=> __('Alternate post title in page header?', 'hypno' ),
            'id'		=> $prefix . "portfolio_enable_alternative_title",
            'clone'		=> false,
            'type' => 'checkbox',
            'std'  => 0,
        ),

        array(
            'name'		=> __('Alternative post title (page header)', 'hypno' ),
            'id'		=> $prefix . 'portfolio_alternative_title',
            'desc'		=> __('HTML is allowed. Box above must be set to "yes" in order to enable the alternative title', 'hypno' ),
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'This is a <span class="medium-thin-font">portfolio</span> item'
        ),


        array(
            'name'	=> __('Background Color', 'hypno' ),
            'id'	=> $prefix . 'portfolio_item_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),
        array(
            'name'	=> __('Header Background Color', 'hypno' ),
            'id'	=> $prefix . 'portfolio_header_bg_color',
            'type'	=> 'color',
            'std'		=> '#ffffff'
        ),
        array(
            'name'	=> __('Heading Font Color', 'hypno' ),
            'id'	=> $prefix . 'portfolio_header_font_color',
            'type'	=> 'color',
            'std'		=> '#000000'
        ),


    )
);

// Portfolio Type Selector

$synergy_meta_boxes[] = array(
    'id' => 'portfolio_item',
    'title' => __('Portfolio Item Details', 'hypno' ),
    'pages' => array( 'portfolio' ),
    'context' => 'normal',

    'fields' => array(


        array(
            'name'		=> __('Item Type', 'hypno' ),
            'id'		=> $prefix . 'portfolio_item_type',
            'type'		=> 'select',
            'options'	=> array(
                'image'		=> __('Image', 'hypno' ),
                'gallery'	=> __('Slider', 'hypno' ),
                'video'		=> __('Video', 'hypno' ),
            ),
            'multiple'	=> false,
            'std'		=> array( 'no' )
        ),

    )
);


// Portfolio Slider Settings

$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'portfolio-gallery',
    'title'		=> __('Project Slider Settings', 'hypno' ),
    'pages'		=> array( 'portfolio' ),
    'context' => 'normal',

    'fields'	=> array(
        array(
            'name'	=> __('Slider Imaes', 'hypno' ),
            'id'	=> $prefix . 'featured_slides',
            'type'	=> 'plupload_image',
            'max_file_uploads' => 10,
        )

    )
);


// Portfolio Video settings

$synergy_meta_boxes[] = array(
    'id'		=> $prefix . 'portfolio-video',
    'title'		=> __('Portfolio Video Settings', 'hypno' ),
    'pages'		=> array( 'portfolio' ),
    'context' => 'normal',

    'fields'	=> array(

        array(
            'name'		=> __('Type', 'hypno' ),
            'id'		=> $prefix . "video_type",
            'clone'		=> false,
            'type'		=> 'select',
            'desc'		=> __('Select id, if you want to provide youtube ID (e.g. https://www.youtube.com/watch?v=<strong>Jn6-TItCazo</strong>) or Embeded code, if you want to provide full <strong>iframe code</strong>', 'hypno' ),
            'options'	=> array(
                'id'		=> __('ID', 'hypno' ),
                'code'		=> __('Embeded Code',  'hypno' ),
            ),
            'multiple'	=> false,
        ),

        array(
            'name'		=> __('ID', 'hypno' ),
            'id'		=> $prefix . "video_id",
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> 'Jn6-TItCazo',
            'desc'		=> __('Provide youtube video id (if you have selected "id" option above)', 'hypno' ),
        ),

        array(
            'name'		=> __('Embeded code (iframe)', 'hypno' ),
            'id'		=> $prefix . "video_code",
            'clone'		=> false,
            'type'		=> 'text',
            'std'		=> '',
            'desc'		=> __('Provide iframe code (if you have selected "Embeded code" option above)', 'hypno' ),
        ),
    )
);


function synergy_meta_boxes()
{
    global $synergy_meta_boxes;
    if ( class_exists( 'RW_Meta_Box' ) )
    {
        foreach ( $synergy_meta_boxes as $meta_box )
        {
            new RW_Meta_Box( $meta_box );
        }
    }
}
add_action( 'rwmb_meta_boxes', 'synergy_meta_boxes' );

?>