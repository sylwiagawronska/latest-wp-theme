<?php get_header();
/*
 Template name: Blog Template
 */
if (is_front_page()) { get_template_part('slider'); }

    // Check if user specified alternative page title
    if ((esc_attr(get_post_meta($post -> ID, "synergy_blog_enable_alternative_title", true)) ==  1)
        && (esc_attr(get_post_meta($post -> ID, "synergy_blog_alternative_title", true)) !==  "")
    ) {
        $title = (get_post_meta($post -> ID, "synergy_blog_alternative_title", true));
    } else {
        // if not, use default title
        $title = get_the_title();
    }

    $type = esc_attr(get_post_meta($post -> ID, "synergy_blog_type", true));

    if (strpos($type, 'masonry') !== false) {
        $masonry = true;
        if (strpos($type, 'masonry_two') !== false) {
            $masonry_type = 'masonry-2-col';
            $per_page = 6;
        } else if (strpos($type, 'masonry_three') !== false) {
            $masonry_type = 'masonry-3-col';
            $per_page = 9;
        } else {
            $masonry_type = '';
            $per_page = 8;
        }
        if (strpos($type, 'feat') !== false) {
            $feat = true;
        } else {
            $feat = false;
        }
    } else {
        $masonry = false;
        if (strpos($type, 'sidebar') !== false) {
            $sidebar = 1;
        } else {
            $sidebar = 0;
        }
    }

    $active_menu = esc_attr(get_post_meta($post -> ID, "synergy_blog_active_menu_item", true));

    $header_type = esc_attr(get_post_meta($post -> ID, "synergy_blog_header_type", true));
    $include_slider = esc_attr(get_post_meta($post -> ID, "synergy_blog_fw_slider", true));
    $slider_id = esc_attr(get_post_meta($post -> ID, "synergy_blog_slider_shortcode", true));

    $images = get_post_meta(get_the_ID(), 'synergy_blog_header_image', false);
    $image_src = "";
    $url = "";
    if ($images) {
        $image_src = wp_get_attachment_image_src($images[0], 'full');
        $url = esc_url($image_src[0]);
    }

    $page_bg = esc_attr(get_post_meta($post -> ID, "synergy_blog_bg_color", true));
    $masonry_bg = esc_attr(get_post_meta($post -> ID, "synergy_blog_masonry_color", true));

    $header_bg = esc_attr(get_post_meta($post -> ID, "synergy_blog_header_bg_color", true));
    $heading_color = esc_attr(get_post_meta($post -> ID, "synergy_blog_heading_font_color", true));


    // fetch breadcrums settings
    $breadcrums = esc_attr(get_post_meta($post -> ID, "synergy_blog_breadcrums", true));
    $breadcrums_border = esc_attr(get_post_meta($post -> ID, "synergy_blog_breadcrums_border", true));
    $breadcrums1 = esc_attr(get_post_meta($post -> ID, "synergy_blog_breadcrums1_color", true));
    $breadcrums2 = esc_attr(get_post_meta($post -> ID, "synergy_blog_breadcrums2_color", true));
    $breadcrums_home = esc_attr(get_post_meta($post -> ID, "synergy_blog_breadcrums_home", true));
?>

    <?php if (!is_front_page()) {
        if  (($include_slider == 1) && ($slider_id != '')){
            echo '<div class="header">';
            putRevSlider(esc_attr($slider_id));
            echo '</div>';
        } else { ?>

            <div class="page-header padding-top-100 padding-bottom-100"  <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 align-center wow fadeIn">
                            <h1 style="color: <?php echo $heading_color; ?>"><?php echo $title;?></h1>
                            <?php
                            if ($breadcrums == 1) {
                                synergy_breadcrumb($breadcrums_home, $breadcrums_border, $breadcrums1, $breadcrums2);
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>



        <?php
        } }?>


<div class="page-content <?php if (!is_front_page()) { ?>padding-top-90<?php }?> padding-bottom-50" style="background-color: <?php echo $page_bg; ?>">
    <div class="container">
        <div class="  <?php if ($masonry) {echo "masonry-grid blog-masonry "; echo $masonry_type;}?>">
            <?php if ($masonry) { ?>
                <?php query_posts("post_type=post &posts_per_page=" .$per_page. "&paged=" . get_query_var('paged'));
                if(have_posts()): while(have_posts()) : the_post();
                    $post_name = $post->post_name;
                    $post_id = get_the_ID();
                    $content = get_the_content();

                    $sticky = '';

                    if (is_sticky())
                        $sticky = '<div class="sticky-post"></div>';
                    ?>

                    <div class="item-masonry">
                        <?php echo $sticky; ?>
                        <div class="well-masonry" style="background-color: <?php echo $masonry_bg; ?>">
                            <div class="news padding-20 margin-bottom-10 wow fadeIn">
                                <?php if ($feat) { get_template_part( 'post-formats/single', get_post_format() ); } ?>
                                <a href="<?php the_permalink(); ?>" class="post-title"><h5><?php the_title(); ?></h5></a>

                                <p>
                                    <?php echo wp_trim_words( $content , rand ( 15 , 35 ) ); ?>
                                </p>

                                <span class="post-info">
                                    <i class="fa fa-pencil-square"></i>
                                    <?php echo '' . __('by', 'hypno') . ' ' . get_the_author_link(); ?>
                                    <i class="fa fa-calendar"></i>
                                    <?php echo '' . __('on ', 'hypno') . ' ' . get_the_date(); ?>
                                </span>
                            </div>
                        </div>
                    </div>

                <?php
                endwhile;
                endif;
                ?>
            <?php } else { ?>
            <?php if ($sidebar == 0) { ?>
            <div class="col-md-12 clearfix">
                <?php } else { ?>
                <div class="col-md-9 clearfix">
                    <?php }  ?>

                    <?php query_posts("post_type=post&paged=" . get_query_var('paged'));
                    if(have_posts()): while(have_posts()) : the_post();
                        $post_name = $post->post_name;
                        $post_id = get_the_ID();
                        $content = get_the_content();
                        $sticky = '';

                        if (is_sticky())
                            $sticky = '<div class="sticky-post"></div>';
                        ?>
                    <div class="blog-content">
                        <div class="post padding-bottom-40 margin-bottom-40">
                            <?php echo $sticky; ?>
                            <?php get_template_part( 'post-formats/single', get_post_format() ); ?>
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2 align-center post-date">
                                    <i class="fa fa-camera"></i>
                                    <h6 class="month"><?php echo get_the_date("M");?></h6>
                                    <h1 class="day"><?php echo get_the_date("d");?></h1>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <h2><?php echo get_the_title();?></h2>

                                    <div class="post-info padding-bottom-20 padding-top-20">
                                        <i class="fa fa-pencil-square"></i>
                                        <span><?php echo '' . __('by', 'hypno') . ' ' . get_the_author_link(); ?></span>
                                        <i class="fa fa-calendar"></i>
                                        <span><?php echo '' . __('on ', 'hypno') . ' ' . get_the_date(); ?></span>
                                        <i class="fa fa-folder-open"></i>
                                        <span><?php echo '' . __('in ', 'hypno') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
                                        <i class="fa fa-comments"></i>
                                        <span><a href="<?php comments_link(); ?>">
                                                <?php comments_number( '0', '1', '%' ); ?></a></span>
                                    </div>
                                    <div class="post-content padding-bottom-30">
                                        <?php echo wp_trim_words( $content , 100 ); ?>
                                    </div>
                                    <div class="read-more padding-top-10 align-right">
                                        <a href="<?php the_permalink(); ?>" role="button" class="btn btn-lg btn-dark"><?php echo __('Read more', 'hypno'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;
                    endif;
                    ?>
                    <?php if (!$masonry) {
                        if(get_next_posts_link() || get_previous_posts_link()) {
                            ;?>
                            <div class="pagination-container padding-bottom-20 padding-top-40">
                                <?php synergy_paginate() ?>
                            </div>
                        <?php }
                    } ?>
                </div>
                <?php if ($sidebar == 1) { ?>
                    <div class="col-md-3 sidebar clearfix">
                        <?php get_sidebar('blog'); ?>
                    </div>
                    <?php }  ?>
            <?php } ?>
        </div><!-- end row -->
            <?php if ($masonry) {
                if(get_next_posts_link() || get_previous_posts_link()) {
                    ;?>
                    <div class="pagination-container align-center padding-bottom-20 padding-top-40">
                        <?php synergy_paginate() ?>
                    </div>
                <?php }
            } ?>
    </div>
</div>



<?php
if ($active_menu != "") {
    ?>
    <script>
        jQuery('.mega-menu-item').each(function() {
            var mega_mnu_item = 'mega-menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mega_mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });

        jQuery('.menu-item').each(function() {
            var mnu_item = 'menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });
    </script>
<?php } ?>
<?php get_footer(); ?>