<?php


// set up textdomain
load_theme_textdomain('hypno', get_template_directory() . '/lang');

// include admin settings
require_once (dirname(__FILE__) . '/admin/admin.php');

// include plugin activator
include_once (get_template_directory() . '/inc/plugin-activator.php');

// include demo data importer
include_once (get_template_directory() . '/inc/demo-data.php');

// add theme support for title tag
add_theme_support( 'title-tag' );

// add theme support for feed links
add_theme_support( 'automatic-feed-links' );

// include metaboxes settings
if ( class_exists( 'RW_Meta_Box' ) ) {
    include_once (get_template_directory() . '/inc/metaboxes.php');
}

// set content width
if ( ! isset( $content_width ) ) {
    $content_width = 1200;
}

// Flush rewrite rules
function synergy_flush_rewrite_rules() {
    flush_rewrite_rules();
}
add_action('after_switch_theme', 'synergy_flush_rewrite_rules');

// include stripts for backend
function admin_enqueue_scripts() {
    if(is_admin()) {
        wp_enqueue_script('admin-scripts', get_template_directory_uri() . '/build/js/admin.js', array('jquery'), true);
    }
}
add_action('admin_enqueue_scripts', 'admin_enqueue_scripts');

// include scripts for frontend
function theme_enqueue_scripts() {
    if(!is_admin()) {
        wp_enqueue_script('theme-scripts', get_template_directory_uri() . '/build/js/scripts.min.js', array('jquery'), true);
    }
}
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');

// include theme styles
// if a new stylesheet has been created by Admin panel, attach it
// else attach default file
function theme_enqueue_styles() {

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/synergy/theme.css';

    if(!is_admin()) {
        wp_enqueue_style('theme-scripts', get_template_directory_uri() . '/build/css/style.min.css');

        if (class_exists( 'Redux' ) && class_exists( 'Synergy' ) && file_exists($upload_dir)){
            wp_enqueue_style( 'theme', $upload['baseurl'] . '/synergy/theme.css');
        } else {
            wp_enqueue_style('theme-main', get_template_directory_uri() . '/build/css/theme.min.css');
        }
    }


}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');


// load google fonts
function synergy_add_google_fonts() {
    wp_register_style('Roboto', 'http://fonts.googleapis.com/css?family=Roboto:100,200,400,300,500,600,700,800,900');
    wp_enqueue_style('Roboto');
}
add_action('wp_enqueue_scripts', 'synergy_add_google_fonts');



/**
 * ********************
 * **** NAVIGATION ****
 * ********************
 */

// register menu
function synergy_register_menus() {
    register_nav_menus(array(
        'primary-menu' => 'Primary Navigation Menu',
        'social-bar' => 'Social Bar',
    ));
}
add_action('init', 'synergy_register_menus');


/**
 * Class Synergy_Menu_Walker
 * Custom navigation markup
 */

class Synergy_Menu_Walker extends Walker_Nav_Menu {

    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        // Change sub-menu to dropdown menu
        $output .= "\n$indent<ul class=\"dropdown-menu span10\">\n";
    }

    function start_el ( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query, $wpdb;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';
        $postmeta = $wpdb->prefix . 'postmeta';
        $has_children = $wpdb->get_var("SELECT COUNT(meta_id)
                            FROM $postmeta
                            WHERE meta_key='_menu_item_menu_item_parent'
                            AND meta_value='".$item->ID."'");

        $dropdown = "";

        // add dropdown class
        if ($has_children) {
            $dropdown = "dropdown";
        }

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr( $class_names ) . ' ' . $dropdown .'"';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';


        $output .= $indent . '<li' . $id . $value . $class_names .'>';

        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

        // Check if menu item is in main menu
        if ( $has_children > 0  ) {
            // These lines adds your custom class and attribute
              $attributes .= ' class="dropdown-toggle"';
        }

        $item_output = $args->before;

        // Add the caret if menu level is 0
        if ( $has_children > 0  ) {
            $item_output .= '<span class="toggleDropdown" href="#" data-toggle="dropdown"><i class="fa fa-angle-double-down"></i></span>';
        }

        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

}


/**
 * *********************************
 * Custom breadcrums display
 * @param $breadcrums_home
 * @param $breadcrums_border
 * @param $breadcrums1
 * @param $breadcrums2
 * **********************************
 */
function synergy_breadcrumb ($breadcrums_home, $breadcrums_border, $breadcrums1, $breadcrums2) {

    // Settings

    if ($breadcrums_home && $breadcrums_home != "") {
        $home_title = $breadcrums_home;
    } else {
        $home_title = __('Home', 'hypno');
    }

    if ($breadcrums_border == 1) {
        $border = 'breadcrumb-border';
    } else {
        $border = '';
    }
    $id         = 'breadcrumb';
    $class      = 'breadcrumb';

    // Get the query & post information
    global $post,$wp_query;
    $category = get_the_category();

    // Build the breadcrums
    echo '<nav id="' . esc_attr($id) . '" class="margin-top-40 ' . esc_attr($class) . ' ' . $border .'">';

    // Do not display on the homepage
    if ( !is_front_page() ) {

        // Home page
        echo '<a style="color: '.esc_attr($breadcrums1).'" class=" breadcrumb-itembread-link bread-home" href="' . get_home_url() . '" title="' . esc_attr($home_title) . '">' . esc_attr($home_title) . '</a>';


        if ( is_single() ) {

            // Single post (Only display the first category)
            if ($category) {
                echo '
            <a style="color: '.esc_attr($breadcrums1).'" class="breadcrumb-item bread-cat bread-cat-' . esc_attr($category[0]->term_id) . ' bread-cat-' . esc_attr($category[0]->category_nicename) . '
            " href="' . get_category_link($category[0]->term_id ) . '" title="' . esc_attr($category[0]->cat_name) . '">' . esc_attr($category[0]->cat_name) . '</a>';
            }
            echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-' . esc_attr($post->ID) . '" title="' . get_the_title() . '">' . get_the_title() . '</span>';

        } else if ( is_category() ) {

            // Category page
            echo '
            <span style="color: '.$breadcrums2.'" class="breadcrumb-item active bread-current bread-cat-' . esc_attr($category[0]->term_id) . ' bread-cat-' . esc_attr($category[0]->category_nicename) . '">' . esc_attr($category[0]->cat_name) . '</span>';

        } else if ( is_page() ) {

            // Standard page
            if( $post->post_parent ){
                $parents ='';
                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                foreach ( $anc as $ancestor ) {
                    $parents .= '<a style="color: '.esc_attr($breadcrums1).'" class="breadcrumb-item bread-parent bread-parent-' . esc_attr($ancestor) . '
                    " href="' . esc_attr(get_permalink($ancestor)) . '" title="' . esc_attr(get_the_title($ancestor)) . '">' . esc_attr(get_the_title($ancestor)) . '</a>';
                }

                // Display parent pages
                echo wp_kses_post($parents);

                // Current page
                echo '<span class="breadcrumb-item active" style="color: '.esc_attr($breadcrums2).'" title="' . get_the_title() . '"> ' . get_the_title() . '</span>';

            } else {

                // Just display current page if not parents
                echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-' . esc_attr($post->ID) . '"> ' . get_the_title() . '</span>';

            }

        } else if ( is_tag() ) {

            // Tag page

            // Get tag information
            $term_id = get_query_var('tag_id');
            $taxonomy = 'post_tag';
            $args ='include=' . $term_id;
            $terms = get_terms( $taxonomy, $args );

            // Display the tag name
            echo '<span style="color: '.esc_attr($breadcrums2).'"
            class="breadcrumb-item active bread-current bread-tag-' . esc_attr($terms[0]->term_id) . ' bread-tag-' . esc_attr($terms[0]->slug) . '">' . esc_attr($terms[0]->name) . '</span>';

        } elseif ( is_day() ) {

            // Day archive

            // Year link
            echo '<a style="color: '.esc_attr($breadcrums1).'" class="breadcrumb-item bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';

            // Month link
            echo '<a style="color: '.$breadcrums1.'" class="breadcrumb-item bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a>';

            // Day display
            echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</span>';

        } else if ( is_month() ) {

            // Month Archive

            // Year link
            echo '<a style="color: '.esc_attr($breadcrums1).'" class="breadcrumb-item bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a>';
            // Month display
            echo '<span style="color: '.esc_attr($breadcrums1).'" class="breadcrumb-item bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</span>';

        } else if ( is_year() ) {

            // Display year archive
            echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</span>';

        } else if ( is_author() ) {

            // Auhor archive

            // Get the author information
            global $author;
            $userdata = get_userdata( $author );

            // Display author name
            echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-current-' . esc_attr($userdata->user_nicename) . '"
            title="' . esc_attr($userdata->display_name) . '">' . 'Author: ' . esc_attr($userdata->display_name) . '</span>';

        } else if ( get_query_var('paged') ) {

            // Paginated archives
            echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page', 'hypno') . ' ' . get_query_var('paged') . '</span>';

        } else if ( is_search() ) {

            // Search results page
            echo '<span style="color: '.esc_attr($breadcrums2).'" class="breadcrumb-item active bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</span>';

        } elseif ( is_404() ) {

            // 404 page
            echo '<span class="breadcrumb-item active" style="color: '.esc_attr($breadcrums1).'">' .  _e('404 error', 'hypno') . '</span>';
        }

    }


    echo '</nav>';

}



// page sidebar
register_sidebar(array('name' => __('Page Sidebar', 'hypno'), 'id' => 'page-sidebar', 'description' => __('Page widgets.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));


// blog sidebar
register_sidebar(array('name' => __('Blog Sidebar', 'hypno'), 'id' => 'blog-sidebar', 'description' => __('Blog widgets.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));

// store sidebar
register_sidebar(array('name' => __('Store Sidebar', 'hypno'), 'id' => 'store-sidebar', 'description' => __('Store widgets.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));


// first footer widget
register_sidebar(array('name' => __('Footer Widgets : Fist Column', 'hypno'), 'id' => 'footer-widget-1', 'description' => __('Footer Widgets : Fist Column.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));

// second footer widget
register_sidebar(array('name' => __('Footer Widgets : Second Column', 'hypno'), 'id' => 'footer-widget-2', 'description' => __('Footer Widgets : Second Column.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));

// third footer widget
register_sidebar(array('name' => __('Footer Widgets : Third Column', 'hypno'), 'id' => 'footer-widget-3', 'description' => __('Footer Widgets : Third Column.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));

// fourts footer widget
register_sidebar(array('name' => __('Footer Widgets : Fourth Column', 'hypno'), 'id' => 'footer-widget-4', 'description' => __('Footer Widgets : Fourth Column.', 'hypno'), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>'));

// post types
add_theme_support('post-formats', array('gallery', 'quote', 'audio', 'video'));




/**
 * ********************
 * **** THUMBNAILS ****
 * ********************
 */

// post thumbnails
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

// add woocommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;

    ob_start();

    ?>
    <a class="cart-customlocation" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'hypno'); ?>"><i class="fa  fa-shopping-bag"></i> [<?php echo $woocommerce->cart->cart_contents_count; ?>] - <?php echo WC()->cart->get_cart_total(); ?></a>
    <?php

    $fragments['a.cart-customlocation'] = ob_get_clean();

    return $fragments;

}


add_filter( 'woocommerce_pagination_args', 	'rocket_woo_pagination' );

// adjust woocommerce pagination
function rocket_woo_pagination( $args ) {
    $args['prev_text'] = '<i class="fa fa-angle-left"></i>';
    $args['next_text'] = '<i class="fa fa-angle-right"></i>';
    return $args;
}

// define woocommerce columns per row
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        global $hypno_options;
        if (array_key_exists('woocommerce-columns', $hypno_options) && $hypno_options['woocommerce-columns'] !== "") {
            return $hypno_options['woocommerce-columns'];
        }
        else return 3; // 3 products per row
    }
}

// adjust woocommerce breadcrums
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
        'delimiter'   => '',
        'wrap_before' => '<nav id="breadcrumb" class="margin-top-40 breadcrumb breadcrumb-border">',
        'wrap_after'  => '</nav>',
        'before'      => '',
        'after'       => '',
        'home'        => _x( 'Home', 'breadcrumb', 'hypno' ),
    );
}

/**
 * *******************
 * **** COMMENTS ****
 * *******************
 */


/**
 * Change comment form fields markup
 */
add_filter( 'comment_form_default_fields', 'bootstrap3_comment_form_fields' );
function bootstrap3_comment_form_fields( $fields ) {
    $commenter = wp_get_current_commenter();

    $req      = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    $html5    = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;

    $fields   =  array(
        'author' => '<div class="form-group comment-form-author col-md-4">' .
            '<input class="form-control" placeholder="' . __( 'Name', 'hypno' ) . '" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
        'email'  => '<div class="form-group comment-form-email col-md-4">' .
            '<input class="form-control" placeholder="' . __( 'Email', 'hypno' ) . '" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
        'url'    => '<div class="form-group comment-form-url col-md-4">' .
            '<input class="form-control" placeholder="' . __( 'Website', 'hypno' ) . '" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div>'
    );

    return $fields;
}


/**
 * Change comment form's textarea and sumbit button markup
 */
add_filter( 'comment_form_defaults', 'bootstrap3_comment_form' );
function bootstrap3_comment_form( $args ) {
    $args['comment_field'] = '<div class="form-group comment-form-comment col-md-12">
            <textarea class="form-control" id="comment" placeholder="' . __( 'Comment', 'hypno' ) . '" name="comment" cols="45" rows="8" aria-required="true"></textarea>
        </div>';
    $args['class_submit'] = 'btn btn-lg btn-dark'; // since WP 4.1

    return $args;
}

/**
 * Wrap comments in container
 */
add_filter( 'comment_form_field_comment', 'my_comment_form_field_comment' );
function my_comment_form_field_comment( $comment_field ) {

    $comment_field = '<div>' . $comment_field . '</div>';

    return $comment_field;
}


/**
 * Move the comment text area to the bottom of the form
 */
add_filter( 'comment_form_fields', 'move_comment_field' );
function move_comment_field( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}


/**
 * Change comments section markup
 * @param $comment
 * @param $args
 * @param $depth
 */
function synergy_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo wp_kses_post($tag) ?>
    <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?>
    id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) :
        ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-name">
        <?php
        if ($args['avatar_size'] != 0)
            echo get_avatar($comment, $args['avatar_size']);
        ?>
        <div class="comment-info">
            <?php printf(__('<span class="fn name">%s</span>', 'hypno'), get_comment_author_link()); ?>
            <span class="reply">
            (<?php comment_reply_link(array_merge($args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>)
            </span>
        </div>
    </div>
    <?php if ( $comment->comment_approved == '0' ) :
        ?>
        <div class="comment-date"><em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'hypno'); ?>
            </em>
            <br /></div>
    <?php endif; ?>

    <div class="comment-date"><a href="<?php echo htmlspecialchars(get_comment_link($comment -> comment_ID)); ?>">
            <?php
            printf(__('%1$s at %2$s', 'hypno'), get_comment_date(), get_comment_time());
            ?>
        </a><?php edit_comment_link(__('(Edit)', 'hypno'), '  ', ''); ?>
    </div>

    <?php comment_text(); ?>
    <?php if ( 'div' != $args['style'] ) :
        ?>
        </div>
    <?php endif; ?>
<?php
}


// customize recent posts widget

class Synergy_Widget_Recent_Posts extends WP_Widget {

    /**
     * Sets up a new Recent Posts widget instance.
     *
     * @since 2.8.0
     * @access public
     */
    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget_recent_entries',
            'description' => __( 'Your site&#8217;s most recent Posts.', 'hypno' ),
            'customize_selective_refresh' => true,
        );
        parent::__construct( 'recent-posts', __( 'Recent Posts', 'hypno' ), $widget_ops );
        $this->alt_option_name = 'widget_recent_entries';
    }

    /**
     * Outputs the content for the current Recent Posts widget instance.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $args     Display arguments including 'before_title', 'after_title',
     *                        'before_widget', and 'after_widget'.
     * @param array $instance Settings for the current Recent Posts widget instance.
     */
    public function widget( $args, $instance ) {
        if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }

        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts', 'hypno' );

        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number )
            $number = 5;
        $show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

        /**
         * Filters the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args An array of arguments used to retrieve the recent posts.
         */
        $r = new WP_Query( apply_filters( 'widget_posts_args', array(
            'posts_per_page'      => $number,
            'no_found_rows'       => true,
            'post_status'         => 'publish',
            'ignore_sticky_posts' => true
        ) ) );

        if ($r->have_posts()) :
            ?>
            <?php echo $args['before_widget']; ?>

            <?php if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        } ?>
            <ul class="latest-news-list">
                <?php while ( $r->have_posts() ) : $r->the_post(); ?>
                    <li <?php if ( has_post_thumbnail() ) { ?>style="background-image:url('<?php the_post_thumbnail_url( 'post-featured-image-grids' ) ?>')"<?php } ?>>
                        <div class="news-list-hover">
                            <h6><?php get_the_title() ? the_title() : the_ID(); ?></h6>
                            <a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
            <?php echo $args['after_widget']; ?>
            <?php
            // Reset the global $the_post as this query will have stomped on it
            wp_reset_postdata();

        endif;
    }

    /**
     * Handles updating the settings for the current Recent Posts widget instance.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $new_instance New settings for this instance as input by the user via
     *                            WP_Widget::form().
     * @param array $old_instance Old settings for this instance.
     * @return array Updated settings to save.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        $instance['number'] = (int) $new_instance['number'];
        $instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
        return $instance;
    }

    /**
     * Outputs the settings form for the Recent Posts widget.
     *
     * @since 2.8.0
     * @access public
     *
     * @param array $instance Current settings.
     */
    public function form( $instance ) {
        $title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 2;
        ?>
        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'hypno' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' , 'hypno'); ?></label>
            <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" /></p>

    <?php
    }
}

add_action( 'widgets_init', create_function( '', 'register_widget( "Synergy_Widget_Recent_Posts" );' ) );

// adjust pagination
function synergy_paginate() {
    global $wp_query, $wp_rewrite;
    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

    $pagination = array(
        'base' => @add_query_arg('page','%#%'),
        'format' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'show_all' => true,
        'type' => 'list',
        'next_text' => '&raquo;',
        'prev_text' => '&laquo;'
    );

    if( $wp_rewrite->using_permalinks() )
        $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

    if( !empty($wp_query->query_vars['s']) )
        $pagination['add_args'] = array( 's' => get_query_var( 's' ) );

    echo paginate_links( $pagination );
}



// customize woocommerce
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);


if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    }
}
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
    function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
        global $post, $woocommerce;
        $output = '<div class="woo-imagewrapper">';

        if ( has_post_thumbnail() ) {
            $output .= get_the_post_thumbnail( $post->ID, $size );
        }
        $output .= '</div>';
        return $output;
    }
}





// thumbnails sizes
set_post_thumbnail_size(900, 600, false);
add_image_size('post-featured-image', 900, 600, true);
add_image_size('post-featured-image-grids', 600, 400, false);
add_image_size('portfolio-feat', 900, 600, true);
add_image_size('portfolio-thumb', 600, 400, true);
add_image_size('avatars', 100, 100, true);
add_image_size('shop_catalog_image_size', 400, 400, true);
add_image_size('shop_single_image_size', 800, 800, true);
add_image_size('shop_thumbnail_image_size', 120, 120, true);

// set image dimensions for woocommerce
function synergy_woocommerce_image_dimensions() {
    global $pagenow;

    if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
        return;
    }
    $catalog = array(
        'width' 	=> '400',	// px
        'height'	=> '400',	// px
        'crop'		=> 1 		// true
    );
    $single = array(
        'width' 	=> '800',	// px
        'height'	=> '800',	// px
        'crop'		=> 1 		// true
    );
    $thumbnail = array(
        'width' 	=> '120',	// px
        'height'	=> '120',	// px
        'crop'		=> 0 		// false
    );
    // Image sizes
    update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
    update_option( 'shop_single_image_size', $single ); 		// Single product image
    update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}
add_action( 'after_switch_theme', 'synergy_woocommerce_image_dimensions', 1 );


// Add megamenu custom theme

function megamenu_add_theme_hypno_1484672471($themes) {
    $themes["hypno_1484672471"] = array(
        'title' => 'Hypno',
        'container_background_from' => 'rgb(255, 255, 255)',
        'container_background_to' => 'rgb(255, 255, 255)',
        'container_padding_left' => '10px',
        'container_padding_right' => '15px',
        'container_padding_top' => '5px',
        'container_padding_bottom' => '10px',
        'arrow_up' => 'disabled',
        'arrow_down' => 'disabled',
        'arrow_left' => 'disabled',
        'menu_item_background_hover_from' => 'rgb(255, 255, 255)',
        'menu_item_background_hover_to' => 'rgb(255, 255, 255)',
        'menu_item_spacing' => '10px',
        'menu_item_link_font_size' => '15px',
        'menu_item_link_color' => 'rgb(34, 34, 34)',
        'menu_item_link_weight' => 'bold',
        'menu_item_link_color_hover' => 'rgb(203, 203, 203)',
        'menu_item_link_weight_hover' => 'bold',
        'menu_item_highlight_current' => 'on',
        'panel_background_from' => 'rgb(255, 255, 255)',
        'panel_background_to' => 'rgb(255, 255, 255)',
        'panel_width' => 'body',
        'panel_inner_width' => 'body',
        'panel_border_color' => 'rgb(30, 31, 32)',
        'panel_border_top' => '3px',
        'panel_header_color' => 'rgb(34, 34, 34)',
        'panel_header_text_transform' => 'none',
        'panel_header_font_size' => '15px',
        'panel_header_padding_bottom' => '20px',
        'panel_header_border_color' => '#555',
        'panel_padding_left' => '30px',
        'panel_padding_right' => '30px',
        'panel_padding_top' => '30px',
        'panel_padding_bottom' => '30px',
        'panel_widget_padding_left' => '30px',
        'panel_widget_padding_right' => '30px',
        'panel_font_size' => '14px',
        'panel_font_color' => '#666',
        'panel_font_family' => 'inherit',
        'panel_second_level_font_color' => 'rgb(0, 0, 0)',
        'panel_second_level_font_color_hover' => '#555',
        'panel_second_level_text_transform' => 'none',
        'panel_second_level_font' => 'inherit',
        'panel_second_level_font_size' => '15px',
        'panel_second_level_font_weight' => 'normal',
        'panel_second_level_font_weight_hover' => 'bold',
        'panel_second_level_text_decoration' => 'none',
        'panel_second_level_text_decoration_hover' => 'none',
        'panel_second_level_padding_bottom' => '5px',
        'panel_second_level_margin_bottom' => '5px',
        'panel_second_level_border_color' => '#555',
        'panel_third_level_font_color' => '#666',
        'panel_third_level_font_color_hover' => '#666',
        'panel_third_level_font' => 'inherit',
        'panel_third_level_font_size' => '14px',
        'panel_third_level_padding_bottom' => '5px',
        'flyout_width' => '220px',
        'flyout_menu_background_from' => 'rgb(255, 255, 255)',
        'flyout_menu_background_to' => 'rgb(255, 255, 255)',
        'flyout_border_color' => 'rgb(30, 31, 32)',
        'flyout_border_top' => '3px',
        'flyout_menu_item_divider' => 'on',
        'flyout_menu_item_divider_color' => 'rgb(220, 222, 223)',
        'flyout_link_padding_left' => '50px',
        'flyout_link_padding_right' => '5px',
        'flyout_link_padding_top' => '8px',
        'flyout_link_padding_bottom' => '8px',
        'flyout_background_from' => 'rgb(255, 255, 255)',
        'flyout_background_to' => 'rgb(255, 255, 255)',
        'flyout_background_hover_from' => 'rgb(30, 31, 32)',
        'flyout_background_hover_to' => 'rgb(30, 31, 32)',
        'flyout_link_size' => '15px',
        'flyout_link_color' => 'rgb(0, 0, 0)',
        'flyout_link_color_hover' => 'rgb(255, 255, 255)',
        'flyout_link_family' => 'inherit',
        'responsive_breakpoint' => '991px',
        'transitions' => 'on',
        'mobile_columns' => '1',
        'toggle_background_from' => '#222',
        'toggle_background_to' => '#222',
        'toggle_font_color' => 'rgb(34, 34, 34)',
        'mobile_background_from' => 'rgb(255, 255, 255)',
        'mobile_background_to' => 'rgb(255, 255, 255)',
        'disable_mobile_toggle' => 'on',
        'custom_css' => '/** Push menu onto new line **/
#{$wrap} {
    clear: both;
}
#{$wrap} #{$menu} > li.mega-menu-item.active > a {
    color: #cbcbcb;
}
@media only screen and (max-width: 991px) {
#{$wrap} #{$menu} > li.mega-menu-flyout ul.mega-sub-menu li.mega-menu-item ul.mega-sub-menu a.mega-menu-link{
	padding-left: 50px;
}
}',
    );
    return $themes;
}
add_filter("megamenu_themes", "megamenu_add_theme_hypno_1484672471");