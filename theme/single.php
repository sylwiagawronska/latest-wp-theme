<?php get_header();
if (is_front_page()) { get_template_part('slider'); }

if (have_posts()) : while (have_posts()) : the_post();

    // Check if user specified alternative page title
    if ((esc_attr(get_post_meta($post -> ID, "synergy_enable_alternative_title", true)) ==  1)
        && (esc_attr(get_post_meta($post -> ID, "synergy_alternative_title", true)) !==  "")
    ) {
        $title = (get_post_meta($post -> ID, "synergy_alternative_title", true));
    } else {
        // if not, use default title
        $title = get_the_title();
    }

    $active_menu = esc_attr(get_post_meta($post -> ID, "synergy_active_menu_item", true));

    $sidebar = esc_attr(get_post_meta($post -> ID, "synergy_sidebar", true));

    $post_header = esc_attr(get_post_meta($post -> ID, "synergy_post_header", true));

    $enable_comments = esc_attr(get_post_meta($post -> ID, "synergy_enable_comments", true));

    $header_type = esc_attr(get_post_meta($post -> ID, "synergy_header_type", true));
    $include_slider = esc_attr(get_post_meta($post -> ID, "synergy_fw_slider", true));
    $slider_id = esc_attr(get_post_meta($post -> ID, "synergy_slider_shortcode", true));

    $images = get_post_meta(get_the_ID(), 'synergy_header_image', false);
    if ($images) {
        $image_src = wp_get_attachment_image_src($images[0], 'full');
        $url = esc_url($image_src[0]);
    }

    $header_bg = esc_attr(get_post_meta($post -> ID, "synergy_header_bg_color", true));
    $heading_color = esc_attr(get_post_meta($post -> ID, "synergy_heading_font_color", true));


    // fetch breadcrums settings
    $breadcrums = esc_attr(get_post_meta($post -> ID, "synergy_breadcrums", true));
    $breadcrums_border = esc_attr(get_post_meta($post -> ID, "synergy_breadcrums_border", true));
    $breadcrums1 = esc_attr(get_post_meta($post -> ID, "synergy_breadcrums1_color", true));
    $breadcrums2 = esc_attr(get_post_meta($post -> ID, "synergy_breadcrums2_color", true));
    $breadcrums_home = esc_attr(get_post_meta($post -> ID, "synergy_breadcrums_home", true));
?>


    <?php if (!is_front_page()) {
        if  (($include_slider == 1) && ($slider_id != '')){
            echo '<div class="header">';
            putRevSlider(esc_attr($slider_id));
            echo '</div>';
        } else if ($post_header == 1) { ?>

            <div class="page-header padding-top-100 padding-bottom-100"  <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 align-center wow fadeIn">
                            <h1 style="color: <?php echo $heading_color; ?>"><?php echo $title;?></h1>
                            <?php
                            if ($breadcrums == 1) {
                                synergy_breadcrumb($breadcrums_home, $breadcrums_border, $breadcrums1, $breadcrums2);
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>



        <?php
        } else { ?>
            <div class="page-header padding-top-50 padding-bottom-50">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 align-center wow fadeIn">
                        </div>
                    </div>
                </div>
            </div>
        <?php } }?>


    <div class="page-content <?php if (!is_front_page()) { ?>padding-top-50<?php }?> padding-bottom-50">
        <div class="container">
            <div class="row">
                <?php if ($sidebar == 0) { ?>
                    <div class="col-md-12 clearfix">
                <?php } else { ?>
                    <div class="col-md-9 clearfix">
                <?php }  ?>
                        <div <?php post_class('post padding-bottom-40 margin-bottom-40'); ?>>
                            <?php get_template_part( 'post-formats/single', get_post_format() );  ?>

                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2 align-center post-date">
                                    <i class="fa fa-camera"></i>
                                    <h6 class="month"><?php echo get_the_date("M");?></h6>
                                    <h1 class="day"><?php echo get_the_date("d");?></h1>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <h2><?php echo get_the_title();?></h2>

                                    <div class="post-info padding-bottom-20 padding-top-20">
                                        <i class="fa fa-pencil-square"></i>
                                        <span><?php echo '' . __('by', 'hypno') . ' ' . get_the_author_link(); ?></span>
                                        <i class="fa fa-calendar"></i>
                                        <span><?php echo '' . __('on ', 'hypno') . ' ' . get_the_date(); ?></span>
                                        <i class="fa fa-folder-open"></i>
                                        <span><?php echo '' . __('in ', 'hypno') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
                                        <i class="fa fa-comments"></i>
                                        <span><a href="<?php comments_link(); ?>" title="<?php esc_attr(comments_number()); ?>">
                                                    <?php comments_number( '0', '1', '%' ); ?></a></span>
                                    </div>
                                    <div class="post-content">
                                        <?php the_content('', FALSE); ?>
                                    </div>
                                    <?php
                                    if (has_tag()) {
                                        the_tags('<div class="post-tags padding-top-20 padding-bottom-20"><i class="fa fa-tags"> </i> ', ', ', '</div>');
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>


                        <?php if ($enable_comments) {
                            comments_template();
                        }?>

                </div>
                <?php if ($sidebar == 1) { ?>
                    <div class="col-md-3 sidebar clearfix">
                        <?php get_sidebar('blog'); ?>
                    </div>
                <?php }  ?>
            </div>
        </div>
    </div>



<?php
endwhile;
endif;

if ($active_menu != "") {
?>
    <script>
        jQuery('.mega-menu-item').each(function() {
            var mega_mnu_item = 'mega-menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mega_mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });

        jQuery('.menu-item').each(function() {
            var mnu_item = 'menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });
    </script>
<?php } ?>
<?php get_footer(); ?>