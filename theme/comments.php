<?php if ( post_password_required() ) : ?>
    <p class="access-denied"><?php _e('Access denied. This post is password protected.', 'hypno'); ?></p>
    <?php
    return;
endif;
?>
<?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
<div class="comments-container">
    <?php if ( have_comments() ) : ?>
    <h4 class="padding-bottom-40"><?php printf(_n(__('1 Comment', 'hypno'), '%1$s ' . __('Comments', 'hypno') . '', get_comments_number()), number_format_i18n(get_comments_number())); ?>
        </h4>
        <ul class="comments">
            <?php wp_list_comments(array('avatar_size' => '90', 'type' => 'comment', 'callback' => 'synergy_comment')); ?>
        </ul>

        <div class="comments-pagination wow fadeIn animated">
            <?php paginate_comments_links(); ?>
        </div>


    <?php endif; ?>
    <?php comment_form(); ?>

</div>