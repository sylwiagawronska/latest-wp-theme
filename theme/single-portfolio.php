<?php get_header();

if (have_posts()) : while (have_posts()) : the_post();

    // Check if user specified alternative page title
    if ((esc_attr(get_post_meta($post -> ID, "synergy_portfolio_enable_alternative_title", true)) ==  1)
        && (esc_attr(get_post_meta($post -> ID, "synergy_portfolio_alternative_title", true)) !==  "")
    ) {
        $title = (get_post_meta($post -> ID, "synergy_portfolio_alternative_title", true));
    } else {
        // if not, use default title
        $title = get_the_title();
    }

    $active_menu = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_active_menu_item", true));

    $previous_posts_image = '<img src="'.get_template_directory_uri().'/images/arrow-dark-prev.png" alt="previous">';
    $next_posts_image = '<img src="'.get_template_directory_uri().'/images/arrow-dark-next.png" alt="next">';
    $exit_image = '<img src="'.get_template_directory_uri().'/images/exit.png" alt="exit">';

    $header_bg = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_header_bg_color", true));
    $heading_color = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_header_font_color", true));
    $background_color = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_item_bg_color", true));

    $type = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_item_type", true));


?>


    <div class="page-header light-header padding-top-70 padding-bottom-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-center">
                    <h1><?php echo $title; ?></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="portfolio-item-navigation padding-top-30">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-center">
                    <span class="prev-item">
                        <?php previous_post_link('%link',  $previous_posts_image );?>
                    </span>
                    <span class="next-item">
                        <?php next_post_link('%link',  $next_posts_image );?>
                    </span>
                </div>
            </div>
        </div>
    </div>


    <div id="ajaxItem" class="padding-bottom-50">
        <div class="portfolio-item-navigation-ajax">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 align-center">

                        <span class="prev-item prev-ajax"><?php previous_post_link('%link',  $previous_posts_image );?></span>

                        <a href="#" class="exit-item exit-ajax"> <?php echo $exit_image; ?> </a>

                        <span class="next-item next-ajax"><?php next_post_link('%link',  $next_posts_image );?></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content padding-top-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <?php get_template_part( 'post-formats/single', $type );  ?>
                    </div>
                    <div class="col-md-4 portfolio-desc">
                        <h3 class="padding-bottom-30"><?php echo get_the_title();?></h3>
                        <?php the_content('', FALSE); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
endwhile;
endif;

if ($active_menu != "") {
    ?>
    <script>
        jQuery('.mega-menu-item').each(function() {
            var mega_mnu_item = 'mega-menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mega_mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });

        jQuery('.menu-item').each(function() {
            var mnu_item = 'menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });
    </script>
<?php }
?>
<?php get_footer(); ?>