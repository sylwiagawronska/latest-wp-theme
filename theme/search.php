<?php get_header();

global $hypno_options;
$breadcrums1 = $hypno_options['hypno-typography-heading']['color'];
$breadcrums2 = $hypno_options['hypno-typography-body']['color'];
?>

    <div class="page-header padding-top-100 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-center wow fadeIn">
                    <?php if ( have_posts() ) : ?>
                        <h1><?php printf( __( 'Search Results for: %s', 'hypno' ), '<span class="medium-thin-font">' . get_search_query() . '</span>' ); ?></h1>
                        <?php synergy_breadcrumb("", 1, $breadcrums1, $breadcrums2); ?>
                    <?php else : ?>
                        <h1><?php _e( 'Nothing Found', 'hypno' ); ?></h1>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>


    <div class="page-content padding-top-90 padding-bottom-50">
        <div class="container">
            <div class="col-md-9 clearfix">
                <?php
                if(have_posts()): while(have_posts()) : the_post();
                    $post_name = $post->post_name;
                    $post_id = get_the_ID();
                    $content = get_the_content();
                    ?>
                    <div class="blog-content">
                        <div class="post padding-bottom-40 margin-bottom-40">
                            <?php get_template_part( 'post-formats/single', get_post_format() ); ?>
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-2 align-center post-date">
                                    <i class="fa fa-camera"></i>
                                    <h6 class="month"><?php echo get_the_date("M");?></h6>
                                    <h1 class="day"><?php echo get_the_date("d");?></h1>
                                </div>
                                <div class="col-md-10 col-sm-10 col-xs-10">
                                    <h2><?php echo get_the_title();?></h2>

                                    <div class="post-info padding-bottom-20 padding-top-20">
                                        <i class="fa fa-pencil-square"></i>
                                        <span><?php echo '' . __('by', 'hypno') . ' ' . get_the_author_link(); ?></span>
                                        <i class="fa fa-calendar"></i>
                                        <span><?php echo '' . __('on ', 'hypno') . ' ' . get_the_date(); ?></span>
                                        <i class="fa fa-folder-open"></i>
                                        <span><?php echo '' . __('in ', 'hypno') . ' ' . get_the_category_list(', ', 'single', $post -> ID); ?></span>
                                        <i class="fa fa-comments"></i>
                                        <span><a href="<?php comments_link(); ?>">
                                                <?php comments_number( '0', '1', '%' ); ?></a></span>
                                    </div>
                                    <div class="post-content padding-bottom-30">
                                        <?php echo wp_trim_words( $content , 100 ); ?>
                                    </div>
                                    <div class="read-more padding-top-10 align-right">
                                        <a href="<?php the_permalink(); ?>" role="button" class="btn btn-lg btn-dark"><?php echo __('Read more', 'hypno'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.', 'hypno'); ?></p>
                <?php endif; ?>
                <?php if(get_next_posts_link() || get_previous_posts_link()) { ?>
                    <div class="pagination-container padding-bottom-20 padding-top-40">
                        <?php synergy_paginate() ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-3 sidebar clearfix">
                <?php get_sidebar('blog'); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>