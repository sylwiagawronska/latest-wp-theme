<?php get_header(); ?>


    <div class="page-header error-page padding-bottom-80">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-center error padding-top-100">
                    <div class="error-shadow wow fadeIn">
                        <h1 class="padding-bottom-60"> <?php _e('404', 'hypno'); ?> </h1>
                    </div>
                    <h1 class="padding-bottom-80"><?php _e('Ooops... ', 'hypno'); ?>
                        <span class="medium-thin-font"><?php _e('something', 'hypno'); ?></span>
                        <?php _e(' went wrong', 'hypno'); ?>
                    </h1>

                    <h4><?php _e('The page you are looking for has not been found.', 'hypno'); ?></h4>

                    <div class="return-home wow fadeIn padding-bottom-80 padding-top-80">
                        <a href="<?php echo esc_url(home_url()); ?>"><i class="fa fa-home"></i></a>
                    </div>

                    <p>
                        <?php _e('Try checking the URL for errors, and hit refresh,', 'hypno'); ?>
                        <BR>
                        <?php _e('or click on the button above to go to ', 'hypno'); ?>
                        <strong><?php _e('main page.', 'hypno'); ?></strong>
                    </p>
                    <?php wp_link_pages(); ?>
                </div>
            </div>
        </div>
    </div>



<?php get_footer(); ?>