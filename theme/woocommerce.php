<?php get_header();

if (is_front_page()) { get_template_part('slider'); }


    //returns shop page post object
    $shop = get_post( wc_get_page_id( 'shop' ) );


    // Check if user specified alternative page title
    if ((esc_attr($shop->synergy_page_enable_alternative_title) ==  1)
       && (esc_attr($shop->synergy_page_alternative_title) !==  "")
    ) {
        $title = $shop->synergy_page_alternative_title;
    }

    $sidebar = esc_attr($shop->synergy_page_sidebar);
    $sidebar_id = esc_attr($shop->synergy_page_sidebar_id);

    $active_menu = esc_attr($shop->synergy_page_active_menu_item);

    $header_type = esc_attr($shop->synergy_page_header_type);
    $include_slider = esc_attr($shop->synergy_page_fw_slider);
    $slider_id = esc_attr($shop->synergy_page_slider_shortcode);
    $image_src = "";
    $url = "";
    $images = $shop->synergy_page_header_image;
    if ($images) {
        $image_src = wp_get_attachment_image_src($images[0], 'full');
        $url = esc_url($image_src[0]);
    }

    $header_bg = esc_attr($shop->synergy_page_header_bg_color);
    $heading_color = esc_attr($shop->synergy_page_heading_font_color);

    $bg_color = esc_attr($shop->synergy_page_bg_color);


    // fetch breadcrums settings
    $breadcrums = esc_attr($shop->synergy_page_breadcrums);
    $breadcrums_border = esc_attr($shop->synergy_page_breadcrums_border);
    $breadcrums1 = esc_attr($shop->synergy_page_breadcrums1_color);
    $breadcrums2 = esc_attr($shop->synergy_page_breadcrums2_color);
    $breadcrums_home = esc_attr($shop->synergy_page_breadcrums_home);


?>

    <?php
        if  (($include_slider == 1) && ($slider_id != '')){
            echo '<div class="header">';
            putRevSlider(esc_attr($slider_id));
            echo '</div>';
        } else { ?>

            <div class="page-header woo-header padding-top-100 padding-bottom-100"  <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 align-center wow fadeIn">
                            <h1 style="color: <?php echo $heading_color; ?>"><?php if ($title) {echo $title;} else {woocommerce_page_title();} ?></h1>
                            <?php
                                if ($breadcrums == 1) {
                                    woocommerce_breadcrumb();
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>



        <?php
        }?>

    <div class="page-content <?php if (!is_front_page()) { ?>padding-top-50<?php }?> padding-bottom-50" style="background-color: <?php echo $bg_color;?>">
        <div class="container">
            <div class="row">
                <?php if ($sidebar == 0) { ?>
                <div class="col-md-12 clearfix">
                    <?php } else { ?>
                    <div class="col-md-9 clearfix">
                <?php }  ?>
                <?php woocommerce_content(); ?>
                </div>
                    <?php if ($sidebar == 1) { ?>
                        <div class="col-md-3 sidebar clearfix">
                        <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar($sidebar_id) ) ?>
                        </div>
                    <?php }  ?>
            </div>
        </div>
    </div>


<?php

if ($active_menu != "") {
    ?>
    <script>
        jQuery('.mega-menu-item').each(function() {
            var mega_mnu_item = 'mega-menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mega_mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });

        jQuery('.menu-item').each(function() {
            var mnu_item = 'menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });
    </script>
<?php } ?>
<?php get_footer(); ?>