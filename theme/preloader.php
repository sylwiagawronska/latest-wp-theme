<?php
global $hypno_options;
if ((array_key_exists('preloader-enabled', $hypno_options) && $hypno_options['preloader-enabled'] == 1 )) {
    if ((array_key_exists('preloader-pages', $hypno_options) && $hypno_options['preloader-pages'] === 'main' )) {
        if (is_front_page()) {?>
            <div class="preloader"></div>
        <?php } } else { ?>
        <div class="preloader"></div>
    <?php }
}
?>