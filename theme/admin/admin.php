<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
    $hypno_options = array();
    return;
}


// This is your option name where all the Redux data is stored.
$opt_name = "hypno_options";

// This line is only for altering the demo. Can be easily removed.
//$opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

/*
 *
 * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
 *
 */

$sampleHTML = '';
if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
    Redux_Functions::initWpFilesystem();

    global $wp_filesystem;

    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
}

// Background Patterns Reader
$sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
$sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
$sample_patterns      = array();

if ( is_dir( $sample_patterns_path ) ) {

    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
        $sample_patterns = array();

        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                $name              = explode( '.', $sample_patterns_file );
                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                $sample_patterns[] = array(
                    'alt' => $name,
                    'img' => $sample_patterns_url . $sample_patterns_file
                );
            }
        }
    }
}

$revolutionslider = array();
$revolutionslider[0] = 'None';

if(class_exists('RevSlider')){
    $slider = new RevSlider();
    $arrSliders = $slider->getArrSliders();
    foreach($arrSliders as $revSlider) {
        $revolutionslider[$revSlider->getAlias()] = $revSlider->getTitle();
    }
}

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name'             => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name'         => $theme->get( 'Name' ),
    // Name that appears at the top of your panel
    'display_version'      => $theme->get( 'Version' ),
    // Version that appears at the top of your panel
    'menu_type'            => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu'       => true,
    // Show the sections below the admin menu item or not
    'menu_title'           => __( 'Hypno Options', 'hypno' ),
    'page_title'           => __( 'Hypno Options', 'hypno' ),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key'       => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography'     => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar'            => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon'       => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority'   => 50,
    // Choose an priority for the admin bar menu
    'global_variable'      => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode'             => false,
    // Show the time the page took to load, etc
    'update_notice'        => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer'           => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority'        => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent'          => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions'     => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon'            => '',
    // Specify a custom URL to an icon
    'last_tab'             => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon'            => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug'            => '',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults'        => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show'         => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark'         => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export'   => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time'       => 60 * MINUTE_IN_SECONDS,
    'output'               => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag'           => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database'             => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn'              => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

    // HINTS
    'hints'                => array(
        'icon'          => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color'    => 'lightgray',
        'icon_size'     => 'normal',
        'tip_style'     => array(
            'color'   => 'red',
            'shadow'  => true,
            'rounded' => false,
            'style'   => '',
        ),
        'tip_position'  => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect'    => array(
            'show' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'mouseover',
            ),
            'hide' => array(
                'effect'   => 'slide',
                'duration' => '500',
                'event'    => 'click mouseleave',
            ),
        ),
    )
);


// Panel Intro text -> before the form
if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
    if ( ! empty( $args['global_variable'] ) ) {
        $v = $args['global_variable'];
    } else {
        $v = str_replace( '-', '_', $args['opt_name'] );
    }
   // $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'hypno' ), $v );
} else {
    //$args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'hypno' );
}

// Add content after the form.
$args['footer_text'] = __( '<p>Shall you have any questions / concerns, feel free to email our support at synergythemes@gmail.com </p>', 'hypno' );

Redux::setArgs( $opt_name, $args );

/*
 * ---> END ARGUMENTS
 */


/*
 * ---> START HELP TABS
 */

$tabs = array(
    array(
        'id'      => 'redux-help-tab-1',
        'title'   => __( 'Support Information', 'hypno' ),
        'content' => __( '<p>Shall you have any questions / concerns, feel free to email our support at synergythemes@gmail.com</p>', 'hypno' )
    ),
);
Redux::setHelpTab( $opt_name, $tabs );

// Set the help sidebar
$content = __( '<p>Shall you have any questions / concerns, feel free to email our support at synergythemes@gmail.com</p>', 'hypno' );
Redux::setHelpSidebar( $opt_name, $content );



// -> START Basic Settings Selection
Redux::setSection( $opt_name, array(
    'title' => __( 'Basic Settings', 'hypno' ),
    'id'    => 'hypno',
    'desc'       => __( 'Those are the most basic settings of Hypno There. Please configure according to your liking', 'hypno' ),
    'icon'  => 'el el-home',
     'fields'     => array(
    array(
        'id'       => 'primary-accent-color',
        'type'     => 'color',
        'transparent' => false,
        'validate' => 'color',
        // 'compiler'  => true,
        'title'    => __( 'Primary Accent Color', 'hypno' ),
        'desc'     => __( 'Pick a primary accent color for the theme, used for many components throughout the site', 'hypno' ),
        'default'  => '#1e1f20',
    ),
    array(
        'id'       => 'secondary-accent-color',
        'type'     => 'color',
        'transparent' => false,
        'validate' => 'color',
        //'compiler'  => true,
        'title'    => __( 'Secondary Accent Color', 'hypno' ),
        'desc'     => __( 'Pick a primary accent color for the theme, used for some components throughout the site', 'hypno' ),
        'default'  => '#cbcbcb',
        'validate' => 'color',
    ),

    array(
        'id'       => 'preloader-enabled',
        'type'     => 'switch',
        'title'    => 'Enable Page Preloader',
        'subtitle' => 'Click <code>On</code> to enable page preloader.',
        'default'  => false
    ),

    array(
        'id'       => 'preloader-logo',
        'type'     => 'media',
        'desc'     => __( 'Pick a preloader logo image. Leave empty in you do not wish do display a logo', 'hypno' ),
        'title'    => __( 'Preloader Logo', 'hypno' ),
        'required' => array( 'preloader-enabled', '=', true ),
    ),

    array(
        'id'       => 'preloader-bg',
        'type'     => 'color',
        'validate' => 'color',
        // 'compiler'  => true,
        'title'    => __( 'Preloader background color', 'hypno' ),
        'default'  => '#ffffff',
        'required' => array( 'preloader-enabled', '=', true ),
        'transparent' => false,
    ),

    array(
        'id'       => 'preloader-pages',
        'type'     => 'select',
        'title'    => __( 'Enable preloader on:', 'hypno' ),
        'desc'     => __( 'Define whether the preloader should launch on index page only, or on all pages', 'hypno' ),
        //Must provide key => value pairs for select options
        'options'  => array(
            'all' => __( 'All pages', 'hypno' ),
            'main' => __( 'Main page only', 'hypno' ),
        ),
        'default'  => 'all',
        'required' => array( 'preloader-enabled', '=', true )
    ),

    array(
        'id'       => 'slider-enabled',
        'type'     => 'switch',
        'title'    => 'Enable Home Page Slider',
        'subtitle' => 'Click <code>On</code> to enable Revolution Slider on main page',
        'default'  => false
    ),

    array(
        'id'       => 'slider',
        'type'     => 'select',
        'title'    => 'Select Main Slider',
        'subtitle' => 'Select a slider to display',
        'options'  => $revolutionslider,
        'default'  => $revolutionslider[0],
        'select2'  => array( 'allowClear' => false ),
        'required' => array( 'slider-enabled', '=', true )
    ),

    array(
        'id'       => 'woocommerce-columns',
        'type'     => 'select',
        'title'    => __( 'Woocommerce Columns', 'hypno' ),
        // 'compiler' => true,
        'subtitle' => __( 'Amount of products to be displayed in a row of your Woocommerce store', 'hypno' ),
        //Must provide key => value pairs for select options
        'options'  => array(
            '2' => '2',
            '3' => '3',
            '4' => '4',
        ),
        'default'  => '3'
    ),



),
) );

// -> START Navigation Selection
Redux::setSection( $opt_name, array(
    'title'            => __( 'Navigation', 'hypno' ),
    'id'               => 'navigation',
    'customizer_width' => '500px',
    'icon'             => 'el el-wrench',
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Navbar', 'hypno' ),
    'id'         => 'navigation-Navbar',
    'subsection' => true,
    'desc'       => __( 'Those are the settings for everything related the top nav bar (excluding the menu itself)', 'hypno' ),
    'fields'     => array(

        array(
            'id'       => 'navbar-background',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title'    => __( 'Navigation bar background color', 'hypno' ),
            'default'  => '#ffffff',
        ),

        array(
            'id'       => 'logo',
            'type'     => 'media',
            // 'compiler'  => true,
            'title'    => __( 'Logo', 'hypno' ),
            'desc'     => __( 'Main logo image ', 'hypno' ),
            'subtitle' => __( 'Upload any image using the WordPress native uploader', 'hypno' ),
        ),

        array(
            'id'       => 'logo-right-border',
            'type'     => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title'    => __( 'Right border color for logo', 'hypno' ),
            'default'  => '#e6e6e7',
        ),


        array(
            'id'       => 'favicon',
            'type'     => 'media',
            'title'    => __( 'Favicon', 'hypno' ),
            'subtitle' => __( 'Upload a favicon of your website', 'hypno' ),
        ),



        array(
            'id'       => 'enable-header-social-bar',
            'type'     => 'switch',
            'title'    => 'Enable Header Social Bar',
            'desc'     => __( 'Determines whether a bar with social profiles should be displayed in the navbar (on the right side of navigation menu)', 'hypno' ),
            'default'  => true
        ),

        array(
            'id'       => 'social-header',
            'required' => array( 'enable-header-social-bar', '=', true ),
            'type'     => 'select',
            'multi'    => true,
            'title'    => __( 'Social Header Icons', 'hypno' ),
            'subtitle' => __( 'Links to be displayed in social area in header (up to 6 is recommended)', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            //'required' => array( 'opt-select', 'equals', array( '1', '3' ) ),
            'default'  => array( 'fa-facebook', 'fa-twitter' )
        ),


        array(
            'id'       => 'navbullet',
            'type'     => 'media',
            // 'compiler'  => true,
            'title'    => __( 'Navigation bullet', 'hypno' ),
            'desc'     => __( 'Upload a bullet image to be used in main navigation (normal state). Leave empty for no bullet. Default image is included in /images/ folder of the theme.', 'hypno' ),
            'subtitle' => __( 'Upload bullet image using the WordPress native uploader', 'hypno' ),
        ),
        array(
            'id'       => 'navbullet-hover',
            'type'     => 'media',
            // 'compiler'  => true,
            'title'    => __( 'Navigation bullet : hover', 'hypno' ),
            'desc'     => __( 'Upload a bullet image to be used in main navigation (hover state). Leave empty for no bullet. Default image is included in /images/ folder of the theme. ', 'hypno' ),
            'subtitle' => __( 'Upload bullet image using the WordPress native uploader', 'hypno' ),
        ),
        array(
            'id'        => 'upper-block',
            'type'      => 'text',
            'title'     => __( 'Bold text in header', 'hypno' ),
            'desc'      => __( 'This text will be displayed on the right side of the logo. Leave empty for no text', 'hypno' ),
            'default'   => 'creative theme',
        ),
        array(
            'id'        => 'lower-block',
            'type'      => 'text',
            'title'     => __( 'Small in header', 'hypno' ),
            'desc'      => __( 'This text will be displayed on the right side of the logo. Leave empty for no text', 'hypno' ),
            'default'   => 'meeting your needs',
        ),

        array(
            'id'       => 'woocommerce-header-cart',
            'type'     => 'switch',
            'title'    => 'Show Woocommerce cart (item count / total amount) in header',
            'default'  => false
        ),


    )
));
Redux::setSection( $opt_name, array(
    'title'      => __( 'Menu', 'hypno' ),
    'id'         => 'navigation-Menu',
    'desc'       => __( 'Settings for main menu navigation. Please keep in mind, that if you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
    'subsection' => true,
    'fields'     => array(


        array(
            'id'       => 'hypno-menu-font',
            'type'     => 'typography',
            'title'    => __( 'Menu Font', 'hypno' ),
            'subtitle' => __( 'Specify the menu font properties.', 'hypno' ),
            'google'   => true,
            // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'default'  => array(
                'google'      => true,
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-size'     => '15px',
                'font-weight'   => 700
            ),
        ),

        array(
            'id'       => 'hypno-submenu-font',
            'type'     => 'typography',
            'title'    => __( 'Submenu Font', 'hypno' ),
            'subtitle' => __( 'Specify the submenu font properties.', 'hypno' ),
            'google'   => true,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'font-weight' => false,
            'text-align' => false,
            'default'  => array(
                'google'      => true,
                'color'       => '#000000',
                'font-family' => 'Arial,Helvetica,sans-serif',
                'font-size'     => '15px'
            ),
        ),


        array(
            'id'       => 'dropdown-size',
            'type'     => 'text',
            'title'    => __( 'Submenu Dropdown width size', 'hypno' ),
            'subtitle' => __( 'This must be numeric (in px). 250 is recommended', 'hypno' ),
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'validate' => 'numeric',
            'default'  => '250',
        ),

        array(
            'id'       => 'hamburger-menu-color',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'title'    => __( 'Hamburger Menu Color', 'hypno' ),
            'default'  => '#cbcbcb',
        ),

        array(
            'id'       => 'submenu-item-hover-color',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'title'    => __( 'Submenu font color on hover', 'hypno' ),
            'default'  => '#ffffff',
        ),

        array(
            'id'       => 'submenu-item-background',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'title'    => __( 'Submenu background color', 'hypno' ),
            'default'  => '#ffffff',
        ),

        array(
            'id'       => 'submenu-item-hover-background',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'title'    => __( 'Submenu background color on hover', 'hypno' ),
            'default'  => '#1e1f20',
        ),

        array(
            'id'       => 'submenu-dropbown-bottom-border',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'title'    => __( 'Submenu dropdown top border color', 'hypno' ),
            'default'  => '#1e1f20',
        ),

        array(
            'id'       => 'submenu-item-bottom-border',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'desc'     => __( 'If you are using Max Mega Menu plugin, the typography settings can be set via Mega Menu > Menu Themes', 'hypno' ),
            'title'    => __( 'Submenu item bottom border color', 'hypno' ),
            'default'  => '#dcdedf',
        ),


    )
));


// -> START Typography Selection
Redux::setSection( $opt_name, array(
    'title'      => __( 'Typography', 'hypno' ),
    'id'         => 'typography',
    'desc'       => __( 'All settings related to site typography', 'hypno' ),
    'icon'             => 'el el-font',
    'fields'     => array(

        array(
            'id'       => 'hypno-typography-body',
            'type'     => 'typography',
            'title'    => __( 'Body Font', 'hypno' ),
            'subtitle' => __( 'Specify the body font properties.', 'hypno' ),
            'google'   => true,
            'text-align' => false,
           // 'compiler'  => true,
            'default'  => array(
                'color'       => '#4f4f4f',
                'font-size'   => '14px',
                'font-family' => 'Arial,Helvetica,sans-serif',
                'font-weight' => 400,
                'line-height' => '17px',
            ),
        ),

        array(
            'id'        => 'stroke-text',
            'type'      => 'text',
            'title'     => __( 'Body text stroke', 'hypno' ),
            'default'   => '0.2px rgba(0, 0, 0, 0.5)',
            'text_hint' => array(
                'title'   => 'Type NONE for no stroke',
                'content' => 'Value should be X.Xpx rgba(x,x,x,0.x), e.g. 0.2px rgba(0, 0, 0, 0.5)'
            )
        ),

        array(
            'id'       => 'hypno-typography-heading',
            'type'     => 'typography',
            'title'    => __( 'General Headings Font', 'hypno' ),
            'subtitle' => __( 'Specify the headings font properties.', 'hypno' ),
            'google'   => true,
           // 'compiler'  => true,
            'all_styles'  => true,
            'font-weight' => false,
            'line-height' => false,
            'font-size' => false,
            'text-align' => false,
            'default'  => array(
                'google'      => true,
                'color'       => '#000000',
                'font-family' => 'Roboto',
            ),
        ),

        array(
            'id'       => 'hypno-typography-h1',
            'type'     => 'typography',
            'title'    => __( 'H1 Font', 'hypno' ),
            'subtitle' => __( 'Specify the H1 font properties.', 'hypno' ),
            'google'   => true,
           // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'default'  => array(
                'google'      => true,
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-weight' => 700,
                'font-size' => '46px',
            ),
        ),


        array(
            'id'       => 'hypno-typography-h2',
            'type'     => 'typography',
            'title'    => __( 'H2 Font', 'hypno' ),
            'subtitle' => __( 'Specify the H2 font properties.', 'hypno' ),
            'google'   => true,
           // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'default'  => array(
                'google'      => true,
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-weight' => 700,
                'font-size' => '40px',
            ),
        ),

        array(
            'id'       => 'hypno-typography-h3',
            'type'     => 'typography',
            'title'    => __( 'H3 Font', 'hypno' ),
            'subtitle' => __( 'Specify the H3 font properties.', 'hypno' ),
            'google'   => true,
            // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'default'  => array(
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-weight' => 700,
                'font-size' => '32px',
            ),
        ),


        array(
            'id'       => 'hypno-typography-h4',
            'type'     => 'typography',
            'title'    => __( 'H4 Font', 'hypno' ),
            'subtitle' => __( 'Specify the H4 font properties.', 'hypno' ),
            'google'   => true,
            // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'default'  => array(
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-weight' => 700,
                'font-size' => '24px',
            ),
        ),

        array(
            'id'       => 'hypno-typography-h5',
            'type'     => 'typography',
            'title'    => __( 'H5 Font', 'hypno' ),
            'subtitle' => __( 'Specify the H5 font properties.', 'hypno' ),
            'google'   => true,
            // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'default'  => array(
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-weight' => 700,
                'font-size' => '20px',
            ),
        ),

        array(
            'id'       => 'hypno-typography-h6',
            'type'     => 'typography',
            'title'    => __( 'H6 Font', 'hypno' ),
            'subtitle' => __( 'Specify the H6 font properties.', 'hypno' ),
            'google'   => true,
            // 'compiler'  => true,
            'all_styles'  => true,
            'line-height' => false,
            'text-align' => false,
            'default'  => array(
                'color'       => '#000000',
                'font-family' => 'Roboto',
                'font-weight' => 700,
                'font-size' => '18px',
            ),
        ),

        array(
            'id'       => 'footnote-font-size',
            'type'     => 'text',
            'title'    => __( 'Footnote font size', 'hypno' ),
            'desc'       => __( 'Footnotes are small texts used in various components, e.g. round progress bars or the small text in the navbar, next to the logo', 'hypno' ),
            'subtitle' => __( 'This must be numeric (in px). 11 is recommended', 'hypno' ),
            'validate' => 'numeric',
            'default'  => '11',
        ),

        array(
            'id'       => 'blockquote-font-size',
            'type'     => 'text',
            'title'    => __( 'Blockquote font size', 'hypno' ),
            'subtitle' => __( 'This must be numeric (in px). 16 is recommended', 'hypno' ),
            'validate' => 'numeric',
            'default'  => '16',
        ),
    )
) );

// -> START Colors Selection
Redux::setSection( $opt_name, array(
    'title'      => __( 'Colors', 'hypno' ),
    'desc'       => __( 'Everything related to color schemes', 'hypno' ),
    'id'         => 'colors',
    'icon'             => 'el el-brush',
    'fields'     => array(


        array(
            'id'       => 'background-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Body background color', 'hypno' ),
            'desc'       => __( 'General body background (useful for posts etc. - page backgrounds can be set individually per page', 'hypno' ),
            'default'  => '#ffffff',
            'validate' => 'color',
        ),


        array(
            'id'       => 'error-page-404',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Error page 404 shadow color', 'hypno' ),
            'desc'       => __( 'The color of "404" shadow in error page. Very light color is recommended', 'hypno' ),
            'default'  => '#f7f7f7',
            'validate' => 'color',
        ),

        array(
            'id'       => 'footnote-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Footnotes color', 'hypno' ),
            'desc'       => __( 'Pick a color for footnotes [small text at the bottom of more important text] located throughout the site. Light grey is recommended', 'hypno' ),
            'default'  => '#b6b6b6',
            'validate' => 'color',
        ),

        array(
            'id'       => 'light-border',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Light borders', 'hypno' ),
            'desc'       => __( 'Pick a color for light borders, e.g. for forms and inputs. Light grey is recommended', 'hypno' ),
            'default'  => '#dddddd',
            'validate' => 'color',
        ),

        array(
            'id'       => 'dark-border',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Dark borders', 'hypno' ),
            'desc'       => __( 'Pick a color for light borders, e.g. for forms and inputs for dark scheme / footer. Dark grey is recommended', 'hypno' ),
            'default'  => '#333333',
            'validate' => 'color',
        ),


        array(
            'id'       => 'dark-section-bg-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Dark elements background', 'hypno' ),
            'desc'       => __( 'Pick a color for dark backgrounds of default elements, e.g. default "read more" buttons, portfolio navigation etc. Also used for WooCommerce buttons / spans. Dark grey / black-ish is recommended', 'hypno' ),
            'default'  => '#1e1f20',
            'validate' => 'color',
        ),


        array(
            'id'       => 'light-grey-bg-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Light elements background', 'hypno' ),
            'desc'       => __( 'Pick a color for light backgrounds of default elements (mainly used in shortcodes). Light grey is recommended', 'hypno' ),
            'default'  => '#f4f4f4',
            'validate' => 'color',
        ),


        array(
            'id'       => 'lighter-grey-bg-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Lighter elements background', 'hypno' ),
            'desc'       => __( 'Pick a color for light backgrounds of default elements (e.g. blog post quotes). Very light grey is recommended', 'hypno' ),
            'default'  => '#f8f8f8',
            'validate' => 'color',
        ),

        array(
            'id'       => 'medium-grey-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Light grey elements', 'hypno' ),
            'desc'       => __( 'Pick a color for medium-dark backgrounds of default elements, e.g. medium-buttons, services, tabs, counters. Medium grey is recommended', 'hypno' ),
            'default'  => '#cbcbcb',
            'validate' => 'color',
        ),

        array(
            'id'       => 'light-text',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Light grey text', 'hypno' ),
            'desc'       => __( 'Pick a color for light font color of default elements, e.g. step boxes, woocommerce icons etc. Light grey is recommended', 'hypno' ),
            'default'  => '#cbcbcb',
            'validate' => 'color',
        ),


        array(
            'id'       => 'dark-text',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'Dark text', 'hypno' ),
            'desc'       => __( 'Default colors of dark scheme elements (mostly shortcodes). Very dark / black-ish color is recommended', 'hypno' ),
            'default'  => '#1e1f20',
            'validate' => 'color',
        ),


        array(
            'id'       => 'white-bg-color',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'White backgrounds', 'hypno' ),
            'desc'       => __( 'Some white backgrounds used in the theme that should stay white (mostly on.hoover colors). You can change it to another light color if you wish', 'hypno' ),
            'default'  => '#ffffff',
            'validate' => 'color',
        ),

        array(
            'id'       => 'white-text',
            'type'     => 'color',
            'transparent' => false,
            'title'    => __( 'White text', 'hypno' ),
            'desc'       => __( 'Some white texts used in the theme that should stay white (mostly on.hoover colors). You can change it to another light color if you wish', 'hypno' ),
            'default'  => '#ffffff',
            'validate' => 'color',
        ),
    )

));


// -> START Social Profiles Selection
Redux::setSection( $opt_name, array(
    'title'      => __( 'Social Profiles', 'hypno' ),
    'id'         => 'social',
    'desc'       => __( 'Fill up the URLs of the social profiles you wish to link to in your website', 'hypno' ),
    'icon'             => 'el el-thumbs-up',
    'fields'     => array(


        array(
            'id'       => 'fa-behance',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Behance Url'
        ),
        array(
            'id'       => 'fa-bitbucket',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Bitbucket Url'
        ),
        array(
            'id'       => 'fa-codepen',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Codepen Url'
        ),
        array(
            'id'       => 'fa-delicious',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Delicious Url'
        ),
        array(
            'id'       => 'fa-deviantart',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Deviantart Url'
        ),
        array(
            'id'       => 'fa-digg',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Digg Url'
        ),
        array(
            'id'       => 'fa-dribbble',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Dribbble Url'
        ),
        array(
            'id'       => 'fa-facebook',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Facebook Url'
        ),
        array(
            'id'       => 'fa-flickr',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Flickr Url'
        ),
        array(
            'id'       => 'fa-github',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Github Url'
        ),
        array(
            'id'       => 'fa-google-plus',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Google+ Url'
        ),
        array(
            'id'       => 'fa-instagram',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Instagram Url'
        ),
        array(
            'id'       => 'fa-linkedin',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Linkedin Url'
        ),
        array(
            'id'       => 'fa-lastfm',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Lastfm Url'
        ),
        array(
            'id'       => 'fa-pinterest',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Pinterest Url'
        ),
        array(
            'id'       => 'fa-skype',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Skype Url'
        ),
        array(
            'id'       => 'fa-soundcloud',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Soundcloud Url'
        ),
        array(
            'id'       => 'fa-spotify',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Spotify Url'
        ),
        array(
            'id'       => 'fa-stack-overflow',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Stack-overflow Url'
        ),
        array(
            'id'       => 'fa-stumbleupon',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Stumbleupon Url'
        ),
        array(
            'id'       => 'fa-tumblr',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Tumblr Url'
        ),
        array(
            'id'       => 'fa-twitch',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Twitch Url'
        ),
        array(
            'id'       => 'fa-twitter',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Twitter Url'
        ),
        array(
            'id'       => 'fa-vimeo',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Vimeo Url'
        ),
        array(
            'id'       => 'fa-youtube',
            'type'     => 'text',
            'default'  => '#',
            'title'    => 'Youtube Url'
        ),


    )

));

// -> START Footer Selection
Redux::setSection( $opt_name, array(
    'title'      => __( 'Footers', 'hypno' ),
    'id'         => 'footers',
    'desc'       => __( 'Everything related to footers', 'hypno' ),
    'icon'             => 'el el-list-alt',
    'fields'     => array(

        array(
            'id'       => 'enable-lower-footer',
            'type'     => 'switch',
            'desc'       => __( 'Enable lower footer area - this area will either contain a footer logo or footer widhets', 'hypno' ),
            'title'    => 'Enable Lower Footer',
            'default'  => true
        ),

        array(
            'id'       => 'lower-footer-bg-color',
            'type'     => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title'    => __( 'Lower Footer Background Color', 'hypno' ),
            'required' => array( 'enable-lower-footer', '=', true ),
            'default'  => '#1e1f20',
        ),

        array(
            'id'       => 'lower-footer-text-color',
            'type'     => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title'    => __( 'Lower Footer Text Color', 'hypno' ),
            'required' => array( 'enable-lower-footer', '=', true ),
            'default'  => '#aaaaaa',
        ),

        array(
            'id'       => 'lower-footer-heading-color',
            'type'     => 'color',
            'transparent' => false,
            'validate' => 'color',
            'title'    => __( 'Lower Footer Headlines Color', 'hypno' ),
            'required' => array( 'enable-lower-footer', '=', true ),
            'default'  => '#ffffff',
        ),


        array(
            'id'       => 'lower-footer-type',
            'required' => array( 'enable-lower-footer', '=', true ),
            'type'     => 'select',
            'multi'    => false,
            'desc'       => __( 'Define if you wish do display footer widgets, or just a logo', 'hypno' ),
            'title'    => __( 'Lower Footer Type', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'widgets' => 'Widgets',
                'logo' => 'Logo only',
            ),
            //'required' => array( 'opt-select', 'equals', array( '1', '3' ) ),
            'default'  => 'logo'
        ),

        array(
            'id'       => 'footer-logo',
            'required' => array( 'lower-footer-type', '=', 'logo' ),
            'type'     => 'media',
            'title'    => __( 'Footer Logo', 'hypno' ),
            'subtitle' => __( 'Upload footer logo', 'hypno' ),
        ),

        array(
            'id'       => 'footer-widgets-count',
            'type'     => 'select',
            'title'    => __( 'Widget columns count', 'hypno' ),
            'subtitle' => __( 'Amount of columns in which footer widgets will be displayed', 'hypno' ),
            'required' => array( 'lower-footer-type', '=', 'widgets' ),
            //Must provide key => value pairs for select options
            'options'  => array(
                '2' => '2',
                '3' => '3',
                '4' => '4',
            ),
            'default'  => '4'
        ),



        array(
            'id'       => 'enable-footer-social-bar',
            'type'     => 'switch',
            'title'    => 'Enable Footer Social Bar (Upper Footer)',
            'default'  => true
        ),

        array(
            'id'       => 'social-footer-bg-color',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title'    => __( 'Footer Social Section Background Color', 'hypno' ),
            'required' => array( 'enable-footer-social-bar', '=', true ),
            'default'  => '#ffffff',
        ),

        array(
            'id'       => 'social-footer-icons-color',
            'type'     => 'color',
            'validate' => 'color',
            'transparent' => false,
            'title'    => __( 'Footer Social Section Icons Color', 'hypno' ),
            'required' => array( 'enable-footer-social-bar', '=', true ),
            'default'  => '#000000',
        ),

        array(
            'id'       => 'social-footer-icons-count',
            'type'     => 'select',
            'title'    => __( 'Footer Icons Amount', 'hypno' ),
            'subtitle' => __( 'Amount of icons to be displayed in a a footer social area', 'hypno' ),
            'required' => array( 'enable-footer-social-bar', '=', true ),
            //Must provide key => value pairs for select options
            'options'  => array(
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '6' => '6'
            ),
            'default'  => '4'
        ),

        array(
            'id'       => 'social-footer-1',
            'required' => array( 'social-footer-icons-count', '>=', 2 ),
            'type'     => 'select',
            'multi'    => false,
            'title'    => __( 'First Footer Social Icon', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            'default'  => array( 'fa-behance' )
        ),



        array(
            'id'       => 'social-footer-2',
            'required' => array( 'social-footer-icons-count', '>=', 2 ),
            'type'     => 'select',
            'multi'    => false,
            'title'    => __( 'Second Footer Social Icon', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            'default'  => array( 'fa-twitter' )
        ),


        array(
            'id'       => 'social-footer-3',
            'required' => array( 'social-footer-icons-count', '>=', 3 ),
            'type'     => 'select',
            'multi'    => false,
            'title'    => __( 'Third Footer Social Icon', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            'default'  => array( 'fa-linkedin' )
        ),

        array(
            'id'       => 'social-footer-4',
            'required' => array( 'social-footer-icons-count', '>=', 4 ),
            'type'     => 'select',
            'multi'    => false,
            'title'    => __( 'Fourth Footer Social Icon', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            'default'  => array( 'fa-youtube' )
        ),

        array(
            'id'       => 'social-footer-5',
            'required' => array( 'social-footer-icons-count', '>=', 6 ),
            'type'     => 'select',
            'multi'    => false,
            'title'    => __( 'Fifth Footer Social Icon', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            'default'  => array( 'fa-twitter' )
        ),

        array(
            'id'       => 'social-footer-6',
            'required' => array( 'social-footer-icons-count', '>=', 6 ),
            'type'     => 'select',
            'multi'    => false,
            'title'    => __( 'Sixth Footer Social Icon', 'hypno' ),
            //Must provide key => value pairs for radio options
            'options'  => array(
                'fa-behance' => 'Behance',
                'fa-bitbucket' => 'Bitbucket',
                'fa-codepen' => 'Codepen',
                'fa-delicious' => 'Delicious',
                'fa-deviantart' => 'Deviantart',
                'fa-digg' => 'Digg',
                'fa-dribbble' => 'Dribbble',
                'fa-facebook' => 'Facebook',
                'fa-flickr' => 'Flickr',
                'fa-github' => 'Github',
                'fa-google-plus' => 'Google+',
                'fa-instagram' => 'Instagram',
                'fa-linkedin' => 'Linkedin',
                'fa-lastfm' => 'Lastfm',
                'fa-pinterest' => 'Pinterest',
                'fa-skype' => 'Skype',
                'fa-soundcloud' => 'Soundcloud',
                'fa-spotify' => 'Spotify',
                'fa-stack-overflow' => 'Stack-Overflow',
                'fa-stumbleupon' => 'Stumbleupon',
                'fa-tumblr' => 'Tumblr',
                'fa-twitch' => 'Twitch',
                'fa-twitter' => 'Twitter',
                'fa-vimeo' => 'Vimeo',
                'fa-youtube' => 'Youtube',
            ),
            'default'  => array( 'fa-vimeo' )
        ),


    )


));

// -> START Custom Code Selection
Redux::setSection( $opt_name, array(
    'title'            => __( 'Custom Code', 'hypno' ),
    'id'               => 'editor',
    'customizer_width' => '500px',
    'icon'             => 'el el-edit',
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Custom CSS', 'hypno' ),
    'id'         => 'editor-css',
    //'icon'  => 'el el-home'
    'desc'       => __( 'Here you can enter your custom CSS', 'hypno' ),
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'include-css',
            'type'     => 'switch',
            'title'    => 'Include custom CSS',
            'subtitle' => 'Click <code>On</code> to include the code below to your pages.',
            'default'  => false
        ),


        array(
            'id'       => 'custom-css',
            'type'     => 'ace_editor',
            'title'    => __( 'Custom CSS Code', 'hypno' ),
            'subtitle' => __( 'Paste your custom CSS code here. It will be included to the top of the pages', 'hypno' ),
            'mode'     => 'css',
            'theme'    => 'monokai',
            'default'  => "#placeholder{\n   margin: 0 auto;\n}"
        ),



    )
) );
Redux::setSection( $opt_name, array(
    'title'      => __( 'Custom Javascript', 'hypno' ),
    'id'         => 'editor-js',
    'desc'       => __( 'Here you can enter your custom Javascript code. Make sure your code works properly and does not produce any errors / warnings', 'hypno' ),
    //'icon'  => 'el el-home'
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'include-js',
            'type'     => 'switch',
            'title'    => 'Include custom Javascript',
            'subtitle' => 'Click <code>On</code> to include the code below to your pages.',
            'default'  => false
        ),

        array(
            'id'       => 'custom-js',
            'type'     => 'ace_editor',
            'title'    => __( 'Custom JS Code', 'hypno' ),
            'subtitle' => __( 'Paste your custom JS code here. It will be attached at the bottom of your pages', 'hypno' ),
            'mode'     => 'javascript',
            'default'  => "jQuery(document).ready(function(){\n\nconsole.log('Hello!');\n\n});"
        ),


    )
) );








if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
    $section = array(
        'icon'   => 'el el-list-alt',
        'title'  => __( 'Documentation', 'hypno' ),
        'fields' => array(
            array(
                'id'       => '17',
                'type'     => 'raw',
                'markdown' => true,
                'content_path' => dirname( __FILE__ ) . '/../README.md', // FULL PATH, not relative please
                //'content' => 'Raw content here',
            ),
        ),
    );
    Redux::setSection( $opt_name, $section );
}
/*
 * <--- END SECTIONS
 */


/*
 *
 * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
 *
 */

/*
*
* --> Action hook examples
*
*/

// If Redux is running as a plugin, this will remove the demo notice and links
//add_action( 'redux/loaded', 'remove_demo' );

// Function to test the compiler hook and demo CSS output.
// Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

// Change the arguments after they've been declared, but before the panel is created
//add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

// Change the default value of a field after it's been set, but before it's been useds
//add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

// Dynamically add a section. Can be also used to modify sections/fields
//add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

/**
 * This is a test function that will let you see when the compiler hook occurs.
 * It only runs if a field    set with compiler=>true is changed.
 * */

if ( ! function_exists( 'compiler_action' ) ) {
    function compiler_action( $options, $css, $changed_values )
    {
    }
}

/**
 * Custom function for the callback validation referenced above
 * */
if ( ! function_exists( 'redux_validate_callback_function' ) ) {
    function redux_validate_callback_function( $field, $value, $existing_value ) {
        $error   = false;
        $warning = false;

        //do your validation
        if ( $value == 1 ) {
            $error = true;
            $value = $existing_value;
        } elseif ( $value == 2 ) {
            $warning = true;
            $value   = $existing_value;
        }

        $return['value'] = $value;

        if ( $error == true ) {
            $field['msg']    = 'your custom error message';
            $return['error'] = $field;
        }

        if ( $warning == true ) {
            $field['msg']      = 'your custom warning message';
            $return['warning'] = $field;
        }

        return $return;
    }
}

/**
 * Custom function for the callback referenced above
 */
if ( ! function_exists( 'redux_my_custom_field' ) ) {
    function redux_my_custom_field( $field, $value ) {
        print_r( $field );
        echo '<br/>';
        print_r( $value );
    }
}

/**
 * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
 * Simply include this function in the child themes functions.php file.
 * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
 * so you must use get_template_directory_uri() if you want to use any of the built in icons
 * */
if ( ! function_exists( 'dynamic_section' ) ) {
    function dynamic_section( $sections ) {
        //$sections = array();
        $sections[] = array(
            'title'  => __( 'Section via hook', 'hypno' ),
            'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'hypno' ),
            'icon'   => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }
}

/**
 * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
 * */
if ( ! function_exists( 'change_arguments' ) ) {
    function change_arguments( $args ) {
        //$args['dev_mode'] = true;

        return $args;
    }
}

/**
 * Filter hook for filtering the default value of any given field. Very useful in development mode.
 * */
if ( ! function_exists( 'change_defaults' ) ) {
    function change_defaults( $defaults ) {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }
}

/**
 * Removes the demo link and the notice of integrated demo from the redux-framework plugin
 */
if ( ! function_exists( 'remove_demo' ) ) {
    function remove_demo() {
        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_filter( 'plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2 );

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
        }
    }
}

add_action ('redux/options/hypno_options/saved', 'compile_less');

function compile_less() {
    if (class_exists('Synergy')){

            Synergy::generateCSS();

    }
}