<?php get_header();
/*
 Template name: Portfolio Template
 */

if (have_posts()) : while (have_posts()) : the_post();

    // Check if user specified alternative page title
    if ((esc_attr(get_post_meta($post -> ID, "synergy_portfolio_enable_alternative_title", true)) ==  1)
        && (esc_attr(get_post_meta($post -> ID, "synergy_portfolio_alternative_title", true)) !==  "")
    ) {
        $title = (get_post_meta($post -> ID, "synergy_portfolio_alternative_title", true));
    } else {
        // if not, use default title
        $title = get_the_title();
    }

    $active_menu = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_active_menu_item", true));

    $background_color = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_bg_color", true));

    $show_all = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_show_all", true));

    $enable_load_more = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_enable_load_more", true));

    $initial_items = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_initial_number", true));
    $increment_items = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_increment_number", true));
    $load_more = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_load_more", true));

    $type = esc_attr(get_post_meta($post -> ID, "synergy_portfolio_type", true));

    ?>


    <div class="page-content padding-top-80 padding-bottom-40" style="background-color: <?php echo $background_color;?>">
        <div class="container">
            <div class="row padding-top-100 padding-bottom-50">
                <div class="col-md-5 padding-bottom-40 wow fadeIn">
                    <h1><?php echo $title; ?></h1>
                </div>
                <div class="col-md-7 padding-bottom-40 wow fadeIn portfolioFilters align-right">
                    <?php
                    $portfolio_categories = get_terms( array(
                        'taxonomy' => 'portfolio_categories',
                        'hide_empty' => false,
                    ) );
                    if ($portfolio_categories):?>
                        <?php foreach ($portfolio_categories as $portfolio_category):?>
                            <a href="#" data-filter-by="filter-<?php echo esc_attr($portfolio_category -> slug); ?>"><?php echo esc_attr($portfolio_category -> name); ?></a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <a href="#" data-filter-by="filter-all" class="active-filter"><?php echo $show_all; ?></a>
                </div>
            </div>

            <div class="row padding-top-40 portfolio-standalone <?php if ($enable_load_more == 1) {?>load-more-container<?php }?>" data-initial="<?php echo $initial_items;?>" data-increment="<?php echo $increment_items;?>">
                <?php
                $i=0;
                $query = new WP_Query('post_type=portfolio &posts_per_page=-1');
                if($query->have_posts()): while($query->have_posts()) : $query->the_post();
                $portfolio_title = get_the_title($post->id);
                $i++;

                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'portfolio-thumb' );
                $url = $thumb['0'];
                $content = get_the_content();

                $categories = get_the_terms( get_the_ID(), 'portfolio_categories' );
                $slug = '';
                if($categories) {
                    foreach ($categories as $category) {
                        $slug .= 'filter-';
                        $slug .= $category->slug;
                        $slug .= ' ';
                    }
                } else {
                    $slug = 'filter-none';
                }
                    if ($type == 'mixed') {
                        if ((($i - 2) % 3 == 0) ) { ?>
                            <div class="portfolio-item padding-bottom-50 col-md-4 col-sm-6 col-xs-12" data-filter='<?php echo esc_attr($slug); ?>'>
                                <div class="portfolio-picture margin-bottom-30" style="background-image:url(<?php echo esc_url($url); ?>)">

                                    <div class="portfolio-hover">
                                        <a href="<?php echo esc_url($url); ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
                                        <a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                                <div class="portfolio-description">
                                    <h4><?php echo $portfolio_title; ?></h4>

                                    <p>
                                        <?php echo wp_trim_words( $content , 20 ); ?>

                                    </p>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="portfolio-item padding-bottom-50 col-md-4 col-sm-6 col-xs-12" data-filter='<?php echo esc_attr($slug); ?>'>
                                <div class="portfolio-description">
                                    <h4><?php echo $portfolio_title; ?></h4>
                                    <p>
                                        <?php echo wp_trim_words( $content , 20 ); ?>
                                    </p>

                                </div>
                                <div class="portfolio-picture margin-top-30" style="background-image:url(<?php echo esc_url($url); ?>)">

                                    <div class="portfolio-hover">
                                        <a href="<?php echo esc_url($url); ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
                                        <a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                    <?php } } else if ($type == 'images_up') { ?>

                        <div class="portfolio-item padding-bottom-50 col-md-4 col-sm-6 col-xs-12" data-filter='<?php echo esc_attr($slug); ?>'>
                            <div class="portfolio-picture margin-bottom-30" style="background-image:url(<?php echo esc_url($url); ?>)">

                                <div class="portfolio-hover">
                                    <a href="<?php echo esc_url($url); ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
                                    <a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-description">
                                <h4><?php echo $portfolio_title; ?></h4>

                                <p>
                                    <?php echo wp_trim_words( $content , 20 ); ?>

                                </p>
                            </div>
                        </div>

                    <?php } else { ?>

                        <div class="portfolio-item padding-bottom-50 col-md-4 col-sm-6 col-xs-12" data-filter='<?php echo esc_attr($slug); ?>'>
                            <div class="portfolio-description">
                                <h4><?php echo $portfolio_title; ?></h4>
                                <p>
                                    <?php echo wp_trim_words( $content , 20 ); ?>
                                </p>

                            </div>
                            <div class="portfolio-picture margin-top-30" style="background-image:url(<?php echo esc_url($url); ?>)">

                                <div class="portfolio-hover">
                                    <a href="<?php echo esc_url($url); ?>" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>
                                    <a href="<?php the_permalink(); ?>"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                <?php
                endwhile;
                endif;
                ?>
            </div>
            <?php if ($enable_load_more == 1) {?>
            <div class="row padding-bottom-60 padding-top-40">
                <div class="col-md-12 align-center padding-top-20 padding-bottom-20">
                    <a href="#" role="button" id="loadMore" class="portfolio_loadMore btn btn-md"><?php echo $load_more; ?></a>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>


<?php
endwhile;
endif;

if ($active_menu != "") {
    ?>
    <script>
        jQuery('.mega-menu-item').each(function() {
            var mega_mnu_item = 'mega-menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mega_mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });

        jQuery('.menu-item').each(function() {
            var mnu_item = 'menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });
    </script>
<?php } ?>
<?php get_footer(); ?>