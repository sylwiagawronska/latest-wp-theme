<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>" role="search" class="search">
    <div class="form-group">
        <input type="text" class="field form-control" name="s" value="<?php echo esc_attr(get_search_query()); ?>" id="s" placeholder="<?php _e('type and press enter', 'hypno'); ?>">
        <button type="submit" class="search-button">
            <i class="fa fa-search"></i>
        </button>
    </div>
</form>
