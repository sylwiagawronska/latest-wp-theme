/**
 * Counters handler
 * @param $
 * @param counter
 * @constructor
 */
function Counters($, counter) {

    $(counter).each(function () {

        var me = $(this),
            countTo = $(this).attr('data-target-value');

        $(this).waypoint(function (event, direction) {

                $({countNum: me.text()}).animate({
                        countNum: countTo
                    },
                    {
                        duration: 3000,
                        easing: 'linear',
                        step: function () {
                            me.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            me.text(this.countNum);
                        }
                    });
            },
            {
                triggerOnce: true,
                offset: '90%'
            });

    });

}