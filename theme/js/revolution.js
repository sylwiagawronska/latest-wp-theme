/**
 * Revolution slider initialization
 * @param $
 * @param sliderContainer
 * @constructor
 */
function Revolution($, sliderContainer) {
    "use strict";

    $(sliderContainer).revolution({

        dottedOverlay : "none",
        delay : 9000,
        startwidth : 1170,
        startheight : 670,

        hideThumbs : 1,

        thumbWidth : 0,
        thumbHeight : 0,
        thumbAmount : 0,

        navigationType : "none",

        touchenabled : "on",
        onHoverStop : "on",

        swipe_velocity : 0.7,
        swipe_min_touches : 1,
        swipe_max_touches : 1,
        drag_block_vertical : false,

        lazyType:"none",
        parallax: {
            type:"mouse",
            origo:"slidercenter",
            speed:2000,
            levels:[11, 8, 7, 6, 9, 8, 7, 6, 5, 4],
            ddd_shadow:"off",
            ddd_bgfreeze:"on",
            ddd_overflow:"hidden",
            ddd_layer_overflow:"visible",
            ddd_z_correction:65
        },
        spinner:"off",

        keyboardNavigation : "off",

        navigationHAlign : "center",
        navigationVAlign : "bottom",
        navigationHOffset : 0,
        navigationVOffset : 20,

        soloArrowLeftHalign : "left",
        soloArrowLeftValign : "center",
        soloArrowLeftHOffset : 20,
        soloArrowLeftVOffset : 0,

        soloArrowRightHalign : "right",
        soloArrowRightValign : "center",
        soloArrowRightHOffset : 20,
        soloArrowRightVOffset : 0,

        shadow : 0,
        fullWidth : "on",
        fullScreen : "off",

        stopLoop : "off",
        stopAfterLoops : -1,
        stopAtSlide : -1,

        shuffle : "off",

        autoHeight : "off",
        forceFullWidth : "off",

        hideThumbsOnMobile : "off",
        hideNavDelayOnMobile : 1500,
        hideBulletsOnMobile : "off",
        hideArrowsOnMobile : "off",
        hideThumbsUnderResolution : 0,

        hideSliderAtLimit : 0,
        hideCaptionAtLimit : 0,
        hideAllCaptionAtLilmit : 0,
        startWithSlide : 0,
        videoJsPath : "plugins/revolution/videojs/",
        fullScreenOffsetContainer : ""
    });
}