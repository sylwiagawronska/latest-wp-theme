/**
 * Initialize tabs, so that they're compatibile with visual composer
 * @param $
 * @param tabContainer
 * @constructor
 */
function Tabs($, tabContainer) {

    var tabs = {

        init : function () {

            $(tabContainer).each(function(){

                var me = $(this);
                var scheme = me.attr('data-scheme');

                // initialize navigation container
                var navigation = $('<ul></ul>');
                navigation.addClass('nav nav-tabs ' + scheme);
                navigation.attr("role", "tablist");

                me.children().each(function(){
                    // add link to navigation
                    var el = tabs.addChild($(this));
                    el.appendTo(navigation);

                });

                // add navigation
                tabs.addNavigation(me,navigation);

            });

        },

        addNavigation : function(container, navigation) {
            // activate first tab
            container.children().first().addClass('in active');
            // insert navigation to the container
            navigation.insertBefore(container);

        },

        addChild : function(item) {

            var id = item.attr('id');
            var name = item.attr('data-name');

            // create navigation item
            var list_element = $('<li></li>');
            var link = $('<a>'+name+'</a>').attr('role', 'tab').attr('data-toggle', 'tab').attr('href', '#' + id);

            // activate first item
            if (item.is(':first-child'))
                list_element.addClass('active');

            link.appendTo(list_element);

            return list_element;
        }

    }



    tabs.init();
}