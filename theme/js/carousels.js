/**
 * Owl carousels handler
 * @param $
 * @param teamContainer
 * @param portfolioContainer
 * @param teamArrows
 * @param portfolioArrows
 * @param testimonials
 * @constructor
 */
function Carousels($, teamContainer, portfolioContainer, teamArrows, portfolioArrows, testimonials) {
    "use strict";

    $(teamContainer).owlCarousel({
        autoPlay : 5000,
        slideSpeed : 3000,
        pagination : false,
        transitionStyle : "fade",
        singleItem : true,
        navigation : true,
        navigationText : [teamArrows.prev, teamArrows.next],
        lazyLoad : true,
        lazyFollow : true,
        lazyEffect : "fade"
    });

    $(testimonials).owlCarousel({
        autoPlay : 5000,
        slideSpeed : 3000,
        pagination : true,
        transitionStyle : "fade",
        singleItem : true,
        navigation : false,
        lazyLoad : true,
        lazyFollow : true,
        lazyEffect : "fade"
    });

    $(portfolioContainer).owlCarousel({
        transitionStyle : "fade",
        items : 3,
        itemsDesktop : [1200,3],
        itemsDesktopSmall : [992, 2],
        itemsTablet : [768, 1],
        itemsTabletSmall : false,
        itemsMobile : false,
        navigation : true,
        pagination : false,
        navigationText : [portfolioArrows.prev, portfolioArrows.next],
        lazyLoad : true,
        lazyFollow : true,
        lazyEffect : "fade"
    });

}