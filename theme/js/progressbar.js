/**
 * Circle progress bars
 * @param $
 * @param charts
 * @param primaryColor
 * @param secondaryColor
 * @constructor
 */
function ProgressBar($, charts, primaryColor, secondaryColor) {
    "use strict";

    $(charts.chartRegularContainer).each(function() {
        $(this).waypoint(function(event, direction) {
            $(this).easyPieChart({
                animate : 2000,
                barColor : primaryColor,
                size : '125',
                lineWidth : '6',
                trackColor : false,
                scaleColor : false
            });

        }, {
            triggerOnce : true,
            offset : '90%'
        });
    });


    $(charts.chartDarkContainer).each(function() {
        $(this).waypoint(function(event, direction) {
            $(this).easyPieChart({
                animate : 2000,
                barColor : secondaryColor,
                size : '125',
                lineWidth : '6',
                trackColor : false,
                scaleColor : false
            });

        }, {
            triggerOnce : true,
            offset : '90%'
        });
    });

    $(charts.bootstrapProgress).each(function() {
        $(this).waypoint(function(event, direction) {
            var target = $(this).attr('aria-valuenow') + "%";
            $(this).animate({
                width: target
            }, {duration: 500, queue: false} );

        }, {
            triggerOnce : true,
            offset : '90%'
        });
    });

}