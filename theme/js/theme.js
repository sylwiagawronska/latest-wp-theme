/**
 * Theme initialization
 * @param $
 * @returns {{init: Function}}
 * @constructor
 */
var THEME = function($) {
        "use strict";

        var navigation,
            revolution,
            progressbar,
            carousels,
            portfolio,
            ajaxLoading,
            external,
            counters,
            loadMore,
            tabs;


        var init = function(input) {

            // Assign values according to the UI
            var UI = {
                primaryColor: input.primaryColor,
                secondaryColor: input.secondaryColor,
                sliderContainer: '.tp-banner',
                dropdownDesktop: 'li.dropdown > ul.dropdown-menu > li.dropdown',
                toggleDropdown: '.toggleDropdown',
                charts : {
                    chartRegularContainer: '.chart',
                    chartDarkContainer: '.chart-dark',
                    bootstrapProgress: '.progress-bar'
                },
                teamArrows: {
                    prev: '<img src="'+input.imagesSource+'/images/arrow-left-light.png" alt="arrow">',
                    next: '<img src="'+input.imagesSource+'/images/arrow-right-light.png" alt="arrow">'
                },
                portfolioArrows: {
                    prev: '<img src="'+input.imagesSource+'/images/arrow-dark-prev.png" alt="arrow">',
                    next: '<img src="'+input.imagesSource+'/images/arrow-dark-next.png" alt="arrow">'
                },
                portfolio: {
                    portfolioFilter: '.portfolioFilters a',
                    portfolioItem: '.portfolio-item',
                    portfolioCarousel: '.portfolio-carousel'
                },
                teamContainer: '.team-carousel',
                portfolioContainer: '.portfolio-carousel',
                portfolioStandalone: '.portfolio-standalone',
                testimonialsContainer: '.testimonialsCarousel',
                loadMoreContainer: '.load-more-container',
                loadMoreButton: '#loadMore',
                portfolioLoadMore: '.portfolio_loadMore',

                ajaxLoaders: {
                    next: '.next-ajax a',
                    prev: '.prev-ajax a',
                    load: '.load-item',
                    exit: '.exit-ajax'
                },

                ajaxPortfolioContainer: '#ajaxPortfolioContainer',
                ajaxPortfolioItem: '#ajaxItem',
                counter: '.count-up',
                masonryGrid: '.masonry-grid',
                masonryItem: '.item-masonry',

                flexSlider: '.flexslider',
                fitVid: '.fitvid',

                tabs: '.tab-content'

            }
            // Call components
            navigation = Navigation($, UI.dropdownDesktop, UI.toggleDropdown);
            revolution = Revolution($, UI.sliderContainer);
            progressbar = ProgressBar($, UI.charts, UI.primaryColor, UI.secondaryColor);
            carousels = Carousels($,UI.teamContainer,UI.portfolioContainer,UI.teamArrows, UI.portfolioArrows, UI.testimonialsContainer);
            portfolio = Portfolio($, UI.portfolio, UI.portfolioContainer, UI.portfolioStandalone);
            ajaxLoading = AjaxLoading($, UI.ajaxLoaders, UI.ajaxPortfolioContainer, UI.ajaxPortfolioItem);
            external = ExternalScripts($, UI.flexSlider, UI.fitVid, UI.masonryGrid, UI.masonryItem);
            counters = Counters($, UI.counter);
            loadMore = LoadMore($, UI.loadMoreContainer, UI.loadMoreButton, UI.portfolioLoadMore);
            tabs = Tabs($, UI.tabs);
            new WOW().init();


        }

        return {
            init : init
        };

    };
