<?php get_header();
if (is_front_page()) { get_template_part('slider'); }


if (have_posts()) : while (have_posts()) : the_post();

    // Check if user specified alternative page title
    if ((esc_attr(get_post_meta($post -> ID, "synergy_page_enable_alternative_title", true)) ==  1)
       && (esc_attr(get_post_meta($post -> ID, "synergy_page_alternative_title", true)) !==  "")
    ) {
        $title = (get_post_meta($post -> ID, "synergy_page_alternative_title", true));
    } else {
        // if not, use default title
        $title = get_the_title();
    }

    $sidebar = esc_attr(get_post_meta($post -> ID, "synergy_page_sidebar", true));

    $sidebar_id = esc_attr(get_post_meta($post -> ID, "synergy_page_sidebar_id", true));

    $active_menu = esc_attr(get_post_meta($post -> ID, "synergy_page_active_menu_item", true));

    $small_header = esc_attr(get_post_meta($post -> ID, "synergy_page_small_header", true));

    $header_type = esc_attr(get_post_meta($post -> ID, "synergy_page_header_type", true));
    $include_slider = esc_attr(get_post_meta($post -> ID, "synergy_page_fw_slider", true));
    $slider_id = esc_attr(get_post_meta($post -> ID, "synergy_page_slider_shortcode", true));
    $image_src = "";
    $url = "";
    $images = get_post_meta(get_the_ID(), 'synergy_page_header_image', false);
    if ($images) {
        $image_src = wp_get_attachment_image_src($images[0], 'full');
        $url = esc_url($image_src[0]);
    }

    $header_bg = esc_attr(get_post_meta($post -> ID, "synergy_page_header_bg_color", true));
    $heading_color = esc_attr(get_post_meta($post -> ID, "synergy_page_heading_font_color", true));

    $bg_color = esc_attr(get_post_meta($post -> ID, "synergy_page_bg_color", true));


    // fetch breadcrums settings
    $breadcrums = esc_attr(get_post_meta($post -> ID, "synergy_page_breadcrums", true));
    $breadcrums_border = esc_attr(get_post_meta($post -> ID, "synergy_page_breadcrums_border", true));
    $breadcrums1 = esc_attr(get_post_meta($post -> ID, "synergy_page_breadcrums1_color", true));
    $breadcrums2 = esc_attr(get_post_meta($post -> ID, "synergy_page_breadcrums2_color", true));
    $breadcrums_home = esc_attr(get_post_meta($post -> ID, "synergy_page_breadcrums_home", true));
?>

    <?php if (!is_front_page()) {
        if  (($include_slider == 1) && ($slider_id != '')){
            echo '<div class="header">';
            putRevSlider(esc_attr($slider_id));
            echo '</div>';
        } else { ?>

            <div class="page-header <?php if ($small_header == 1) { ?>padding-top-70 padding-bottom-60<?php } else { ?>padding-top-100 padding-bottom-100<?php } ?>" <?php if ($header_type == 'color'){ ?> style="background:<?php echo esc_attr($header_bg);?>" <?php } ; ?> <?php if (($header_type == 'image') and ($image_src != '')) { ?> style="background:url(<?php echo esc_url($url);?>)" <?php } ; ?> >
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 align-center wow fadeIn">
                            <h1 style="color: <?php echo $heading_color; ?>"><?php echo $title;?></h1>
                            <?php
                                if ($breadcrums == 1) {
                                    synergy_breadcrumb($breadcrums_home, $breadcrums_border, $breadcrums1, $breadcrums2);
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>



        <?php
        } }?>

    <div class="page-content <?php if (!is_front_page()) { ?>padding-top-50<?php }?>" style="background-color: <?php echo $bg_color;?>">
        <div class="container">
            <div class="row">
                <?php if ($sidebar == 0) { ?>
                <div class="col-md-12 clearfix">
                    <?php } else { ?>
                    <div class="col-md-9 clearfix">
                <?php }  ?>
                    <?php the_content(); ?>
                </div>
                    <?php if ($sidebar == 1) { ?>
                        <div class="col-md-3 sidebar clearfix">
                        <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar($sidebar_id) ) ?>
                        </div>
                    <?php }  ?>
            </div>
        </div>
    </div>


<?php
endwhile;
endif;

if ($active_menu != "") {
    ?>
    <script>
        jQuery('.mega-menu-item').each(function() {
            var mega_mnu_item = 'mega-menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mega_mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });

        jQuery('.menu-item').each(function() {
            var mnu_item = 'menu-item-' + <?php echo esc_attr($active_menu); ?>;
            if ( jQuery(this).hasClass(mnu_item) ) {
                jQuery(this).addClass('active');
            }
        });
    </script>
<?php } ?>
<?php get_footer(); ?>