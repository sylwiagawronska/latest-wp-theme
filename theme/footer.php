<?php wp_footer(); ?>
<?php global $hypno_options;
if ((array_key_exists('enable-footer-social-bar', $hypno_options) && $hypno_options['enable-footer-social-bar'] == 1 )) {
    if (array_key_exists('social-footer-icons-count', $hypno_options)) {

        echo '<div class="upper-footer">';
            echo '<div class="container">';
                echo '<div class="row">';
                    $count = $hypno_options['social-footer-icons-count'];

                    $col_count = (12 / $count);
                    $xs_count = $col_count;

                    if ($count == 6) {
                        $xs_count = 6;
                    }

                    for ($i = 1; $i < $count+1; $i++) {
                        echo '<div class="col-md-'.$col_count.' col-sm-'.$col_count.' col-xs-'.$xs_count.' padding-top-60 padding-bottom-60 align-center">';
                        echo '<a href="'.$hypno_options[$hypno_options['social-footer-'.$i]].'" class="social-widget wow fadeIn"><i class="fa '.$hypno_options['social-footer-'.$i].'"></i></a>';
                        echo '</div>';
                    }

        echo '</div></div></div>';

    }
}

if ((array_key_exists('enable-lower-footer', $hypno_options) && $hypno_options['enable-lower-footer'] == 1 )) {

            if ((array_key_exists('lower-footer-type', $hypno_options) && $hypno_options['lower-footer-type'] == 'logo' )) {
                echo '<div class="lower-footer padding-top-80 padding-bottom-80">';
                echo '<div class="container">';
                echo '<div class="row">';
                echo '<div class="col-md-12 align-center">';
                if ((array_key_exists('footer-logo', $hypno_options) && $hypno_options['footer-logo'] !== "")) {
                    echo '<a href="' . esc_url(home_url()) . '">' .
                        wp_get_attachment_image( $hypno_options['footer-logo']['id'], 'full' ) .
                        '</a>';
                }
                echo '</div>';
                echo '</div></div></div>';
            } else {

                if (array_key_exists('footer-widgets-count', $hypno_options)) {
                    echo '<div class="lower-footer padding-top-40 padding-bottom-40">';
                    echo '<div class="container">';
                    echo '<div class="row">';
                    $columns = $hypno_options['footer-widgets-count'];
                    $col_count = 12 / $columns;

                    for ($i = 1; $i < $columns+1; $i++) {
                        echo '<div class="col-md-'.$col_count.' col-sm-6 col-xs-12 padding-top-30 padding-bottom-30">';
                        if ( is_active_sidebar( 'footer-widget-' . $i ) ) :
                            dynamic_sidebar( 'footer-widget-' . $i );
                        endif;
                        echo '</div>';
                    }
                    echo '</div></div></div>';
                }
            }

}
?>
<script>
    (function($) {"use strict";

        $(document).ready(function() {
            <?php
            if ((array_key_exists('preloader-enabled', $hypno_options) && $hypno_options['preloader-enabled'] == 1 )) {
                if ((array_key_exists('preloader-pages', $hypno_options) && $hypno_options['preloader-pages'] === 'main' )) {
                    if (is_front_page()) {?>
                    $('body').jpreLoader();
               <?php } } else { ?>
                    $('body').jpreLoader();
               <?php }
            }
            ?>

            <?php if (array_key_exists('primary-accent-color', $hypno_options) && $hypno_options['primary-accent-color'] != '' ) {
                $primary_accent_color = $hypno_options['primary-accent-color'];
            } else {
                $primary_accent_color = '#cbcbcb';
            }

            if (array_key_exists('secondary-accent-color', $hypno_options) && $hypno_options['secondary-accent-color'] != '' ) {
                $secondary_accent_color = $hypno_options['secondary-accent-color'];
            } else {
                $secondary_accent_color = '#1e1f20';
            }

               ?>
           THEME($).init({
                primaryColor: '<?php echo $primary_accent_color;?>',
                secondaryColor: '<?php echo $secondary_accent_color;?>',
                imagesSource : '<?php echo get_template_directory_uri();?>'
            });
        });

    })(jQuery);

    <?php
         if (array_key_exists('include-js', $hypno_options) && $hypno_options['include-js'] == 1) {
             echo $hypno_options['custom-js'];
         }
    ?>
</script>
</body>
</html>