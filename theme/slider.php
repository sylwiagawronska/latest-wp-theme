<?php
global $hypno_options;

if ($hypno_options['slider-enabled'] == true) {
    $slider_id = esc_attr($hypno_options['slider']);
    if ($slider_id && $slider_id !== 'None') {
        echo '<div class="header">';
        putRevSlider(esc_attr($slider_id));
        echo '</div>';
    }

}