<?php
/*
Plugin Name: Synergy
Author: Synergy Themes
Version: 1.0
Text Domain: synergy
*/
add_action( 'init', 'synergy_load_textdomain' );
/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
function synergy_load_textdomain() {
    load_plugin_textdomain( 'synergy', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
if (!class_exists('Synergy')){
    require dirname(__FILE__).'/lib/Synergy.class.php';
    require "vendor/oyejorge/less.php/lessc.inc.php";
}


/**
 * Attempt to adjust php.ini settings
 */
@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );


if (class_exists('WPBakeryShortCode')) {
    include_once dirname(__FILE__).'/lib/vc.php';
}

// Before VC Init
add_action( 'vc_before_init', 'vc_before_init_actions' );


/**
 * Include custom VC templates
 */
function vc_before_init_actions() {
    if( function_exists('vc_set_shortcodes_templates_dir') ){
        vc_set_shortcodes_templates_dir( dirname(__FILE__).'/lib/vc_templates' );

    }

}

/**
 * Add portfolio port type
 */
function portfolio_post_type() {
    $labels = array('name' => __('Portfolio', 'synergy'), 'singular_name' => __('Portfolio Item', 'synergy'), 'add_new' => __('Add New Item', 'synergy'), 'edit_item' => __('Edit Portfolio Item', 'synergy'), 'new_item' => __('Add New Portfolio Item', 'synergy'), 'add_new_item' => __('Add New Item', 'synergy'), 'view_item' => __('View Portfolio Item', 'synergy'), 'not_found' => __('Nothing to display', 'synergy'), );
    $args = array('labels' => $labels, 'public' => true, 'show_ui' => true, 'capability_type' => 'post', 'hierarchical' => false, 'rewrite' => array('slug' => 'portfolio-item'), 'supports' => array('title', 'editor', 'thumbnail'));
    register_post_type('portfolio', $args);
}

add_action('init', 'portfolio_post_type');


/**
 * Add custom portfolio taxonomy
 */
function portfolio_categories() {
    $labels = array('name' => __('Portfolio Categories', 'synergy'), 'singular_name' => __('Portfolio Category', 'synergy'), 'search_items' => __('Search Portfolio Categories', 'synergy'), 'popular_items' => __('Popular Portfolio Categories', 'synergy'), 'all_items' => __('All Portfolio Categories', 'synergy'), 'parent_item' => null, 'parent_item_colon' => null, 'edit_item' => __('Edit Portfolio Categories', 'synergy'), 'update_item' => __('Update Portfolio Category', 'synergy'), 'add_new_item' => __('Add New Category', 'synergy'), 'new_item_name' => __('New Category Name', 'synergy'), 'separate_items_with_commas' => __('Separate Categories With Commas', 'synergy'), 'add_or_remove_items' => __('Add or Remove Categories', 'synergy'), 'choose_from_most_used' => __('Choose from Most Used Categories', 'synergy'), 'not_found' => __('No Caategories Found', 'synergy'), 'menu_name' => __('Portfolio Categories', 'synergy'), );
    $args = array('labels' => $labels, 'hierarchical' => true, 'show_ui' => true, 'show_admin_column' => true, 'query_var' => true, 'rewrite' => true, );
    register_taxonomy('portfolio_categories', 'portfolio', $args);
}

add_action('init', 'portfolio_categories', 0);


function synergy_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'synergy_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function synergy_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array
    $style_formats = array(
        /*
        * Each array child is a format with it's own settings
        * Notice that each array has title, block, classes, and wrapper arguments
        * Title is the label which will be visible in Formats menu
        * Block defines whether it is a span, div, selector, or inline style
        * Classes allows you to define CSS classes
        * Wrapper whether or not to add a new block-level element around any selected elements
        */
        array(
            'title' => __('Inline H1', 'synergy' ),
            'block' => 'h1',
            'inline' => 'h1',
            'classes' => 'inline-block',
            'wrapper' => false,

        ),
        array(
            'title' => __('Inline H2', 'synergy' ),
            'block' => 'h2',
            'inline' => 'h2',
            'classes' => 'inline-block',
            'wrapper' => false,

        ),
        array(
            'title' => __('Inline H3', 'synergy' ),
            'block' => 'h3',
            'inline' => 'h3',
            'classes' => 'inline-block',
            'wrapper' => false,

        ),
        array(
            'title' => __('Inline H4', 'synergy' ),
            'block' => 'h4',
            'inline' => 'h4',
            'classes' => 'inline-block',
            'wrapper' => false,

        ),
        array(
            'title' => __('Inline H5', 'synergy' ),
            'block' => 'h5',
            'inline' => 'h5',
            'classes' => 'inline-block',
            'wrapper' => false,

        ),
        array(
            'title' => __('Inline H6', 'synergy' ),
            'block' => 'h6',
            'inline' => 'h6',
            'classes' => 'inline-block',
            'wrapper' => false,

        ),
        array(
            'title' => __('Thin Font', 'synergy' ),
            'inline' => 'span',
            'classes' => 'thin-font',
            'wrapper' => false,
        ),
        array(
            'title' => __('Medium Thin Font', 'synergy' ),
            'inline' => 'span',
            'classes' => 'medium-thin-font',
            'wrapper' => false,
        ),

    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'synergy_mce_before_init_insert_formats' );


/* Hook to init */
add_action( 'init', 'my_editor_background_color' );

/**
 * Add TinyMCE Button
 * @link https://shellcreeper.com/?p=1339
 */
function my_editor_background_color(){

    /* Add the button/option in second row */
    add_filter( 'mce_buttons_2', 'my_editor_background_color_button', 1, 2 ); // 2nd row
}

/**
 * Modify 2nd Row in TinyMCE and Add Background Color After Text Color Option
 * @link https://shellcreeper.com/?p=1339
 */
function my_editor_background_color_button( $buttons, $id ){

    /* Only add this for content editor, you can remove this line to activate in all editor instance */
    if ( 'content' != $id ) return $buttons;

    /* Add the button/option after 4th item */
    array_splice( $buttons, 4, 0, 'backcolor' );

    return $buttons;
}




