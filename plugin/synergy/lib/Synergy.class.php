<?php

class Synergy{

    public static function generateCSS(){

        global $hypno_options;

        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $upload_dir = $upload_dir . '/synergy';
        if (! is_dir($upload_dir)) {
            mkdir( $upload_dir, 0755 );
        }

        $woocommerce_column_width = "30.8%";

        if ($hypno_options['woocommerce-columns'] == 2) {
            $woocommerce_column_width = "48.1%";
        } else if ($hypno_options['woocommerce-columns'] == 4) {
            $woocommerce_column_width = "22.05%";
        }

        $dummy_img = get_template_directory_uri() . "/images/dummy.png";


        $parser = new Less_Parser();
        $parser->parseFile(get_template_directory() . "/less/style.less", get_template_directory() . "/less/");

        $parser->ModifyVars( array(
            'background-color' => $hypno_options['background-color'],
            'accent-color' => $hypno_options['primary-accent-color'],
            'secondary-accent-color' => $hypno_options['secondary-accent-color'],
            'woocommerce-product-width' => $woocommerce_column_width,
            'upper-footer-link-color' => $hypno_options['social-footer-icons-color'],
            'upper-footer-bg-color' => $hypno_options['social-footer-bg-color'],
            'lower-footer-bg-color' => $hypno_options['lower-footer-bg-color'],
            'lower-footer-text-color' => $hypno_options['lower-footer-text-color'],
            'lower-footer-heading-color' => $hypno_options['lower-footer-heading-color'],
            'preloader-bg' => $hypno_options['preloader-bg'],


            'body-font-weight' => $hypno_options['hypno-typography-body']['font-weight'],
            'body-line-height' => $hypno_options['hypno-typography-body']['line-height'],
            'body-font-color' => $hypno_options['hypno-typography-body']['color'],
            'body-font-family' => $hypno_options['hypno-typography-body']['font-family'],
            'body-font-size' => $hypno_options['hypno-typography-body']['font-size'],

            'headline-font-family' => $hypno_options['hypno-typography-heading']['font-family'],
            'headline-color' => $hypno_options['hypno-typography-heading']['color'],

            'h1-font-weight' => $hypno_options['hypno-typography-h1']['font-weight'],
            'h2-font-weight' => $hypno_options['hypno-typography-h2']['font-weight'],
            'h3-font-weight' => $hypno_options['hypno-typography-h3']['font-weight'],
            'h4-font-weight' => $hypno_options['hypno-typography-h4']['font-weight'],
            'h5-font-weight' => $hypno_options['hypno-typography-h5']['font-weight'],
            'h6-font-weight' => $hypno_options['hypno-typography-h6']['font-weight'],

            'h1-font-size' => $hypno_options['hypno-typography-h1']['font-size'],
            'h2-font-size' => $hypno_options['hypno-typography-h2']['font-size'],
            'h3-font-size' => $hypno_options['hypno-typography-h3']['font-size'],
            'h4-font-size' => $hypno_options['hypno-typography-h4']['font-size'],
            'h5-font-size' => $hypno_options['hypno-typography-h5']['font-size'],
            'h6-font-size' => $hypno_options['hypno-typography-h6']['font-size'],

            'h1-font-family' => $hypno_options['hypno-typography-h1']['font-family'],
            'h2-font-family' => $hypno_options['hypno-typography-h2']['font-family'],
            'h3-font-family' => $hypno_options['hypno-typography-h3']['font-family'],
            'h4-font-family' => $hypno_options['hypno-typography-h4']['font-family'],
            'h5-font-family' => $hypno_options['hypno-typography-h5']['font-family'],
            'h6-font-family' => $hypno_options['hypno-typography-h6']['font-family'],

            'h1-font-color' => $hypno_options['hypno-typography-h1']['color'],
            'h2-font-color' => $hypno_options['hypno-typography-h2']['color'],
            'h3-font-color' => $hypno_options['hypno-typography-h3']['color'],
            'h4-font-color' => $hypno_options['hypno-typography-h4']['color'],
            'h5-font-color' => $hypno_options['hypno-typography-h5']['color'],
            'h6-font-color' => $hypno_options['hypno-typography-h6']['color'],

            'footnote-color' => $hypno_options['footnote-color'],
            'light-border' => $hypno_options['light-border'],
            'dark-border' => $hypno_options['dark-border'],
            'logo-right-border' => $hypno_options['logo-right-border'],
            'navbar-background' => $hypno_options['navbar-background'],
            'dark-section-bg-color' => $hypno_options['dark-section-bg-color'],
            'light-grey-bg-color' => $hypno_options['light-grey-bg-color'],
            'lighter-grey-bg-color' => $hypno_options['lighter-grey-bg-color'],
            'white-bg-color' => $hypno_options['white-bg-color'],
            'medium-grey-color' => $hypno_options['medium-grey-color'],
            'white-text' => $hypno_options['white-text'],
            'light-text' => $hypno_options['light-text'],
            'dark-text' => $hypno_options['dark-text'],
            'error-page-404' => $hypno_options['error-page-404'],
            'footnote-font-size' => $hypno_options['footnote-font-size'] . 'px',
            'blockquote-font-size' => $hypno_options['blockquote-font-size'] . 'px',
            'dropdown-size' => $hypno_options['dropdown-size'] . 'px',
            'stroke-text' => $hypno_options['stroke-text'],

            'hamburger-menu-color' => $hypno_options['hamburger-menu-color'],
            'submenu-item-hover-color' => $hypno_options['submenu-item-hover-color'],
            'submenu-item-hover-background' => $hypno_options['submenu-item-hover-background'],
            'submenu-item-background' => $hypno_options['submenu-item-background'],
            'submenu-dropbown-bottom-border' => $hypno_options['submenu-dropbown-bottom-border'],
            'submenu-item-bottom-border' => $hypno_options['submenu-item-bottom-border'],

            'menu-font-color' => $hypno_options['hypno-menu-font']['color'],
            'menu-font-family' => $hypno_options['hypno-menu-font']['font-family'],
            'menu-font-size' => $hypno_options['hypno-menu-font']['font-size'],
            'menu-font-weight' => $hypno_options['hypno-menu-font']['font-weight'],

            'submenu-font-color' => $hypno_options['hypno-submenu-font']['color'],
            'submenu-font-family' => $hypno_options['hypno-submenu-font']['font-family'],
            'submenu-font-size' => $hypno_options['hypno-submenu-font']['font-size'],

        ) );

        if ($hypno_options['preloader-logo']['id'] ) {
            $parser->ModifyVars( array(
                'preloader-logo' => '"' . $hypno_options['preloader-logo']['url'] . '"',
            ) );
        } else {
            $parser->ModifyVars( array(
                'preloader-logo'=> '"' . $dummy_img . '"'
            ) );
        }

        if ($hypno_options['navbullet']['id'] ) {
            $parser->ModifyVars( array(
                'submenu-bullet-url' => 'url(' . wp_get_attachment_url( $hypno_options['navbullet']['id'] ) . ')  no-repeat left 20px top 20px;',
                'submenu-bullet-url-single' => 'url(' . wp_get_attachment_url( $hypno_options['navbullet']['id'] ) . ')',
            ) );
        } else {
            $parser->ModifyVars( array(
                'submenu-bullet-url'=> 'none',
                'submenu-bullet-url-single'=> 'none',
            ) );
        }

        if ($hypno_options['navbullet-hover']['id'] ) {
            $parser->ModifyVars( array(
                'submenu-bullet-hover-url' => 'url(' . wp_get_attachment_url( $hypno_options['navbullet-hover']['id'] ) . ') no-repeat left 30px top 20px;',
                'submenu-bullet-hover-url-single' => 'url(' . wp_get_attachment_url( $hypno_options['navbullet-hover']['id'] ) . ')',
            ) );
        } else {
            $parser->ModifyVars( array(
                'submenu-bullet-hover-url'=> 'none',
                'submenu-bullet-hover-url-single'=> 'none',
                'navigation-bullet-padding' => '12px 5px 12px 12px',
                'sidebar-list-item-padding' => '15px'
            ) );
        }
        $parser->SetOption('compress', true);
        $css = $parser->getCss();
        file_put_contents($upload_dir . '/theme.css', $css);

    }


}