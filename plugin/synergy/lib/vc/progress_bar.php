<?php

add_action( 'vc_before_init', 'synergy_progress_integrateWithVC' );

function synergy_progress_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Progress Bar', 'synergy' ),
        'base' => 'synergy_progress',
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-graph',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Title', 'synergy' ),
                'param_name' => 'title',
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Percentage', 'synergy' ),
                'param_name' => 'percentage',
                'value'      => getPercentage()
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Style', 'synergy' ),
                'param_name' => 'style',
                'value'      => array(
                    __( 'Plain', 'synergy' )       => 'progress-bar-plain',
                    __( 'Stripped', 'synergy' )       => 'progress-bar-striped',
                    __( 'Animated', 'synergy' )       => 'progress-bar-striped active',
                ),
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Bar Color', 'js_composer' ),
                'param_name' => 'bar_color',
                'value' => '#1e1f20'
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Has background?', 'synergy' ),
                'param_name' => 'background',
                'value'      => array(
                    __( 'No', 'synergy' )       => 'nobg',
                    __( 'Colored background', 'synergy' )       => 'withbg',

                ),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __( 'Background Color', 'js_composer' ),
                'param_name' => 'bg_color',
                'value' => '#f4f4f4',

                'dependency' => array(
                    'element' => 'background',
                    'value' => 'withbg',
                )
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Text Color', 'js_composer' ),
                'param_name' => 'text_color',
                'value' => '#ffffff'
            )


        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Progress extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $title = $style = $percentage = $bar_color = $background = $bg_color = $text_color = "";

            //$css_animation = "";
            extract(shortcode_atts(array(
                'title' => '',
                'percentage' => '98',
                'style' => 'progress-bar-plain',
                'bar_color' => '#1e1f20',
                'background' => 'nobg',
                'bg_color' => '#f4f4f4',
                'text_color' => '#ffffff'
            ), $atts));

            if ($background == 'nobg') {
                $bg_color = 'transparent';
            }

            $output = '<div class="progress '.esc_attr($background).'" style="background-color: '.esc_attr($bg_color).'">';
            $output .= '<div class="progress-bar '.esc_attr($style).'" role="progressbar" aria-valuenow="'.esc_attr($percentage).'" aria-valuemin="0" aria-valuemax="100" style="background-color: '.esc_attr($bar_color).'; color: '.esc_attr($text_color).'">';
            $output .= ''.esc_attr($title).' - '.esc_attr($percentage).'%';
            $output .= '</div>';
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}

