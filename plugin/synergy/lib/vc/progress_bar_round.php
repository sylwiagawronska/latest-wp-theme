<?php

add_action( 'vc_before_init', 'synergy_progressround_integrateWithVC' );

function synergy_progressround_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Round Progress Bar', 'synergy' ),
        'base' => 'synergy_progressround',
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-vc-round-chart',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Title', 'synergy' ),
                'param_name' => 'title',
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Subtitle', 'synergy' ),
                'param_name' => 'subtitle',
            ),
            array(
                'type' => 'textarea_html',
                'heading' => __( 'Description', 'synergy' ),
                'param_name' => 'content',
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Percentage', 'synergy' ),
                'param_name' => 'percentage',
                'value'      => getPercentage()
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Chart Color Scheme', 'synergy' ),
                'param_name' => 'color',
                'value'      => array(
                    __( 'Primary Accent Color', 'synergy' )       => 'chart',
                    __( 'Secondary Accent', 'synergy' )       => 'chart-dark',

                ),
            ),


            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Progressround extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $title = $subtitle = $percentage = $color = $css_animation = "";

            //$css_animation = "";
            extract(shortcode_atts(array(
                'title' => '',
                'subtitle' => '',
                'percentage' => '98',
                'color' => 'chart',
                'css_animation' => ''
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }


            $output = '<div class="row progressbar sm-center '.esc_attr($css_class).'">';
            $output .= '<div class="col-md-5">';
            $output .= '<div class="skill-chart padding-bottom-20">';
            $output .= '<div class="'.esc_attr($color).'" data-percent="'.esc_attr($percentage).'">';
            $output .= '<h2>'.esc_attr($percentage).'%</h2>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '<div class="col-md-7 progress-description">';
            $output .= '<h4>'.esc_attr($title).'</h4>';
            $output .= '<span class="footnote">'.esc_attr($subtitle).'</span>';
            $output .= '<p>';
            $output .= wpb_js_remove_wpautop($content);
            $output .= '</p>';
            $output .= '</div>';
            $output .= '</div>';

            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}

