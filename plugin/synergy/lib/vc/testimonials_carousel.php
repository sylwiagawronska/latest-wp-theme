<?php


add_action( 'vc_before_init', 'synergy_testimonialscarousel_integrateWithVC' );

function synergy_testimonialscarousel_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Testimonials carousel', 'synergy' ),
        'base' => 'synergy_testimonialscarousel',
        'as_parent'  => array('only' => 'synergy_testimonialcarousel'),
        'show_settings_on_create' => false,
         'icon' => 'icon-wpb-vc-round-chart',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'is_container' => true,
        "js_view" => 'VcColumnView',
        'params' => array(

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),
        ),
    ));
}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_synergy_Testimonialscarousel extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $color_schemes = "";
            extract(shortcode_atts(array(
                'color_schemes' => 'light-team-carousel'
            ), $atts));

            $output = '<div class="testimonialsCarousel '.esc_attr($color_schemes).'">';
            $output .= wpb_js_remove_wpautop( $content );
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }



    }
}

