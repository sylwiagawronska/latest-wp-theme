<?php

add_action( 'vc_before_init', 'synergy_quotes_integrateWithVC' );

function synergy_quotes_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Quote', 'synergy' ),
        'base' => 'synergy_quotes',
        'show_settings_on_create' => true,
         'icon' => 'icon-wpb-layer-shape-text',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(

            array(
                'type' => 'textarea',
                'heading' => __( 'Quote Content', 'synergy' ),
                'param_name' => 'description',
            ),

            array(
                'type' => 'textfield',
                'heading' => __( 'Quote author', 'synergy' ),
                'param_name' => 'author',
            ),

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Align",
                "param_name" => "align",
                "value" => array(
                    __( 'Align left', 'synergy' ) => "blockquote-regular",
                    __( 'Align right', 'synergy' ) => "blockquote-reverse"
                )
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Background Color', 'js_composer' ),
                'param_name' => 'bg_color',
                'value' => '#f8f8f6',

            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Background Hover Color', 'js_composer' ),
                'param_name' => 'bg_color_hover',
                'value' => '#ffffff',
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Border Color', 'js_composer' ),
                'param_name' => 'border_color',
                'value' => '#1e1f20',
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Content Text Color', 'js_composer' ),
                'param_name' => 'text_color',
                'value' => '#4f4f4f',
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Author Text Color', 'js_composer' ),
                'param_name' => 'author_color',
                'value' => '#b6b6b6',
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Quotes extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $description = $author = $align = $bg_color = $bg_color_hover = $border_color = $text_color = $author_color = $css_animation = '';
            //$css_animation = "";
            extract(shortcode_atts(array(
                'description' => '',
                'author' => '',
                'align' => 'blockquote-regular',
                'bg_color' => '#f8f8f6',
                'bg_color_hover' => '#ffffff',
                'border_color' => '#1e1f20',
                'text_color' => '#4f4f4f',
                'author_color' => '#b6b6b6',
                'css_animation' => '',
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }


            $output = '';

            $output .= '<div class="quote-wrapper" style="background: '.esc_attr($bg_color_hover).'">';
            $output .= '<blockquote class="'.esc_attr($css_class).' '.esc_attr($align).'" style="background: '.esc_attr($bg_color).'; border-color: '.esc_attr($border_color).'">';
            $output .= '<p style="color: '.esc_attr($text_color).'">'.esc_attr($description).'</p>';
            $output .= '<footer style="color: '.esc_attr($author_color).'">'.esc_attr($author).'</footer>';
            $output .= '</blockquote>';
            $output .= '</div>';

            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
