<?php
add_action( 'vc_before_init', 'synergy_accordion_integrateWithVC' );

function synergy_accordion_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Toggle', 'synergy' ),
        'base' => 'synergy_accordion',
        'as_parent'  => array('only' => 'synergy_accordionitem'),
        'show_settings_on_create' => false,
        'icon' => 'icon-wpb-ui-accordion',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'is_container' => true,
        "js_view" => 'VcColumnView',
        'params' => array(

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            )
        ),
    ));
}

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_synergy_Accordion extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $css_animation = "";
            extract(shortcode_atts(array(
                'css_animation' => ''
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            $output = '<div class="panel-group '.esc_attr($css_class).'">';
            $output .= wpb_js_remove_wpautop( $content );
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }



    }
}

