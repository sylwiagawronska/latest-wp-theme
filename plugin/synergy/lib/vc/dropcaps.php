<?php

add_action( 'vc_before_init', 'synergy_dropcap_integrateWithVC' );

function synergy_dropcap_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Dropcap', 'synergy' ),
        'base' => 'synergy_dropcap',
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-ui-custom_heading',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Dropcap letter', 'synergy' ),
                'param_name' => 'dropcap',
            ),

            array(
                'type' => 'textarea',
                'heading' => __( 'Paragraph content', 'synergy' ),
                'param_name' => 'paragraph',
            ),

            array(
                "type" => "dropdown",
                "param_name" => "type",
                "value" => array(
                    __( 'No background', 'synergy' ) => "drop-cap",
                    __( 'Round background', 'synergy' ) => "drop-cap dropcap-bg-round",
                    __( 'Square background', 'synergy' ) => "drop-cap dropcap-bg"
                )
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Background Color', 'synergy' ),
                'param_name' => 'bg_color',
                'value' => '#1e1f20',
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Letter Color', 'synergy' ),
                'param_name' => 'color',
                'value' => '#ffffff',
            ),


            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Dropcap extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $dropcap = $paragraph = $type = $bg_color = $color = $css_animation = "";

            extract(shortcode_atts(array(
                'dropcap' => 'S',
                'paragraph' => '',
                'type' => 'drop-cap',
                'bg_color' => '#1e1f20',
                'color' => '#ffffff',
                'css_animation' => '',
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            if ('drop-cap' === $type) {
                $bg_color = 'transparent';
            }


            $output = '<p class="'.esc_attr($css_class).'"><span class="'.esc_attr($type).'" style="color: '.esc_attr($color).'; background-color:'.esc_attr($bg_color).'">'.esc_attr($dropcap).'</span>'.esc_attr( $paragraph ).'</p>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}



