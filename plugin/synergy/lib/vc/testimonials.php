<?php


add_action( 'vc_before_init', 'synergy_testimonials_integrateWithVC' );

function synergy_testimonials_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Testimonials grid', 'synergy' ),
        'base' => 'synergy_testimonials',
        'as_parent'  => array('only' => 'synergy_testimonial'),
        'show_settings_on_create' => true,
         'icon' => 'icon-wpb-vc-round-chart',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'is_container' => true,
        "js_view" => 'VcColumnView',
        'params' => array(

            array(
                'type' => 'dropdown',
                'heading' => __( 'Items per row', 'synergy' ),
                'param_name' => 'row',
                'admin_label' => true,
                'value' => array(
                    __( 'Two', 'synergy' ) => 'masonry-2-col',
                    __( 'Three', 'synergy' ) => 'masonry-3-col',
                    __( 'Four', 'synergy' ) => 'masonry-4-col'
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Include load more button', 'synergy' ),
                'param_name' => 'loadmore',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => 'no',
                    __( 'Yes', 'synergy' ) => 'yes'
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Rows to display', 'synergy' ),
                'param_name' => 'rows',
                'value'      => array(
                    __( 'One', 'synergy' )       => '1',
                    __( 'Two', 'synergy' )       => '2',
                    __( 'Three', 'synergy' )       => '3'
                ),
                'dependency' => array(
                    'element' => 'loadmore',
                    'value' => 'yes',
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Load more button text', 'synergy' ),
                'param_name' => 'loadmore_button',
                'dependency' => array(
                    'element' => 'loadmore',
                    'value' => 'yes',
                ),
            ),

        ),
    ));
}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_synergy_Testimonials extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $row = $loadmore = $rows = $loadmore_button = "";
            extract(shortcode_atts(array(
                'row' => 'masonry-2-col',
                'loadmore' => 'no',
                'rows' => '1',
                'loadmore_button' => 'Load more'
            ), $atts));

            $output = '';
            $loadmore_container = $loadmore_attrs = '';

            if ('yes' === $loadmore){
                $loadmore_container = 'load-more-container';

                if ('masonry-3-col' === $row) {
                    $amount = intval($rows) * 3;
                    $loadmore_attrs = 'data-initial='.esc_attr($amount).' data-increment=3';
                } else if ('masonry-2-col' === $row) {
                    $amount = intval($rows) * 2;
                    $loadmore_attrs = 'data-initial='.esc_attr($amount).' data-increment=2';
                } else {
                    $amount = intval($rows) * 4;
                    $loadmore_attrs = 'data-initial='.esc_attr($amount).' data-increment=4';
                }
            }

            $output .= '<div class="masonry-grid '.esc_attr($loadmore_container).' '.esc_attr($row).'" '.esc_attr($loadmore_attrs).'>';

            $output .= wpb_js_remove_wpautop( $content );
            $output .= '</div>';

            if ('yes' === $loadmore){
                $output .= '<div class="row"><div class="col-md-12 align-center padding-top-20 padding-bottom-20">
                            <a href="#" role="button" id="loadMore" class="btn btn-md">'.esc_attr($loadmore_button).'</a>
                            </div>
                            </div>';
            }


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }



    }
}

