<?php
add_action( 'vc_before_init', 'synergy_icon_integrateWithVC' );

function synergy_icon_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Icon', 'synergy' ),
        'base' => 'synergy_icon',
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-vc_icon',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Url', 'synergy' ),
                'param_name' => 'url',
            ),

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Color Scheme",
                "param_name" => "color_scheme",
                "value" => array(
                    "White icon, transparent background, change on hover" => "white-icon",
                    "Dark icon, transparent background, change on hover" => "dark-icon",
                    "White icon, dark background, change on hover" => "dark-bg",
                    "Dark icon, light background, change on hover" => "light-bg",
                )
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Icon library', 'js_composer' ),
                'value' => array(
                    __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
                    __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                    __( 'Typicons', 'js_composer' ) => 'typicons',
                    __( 'Entypo', 'js_composer' ) => 'entypo',
                    __( 'Linecons', 'js_composer' ) => 'linecons',
                    __( 'Mono Social', 'js_composer' ) => 'monosocial',
                    __( 'Material', 'js_composer' ) => 'material',
                ),
                'admin_label' => true,
                'param_name' => 'type',
                'description' => __( 'Select icon library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_fontawesome',
                'value' => 'fa fa-adjust',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'fontawesome',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_openiconic',
                'value' => 'vc-oi vc-oi-dial',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'openiconic',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'openiconic',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_typicons',
                'value' => 'typcn typcn-adjust-brightness',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'typicons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'typicons',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_entypo',
                'value' => 'entypo-icon entypo-icon-note',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'entypo',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'entypo',
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_linecons',
                'value' => 'vc_li vc_li-heart',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'linecons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'linecons',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_monosocial',
                'value' => 'vc-mono vc-mono-fivehundredpx',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'monosocial',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'monosocial',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_material',
                'value' => 'vc-material vc-material-cake',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'material',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'material',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Icon Size', 'synergy' ),
                'param_name' => 'size',
                'value'      => array(
                    __( 'Small', 'synergy' )       => 'small-icon',
                    __( 'Large', 'synergy' )        => 'large-icon',
                ),
            ),



            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Top Margin", "synergy" ),
                "param_name" => "top_margin",
                "value" => getMargins()
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Bottom Margin", "synergy" ),
                "param_name" => "bottom_margin",
                "value" => getMargins()
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Left Margin", "synergy" ),
                "param_name" => "left_margin",
                "value" => getMargins()
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Right Margin", "synergy" ),
                "param_name" => "right_margin",
                "value" => getMargins()
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Icon extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $url = $color_scheme = $type = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypo = $icon_linecons = $icon_monosocial = $icon_material = $size = $top_margin = $bottom_margin = $left_margin = $right_margin = $css_animation = "";
            //$css_animation = "";
            extract(shortcode_atts(array(
                'url' => '#',
                'color_scheme' => 'white-icon',
                'type' => 'fontawesome',
                'icon_fontawesome' => 'fa fa-adjust',
                'icon_openiconic' => 'vc-oi vc-oi-dial',
                'icon_typicons' => 'typcn typcn-adjust-brightness',
                'icon_entypo' => 'entypo-icon entypo-icon-note',
                'icon_linecons' => 'vc_li vc_li-heart',
                'icon_monosocial' => 'vc-mono vc-mono-fivehundredpx',
                'icon_material' => 'vc-material vc-material-cake',
                'size' => 'small-icon',
                'top_margin' => '',
                'bottom_margin' => '',
                'left_margin' => '',
                'right_margin' => '',
                'css_animation' => '',
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            // Enqueue needed icon font.
            vc_icon_element_fonts_enqueue( $type );

            $iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';

            $styles = "";
            $custom_styles = "";

            if ($right_margin != 0) {
                $styles .= "margin-right:".$right_margin;
            }
            if ($left_margin != 0) {
                $styles .= ";margin-left:".$left_margin;
            }
            if ($top_margin != 0) {
                $styles .= ";margin-top:".$top_margin;
            }
            if ($bottom_margin != 0) {
                $styles .= ";margin-bottom:".$bottom_margin;
            }

            if ($styles !== "") {
                $custom_styles = 'style='.$styles.'';
            }


            //	if ($color == 'transparent') {$color == 'transparent dark';}

            $output = '<a href="'.esc_url($url).'" '.esc_attr($custom_styles).'  class="social-icon '.esc_attr($color_scheme).' '.esc_attr($size).' '.esc_attr($css_class).'">
            <i class="'.esc_attr($iconClass).'"></i>
            </a>';

            // $output = '<a href="'.esc_url($url).'" class="button '.esc_attr($color).' '.esc_attr($size).' '.esc_attr($bg_scheme).' '.esc_attr($icon).' '.esc_attr($css_class).'">'.esc_attr($title).'</a>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}

