<?php
add_action( 'vc_before_init', 'synergy_recent_integrateWithVC' );

function synergy_recent_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Recent Posts', 'synergy' ),
        'base' => 'synergy_recent',
        'show_settings_on_create' => true,
        'icon' => 'vc_icon-vc-masonry-grid',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' => __( 'Columns', 'synergy' ),
                'param_name' => 'columns',
                'value'      => array(
                    '2'       => '2',
                    '3'       => '3',
                    '4'       => '4'
                ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Posts to display', 'synergy' ),
                'param_name' => 'items',
                'value'      => array(
                    '2'       => '2',
                    '3'       => '3',
                    '4'       => '4',
                    '6'       => '6',
                    '8'       => '8'
                ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Display features?', 'synergy' ),
                'param_name' => 'features',
                'value'      => array(
                    __( 'Yes', 'synergy' )       => 'yes',
                    __( 'No', 'synergy' )        => 'no',
                ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Feature Type', 'synergy' ),
                'param_name' => 'feature_type',
                'dependency' => array(
                    'element' => 'features',
                    'value' => 'yes',
                ),
                'value'      => array(
                    __( 'Images only', 'synergy' )       => 'images',
                    __( 'All types', 'synergy' )        => 'all',
                ),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __( 'Item background Color', 'js_composer' ),
                'param_name' => 'bg',
                'value' => '#ffffff'
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            )

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Recent extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $columns = $items = $features = $feature_type = $bg = $css_animation = "";
            //$css_animation = "";
            extract(shortcode_atts(array(
                'columns' => '4',
                'items' => '8',
                'features' => 'yes',
                'feature_type' => 'images',
                'bg' => '#ffffff',
                'css_animation' => ''
            ), $atts));


            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            if ($columns == '2') {
                $masonry_type = 'masonry-2-col';
            } else if ($columns == '3') {
                $masonry_type = 'masonry-3-col';
            } else {
                $masonry_type = '';
                $per_page = 8;
            }

            $output = '';
            $output .= '<div class="masonry-grid blog-masonry '.esc_attr($masonry_type).'">';

            $query = new WP_Query('post_type=post &posts_per_page='.esc_attr($items).'');
            if($query->have_posts()): while($query->have_posts()) : $query->the_post();

                $postid = get_the_ID();
                $title = get_the_title($postid);
                $content = get_the_content();

                $output .= '<div class="item-masonry">';
                $output .= '<div class="well-masonry" style="background-color:'.esc_attr($bg).'">';
                $output .= '<div class="news padding-20 margin-bottom-10 '.esc_attr($css_class).'">';

                if ($features == 'yes') {
                    ob_start();
                    if ($feature_type == 'all') {
                        get_template_part( 'post-formats/single', get_post_format());
                    } else {
                        get_template_part( 'post-formats/single');
                    }
                    $output .=  ob_get_clean();
                }
                $output .= '<a href="'.get_the_permalink().'" class="post-title"><h5>'.$title.'</h5></a>';
                $output .= '<p>';
                $output .= wp_trim_words( $content , rand ( 15 , 35 ) );
                $output .= '</p>';
                $output .= '<span class="post-info">';
                $output .= '<i class="fa fa-pencil-square"></i>';
                $output .=  __('by', 'synergy') . ' ' . get_the_author_link();
                $output .= '<i class="fa fa-calendar"></i>';
                $output .=  __('on ', 'synergy') . ' ' . get_the_date();
                $output .= '</span>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';

            endwhile;
            endif;
            $output .= '</div>';

            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
