<?php

add_action( 'vc_before_init', 'synergy_lists_integrateWithVC' );

function synergy_lists_integrateWithVC() {
    vc_map( array(
        "name" => __( 'List', 'synergy' ),
        'base' => 'synergy_lists',
        'as_parent'  => array('only' => 'synergy_listitem'),
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-ui-separator-label',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'is_container' => true,
        "js_view" => 'VcColumnView',
        'params' => array(

            array(
                'type' => 'dropdown',
                'heading' => __( 'List icon type', 'synergy' ),
                'param_name' => 'type',
                'admin_label' => true,
                'value' => array(
                    __( 'Bullet', 'synergy' ) => 'bullet',
                    __( 'Circle arrow', 'synergy' ) => 'circlearrow',
                    __( 'Arrow', 'synergy' ) => 'arrow',
                    __( 'Chevron', 'synergy' ) => 'chevron',
                    __( 'Check', 'synergy' ) => 'check',
                    __( 'Check - circle', 'synergy' ) => "check-circle",
                    __( 'Check - square', 'synergy' ) => "check-square",
                    __( 'X (close)', 'synergy' ) => 'xclose',
                    __( 'Forward', 'synergy' ) => 'forward',
                    __( 'Toggle', 'synergy' ) => 'toggles',
                    __( 'X - circle', 'synergy' ) => 'times',
                    __( 'Play', 'synergy' ) => 'play'

                ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            )
        ),
    ));
}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_synergy_lists extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $css_animation = $type = "";
            extract(shortcode_atts(array(
                'type' => 'bullet',
                'css_animation' => ''
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            $output = '<ul class="item-list '.esc_attr($css_class).' '.esc_attr($type).'">';
            $output .= wpb_js_remove_wpautop( $content );
            $output .= '</ul>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }



    }
}

