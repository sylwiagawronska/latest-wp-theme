<?php

add_action( 'vc_before_init', 'synergy_portfolio_integrateWithVC' );

function synergy_portfolio_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Portfolio Slider', 'synergy' ),
        'base' => 'synergy_portfolio',
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-slideshow',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' => __( 'Display title?', 'synergy' ),
                'param_name' => 'display_title',
                'value'      => array(
                    __( 'Yes', 'synergy' )       => 'yes',
                    __( 'No', 'synergy' )        => 'no',
                ),
            ),

            array(
                'type' => 'textfield',
                'heading' => __( 'Title - part 1', 'synergy' ),
                'param_name' => 'title_one',
                'dependency' => array(
                    'element' => 'display_title',
                    'value' => 'yes',
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Title - part 2', 'synergy' ),
                'param_name' => 'title_two',
                'dependency' => array(
                    'element' => 'display_title',
                    'value' => 'yes',
                ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Display filters?', 'synergy' ),
                'param_name' => 'display_filters',
                'value'      => array(
                    __( 'Yes', 'synergy' )       => 'yes',
                    __( 'No', 'synergy' )        => 'no',
                ),
            ),
            array(
                'type' => 'textfield',
                'dependency' => array(
                    'element' => 'display_filters',
                    'value' => 'yes',
                ),
                'heading' => __( 'Display All Filter Label', 'synergy' ),
                'param_name' => 'all',
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Portfolio extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $display_title = $title_one = $title_two = $display_filters = $all = "";
            //$css_animation = "";
            extract(shortcode_atts(array(
                'display_title' => 'yes',
                'title_one' => 'Our recent',
                'title_two' => 'work',
                'display_filters' => 'yes',
                'all' => 'Show All'
            ), $atts));

            $output = "";

            if ($display_title == 'yes' || $display_filters == 'yes') {
                $output .= '<div class="row padding-bottom-50">';
                $output .= '<div class="col-md-5 padding-bottom-40 wow fadeIn">';
                if ($display_title == 'yes') {
                    $output .= '<h1 class="inline-block">'.esc_attr($title_one).' </h1>';
                    $output .= '<h1 class="medium-thin-font inline-block">'.esc_attr($title_two).'</h1>';
                }
                $output .= '</div>';
                $output .= '<div class="col-md-7 padding-bottom-40 wow fadeIn portfolioFilters align-right">';

                if ($display_filters == 'yes') {
                    $portfolio_categories = get_terms( array(
                        'taxonomy' => 'portfolio_categories',
                        'hide_empty' => false,
                    ) );
                    if ($portfolio_categories):
                        foreach ($portfolio_categories as $portfolio_category):
                            $output .= '<a href="#" data-filter-by="filter-'.esc_attr($portfolio_category -> slug).'">'.esc_attr($portfolio_category -> name).'</a>';
                        endforeach;
                    endif;
                    $output .= '<a href="#" data-filter-by="filter-all" class="active-filter">'.esc_attr($all).'</a>';
                }
                $output .= '</div>';
                $output .= '</div>';
            }

            // a container to which portfolio items will be dynamically loaded
            $output .= '<div class="row">';
            $output .= '<div class="col-md-12">';
            $output .= '<div id="ajaxPortfolioContainer">';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';



            $output .= '<div class="portfolio-carousel">';


            $i=0;
            $query = new WP_Query('post_type=portfolio &posts_per_page=-1');
            if($query->have_posts()): while($query->have_posts()) : $query->the_post();
                $postid = get_the_ID();
                $portfolio_title = get_the_title($postid);
                $i++;
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($postid), 'portfolio-thumb' );
                $url = $thumb['0'];
                $content = get_the_content();

                $categories = get_the_terms( get_the_ID(), 'portfolio_categories' );
                $slug = '';
                if($categories) {
                    foreach ($categories as $category) {
                        $slug .= 'filter-';
                        $slug .= $category->slug;
                        $slug .= ' ';
                    }
                } else {
                    $slug = 'filter-none';
                }

                if ($i % 2 != 0) {
                    $output .= '<div class="portfolio-item padding-bottom-50" data-filter="'.$slug.'">';
                    $output .= '<div class="portfolio-description">';
                    $output .= '<h4>'.$portfolio_title.'</h4>';
                    $output .= '<p>';
                    $output .= wp_trim_words( $content , 20 );
                    $output .= '</p>';
                    $output .= '</div>';
                    $output .= '<div class="portfolio-picture margin-top-30" style="background-image:url('.esc_url($url).')">';
                    $output .= '<div class="portfolio-hover">';
                    $output .= '<a href="'.esc_url($url).'" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>';
                    $output .= '<a href="'.get_the_permalink().'" class="load-item"><i class="fa fa-link"></i></a>';
                    $output .= '</div>';
                    $output .= '</div>';
                    $output .= '</div>';
                } else {
                    $output .= '<div class="portfolio-item padding-bottom-50"  data-filter="'.$slug.'">';
                    $output .= '<div class="portfolio-picture margin-bottom-30" style="background-image:url('.esc_url($url).')">';
                    $output .= '<div class="portfolio-hover">';
                    $output .= '<a href="'.esc_url($url).'" data-rel="prettyPhoto"><i class="fa fa-search"></i></a>';
                    $output .= '<a href="'.get_the_permalink().'" class="load-item"><i class="fa fa-link"></i></a>';
                    $output .= '</div>';
                    $output .= '</div>';
                    $output .= '<div class="portfolio-description">';
                    $output .= '<h4>'.$portfolio_title.'</h4>';
                    $output .= '<p>';
                    $output .= wp_trim_words( $content , 20 );
                    $output .= '</p>';
                    $output .= '</div>';
                    $output .= '</div>';
                }



            endwhile;
            endif;


            $output .= '</div>';
            //$output = '<a href="'.esc_url($url).'"'.esc_attr($custom_styles).' '.$custom_colors.' role="button" class="btn '.esc_attr($color).' '.esc_attr($size).' '.esc_attr($bg_scheme).' '.esc_attr($icon).' '.esc_attr($css_class).'">READ MORE</a>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}

