<?php


add_action( 'vc_before_init', 'synergy_teamcarousel_integrateWithVC' );

function synergy_teamcarousel_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Team carousel', 'synergy' ),
        'base' => 'synergy_teamcarousel',
        'as_parent'  => array('only' => 'synergy_teamitem'),
        'show_settings_on_create' => true,
        // 'icon' => 'icon-wpb-vc_icon',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'is_container' => true,
        "js_view" => 'VcColumnView',
        'params' => array(

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Color Scheme",
                "param_name" => "color_schemes",
                "value" => array(
                    "For light backgrounds" => "light-team-carousel",
                    "For dark backgrounds" => "dark-team-carousel",
                )
            )
        ),
    ));
}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_synergy_Teamcarousel extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $color_schemes = "";
            extract(shortcode_atts(array(
                'color_schemes' => 'light-team-carousel'
            ), $atts));

            $output = '<div class="team-carousel '.esc_attr($color_schemes).'">';
            // $output .= wpb_js_remove_wpautop( $content );
            $output .=do_shortcode( $content );
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }



    }
}

