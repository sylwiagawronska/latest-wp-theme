<?php

add_action( 'vc_before_init', 'synergy_counter_integrateWithVC' );

function synergy_counter_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Counter', 'synergy' ),
        'base' => 'synergy_counter',
        'show_settings_on_create' => true,
         'icon' => 'icon-wpb-toggle-small-expand',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Count to', 'synergy' ),
                'param_name' => 'count',
                'description' => __( 'Enter a numeric value, e.g. 512', 'synergy' ),
            ),

            array(
                'type' => 'textfield',
                'heading' => __( 'Label', 'synergy' ),
                'param_name' => 'label',
            ),

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Color Scheme",
                "param_name" => "color_scheme",
                "value" => array(
                    __( 'For dark background', 'synergy' ) => "dark-bg-counter",
                    __( 'For light background', 'synergy' ) => "light-bg-counter",
                )
            ),


            array(
                'type' => 'dropdown',
                'heading' => __( 'Icon library', 'js_composer' ),
                'value' => array(
                    __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
                    __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                    __( 'Typicons', 'js_composer' ) => 'typicons',
                    __( 'Entypo', 'js_composer' ) => 'entypo',
                    __( 'Linecons', 'js_composer' ) => 'linecons',
                    __( 'Mono Social', 'js_composer' ) => 'monosocial',
                    __( 'Material', 'js_composer' ) => 'material',
                ),
                'admin_label' => true,
                'param_name' => 'type',
                'description' => __( 'Select icon library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_fontawesome',
                'value' => 'fa fa-adjust',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'fontawesome',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_openiconic',
                'value' => 'vc-oi vc-oi-dial',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'openiconic',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'openiconic',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_typicons',
                'value' => 'typcn typcn-adjust-brightness',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'typicons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'typicons',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_entypo',
                'value' => 'entypo-icon entypo-icon-note',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'entypo',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'entypo',
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_linecons',
                'value' => 'vc_li vc_li-heart',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'linecons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'linecons',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_monosocial',
                'value' => 'vc-mono vc-mono-fivehundredpx',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'monosocial',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'monosocial',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_material',
                'value' => 'vc-material vc-material-cake',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'material',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'material',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),


        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Counter extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $count = $label = $color_scheme = $type = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypo = $icon_linecons = $icon_monosocial = $icon_material = "";
            //$css_animation = "";
            extract(shortcode_atts(array(
                'count' => '12',
                'label' => '',
                'color_scheme' => 'dark-bg-counter',
                'type' => 'fontawesome',
                'icon_fontawesome' => 'fa fa-heart',
                'icon_openiconic' => 'vc-oi vc-oi-dial',
                'icon_typicons' => 'typcn typcn-adjust-brightness',
                'icon_entypo' => 'entypo-icon entypo-icon-note',
                'icon_linecons' => 'vc_li vc_li-heart',
                'icon_monosocial' => 'vc-mono vc-mono-fivehundredpx',
                'icon_material' => 'vc-material vc-material-cake'
            ), $atts));

            // Enqueue needed icon font.
            vc_icon_element_fonts_enqueue( $type );

            $iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';

            $output = '';

            $output .= '<div class="counter '.esc_attr($color_scheme).'">';
            $output .= '<i class="'.esc_attr($iconClass).'"></i>';
            $output .= '<div class="counter-data">';
            $output .= '<h2 class="count-up" data-target-value='.esc_attr($count).'>0</h2>';
            $output .= '<span class="footnote">'.esc_attr($label).'</span>';
            $output .= '</div>';
            $output .= '</div>';

            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
