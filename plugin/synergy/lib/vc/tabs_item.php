<?php

add_action( 'vc_before_init', 'synergy_tabsitem_integrateWithVC' );


function synergy_tabsitem_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Tab Section', 'synergy' ),
        'base' => 'synergy_tabsitem',
        "as_child" => array('only' => 'synergy_tabs'),
        'show_settings_on_create' => true,
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'icon' => 'icon-wpb-ui-tab-content',
        'params' => array(

            array(
                'type' => 'textfield',
                'heading' => __( 'Section Title', 'synergy' ),
                'param_name' => 'name',
            ),

            array(
                'type' => 'textfield',
                'heading' => __( 'Section ID', 'synergy' ),
                'param_name' => 'id',
                'description' => __( 'Must be unique', 'synergy' )
            ),

            array(
                'type' => 'textarea_html',
                'heading' => __( 'Section Content', 'synergy' ),
                'param_name' => 'content',
            ),


        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Tabsitem extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $name = $id = $bg = $color = $open = "";

            //$css_animation = "";
            extract(shortcode_atts(array(
                'name' => '',
                'id' => '',
            ), $atts));


            if ($id == "") {
                $id = str_replace(' ', '-', $name);
                $id = preg_replace('/[^A-Za-z0-9\-]/', '', $id);
                $id .= rand(0,9999);
            }


            $output = '';
            $output .= '<div class="tab-pane fade" id="'.esc_attr($id).'" data-name="'.esc_attr($name).'">';
            $output .= '<p>';
            $output .= wpb_js_remove_wpautop($content);
            $output .= '</p>';
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}