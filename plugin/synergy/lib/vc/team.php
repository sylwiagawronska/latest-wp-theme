<?php


add_action( 'vc_before_init', 'synergy_team_integrateWithVC' );

function synergy_team_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Team member', 'synergy' ),
        'base' => 'synergy_team',
        'as_parent'  => array('only' => 'synergy_icon'),
        'show_settings_on_create' => true,
         'icon' => 'icon-wpb-vc-round-chart',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'is_container' => true,
        "js_view" => 'VcColumnView',
        'params' => array(

            array(
                'type' => 'textfield',
                'heading' => __( 'Name', 'synergy' ),
                'param_name' => 'name',
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Title', 'synergy' ),
                'param_name' => 'title',
            ),
            array(
                'type' => 'attach_image',
                'heading' => __( 'Image', 'synergy' ),
                'param_name' => 'image',
            ),
            array(
                'type' => 'textarea',
                'heading' => __( 'Description', 'synergy' ),
                'param_name' => 'description',
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'Include link?', 'synergy' ),
                'param_name' => 'link',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => 'no',
                    __( 'Yes', 'synergy' ) => 'yes'
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Url', 'synergy' ),
                'param_name' => 'url',
                'dependency' => array(
                    'element' => 'link',
                    'value' => 'yes',
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}

//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_synergy_Team extends WPBakeryShortCodesContainer {

        protected function content($atts, $content = null) {

            $name = $title = $image = $description = $link = $url = $css_animation = "";
            extract(shortcode_atts(array(
                'name' => '',
                'title' => '',
                'image' => '',
                'description' => '',
                'link' => 'no',
                'url' => '#',
                'css_animation' => ''
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            $img = wp_get_attachment_image_src($image, "full");
            $imgSrc = $img[0];

            $output = '';

            $output .= '<div class="team-member align-center padding-bottom-40 '.esc_attr($css_class).'">';

            if ('yes' === $link) {
                $output .= '<a href="'.esc_attr($url).'">';
            }
            $output .= '<img src="'.esc_attr($imgSrc).'" alt="'.esc_attr($name).'">';

            if ('yes' === $link) {
                $output .= '</a>';
            }
            $output .= '<h3 class="padding-top-30">'.esc_attr($name).'</h3>';
            $output .= '<h4 class="padding-bottom-40 thin-font">'.esc_attr($title).'</h4>';
            $output .= '<p>'.esc_attr($description).'</p>';
            $output .= '<div class="padding-top-20">';
            $output .= wpb_js_remove_wpautop( $content );
            $output .= '</div>';
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }



    }
}

