<?php

add_action( 'vc_before_init', 'synergy_listitem_integrateWithVC' );


function synergy_listitem_integrateWithVC() {
    vc_map( array(
        "name" => __( 'List Item', 'synergy' ),
        'base' => 'synergy_listitem',
        "as_child" => array('only' => 'synergy_lists'),
        'show_settings_on_create' => true,
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'icon' => 'icon-wpb-ui-separator-label',
        'params' => array(

            array(
                'type' => 'textfield',
                'heading' => __( 'Item text', 'synergy' ),
                'param_name' => 'text',
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Listitem extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $text = "";
            extract(shortcode_atts(array(
                'text' => ''
            ), $atts));



            $output = '<li>'.esc_attr($text).'</li>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
