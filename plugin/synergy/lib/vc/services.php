<?php

add_action( 'vc_before_init', 'synergy_services_integrateWithVC' );

function synergy_services_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Service Box', 'synergy' ),
        'base' => 'synergy_services',
        'show_settings_on_create' => true,
         'icon' => 'icon-wpb-toggle-small-expand',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Title', 'synergy' ),
                'param_name' => 'title',
            ),

            array(
                'type' => 'textarea',
                'heading' => __( 'Description', 'synergy' ),
                'param_name' => 'description',
            ),

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Color Scheme",
                "param_name" => "color_scheme",
                "value" => array(
                    __( 'Primary accent color', 'synergy' ) => "service-dark",
                    __( 'Secondary accent color', 'synergy' ) => "service-light",
                )
            ),

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Design type",
                "param_name" => "design",
                "value" => array(
                    __( 'Icon on the left', 'synergy' ) => "left",
                    __( 'Icon on top', 'synergy' ) => "top"
                )
            ),


            array(
                'type' => 'dropdown',
                'heading' => __( 'Icon library', 'js_composer' ),
                'value' => array(
                    __( 'Font Awesome', 'js_composer' ) => 'fontawesome',
                    __( 'Open Iconic', 'js_composer' ) => 'openiconic',
                    __( 'Typicons', 'js_composer' ) => 'typicons',
                    __( 'Entypo', 'js_composer' ) => 'entypo',
                    __( 'Linecons', 'js_composer' ) => 'linecons',
                    __( 'Mono Social', 'js_composer' ) => 'monosocial',
                    __( 'Material', 'js_composer' ) => 'material',
                ),
                'admin_label' => true,
                'param_name' => 'type',
                'description' => __( 'Select icon library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_fontawesome',
                'value' => 'fa fa-adjust',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'fontawesome',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_openiconic',
                'value' => 'vc-oi vc-oi-dial',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'openiconic',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'openiconic',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_typicons',
                'value' => 'typcn typcn-adjust-brightness',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'typicons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'typicons',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_entypo',
                'value' => 'entypo-icon entypo-icon-note',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'entypo',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'entypo',
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_linecons',
                'value' => 'vc_li vc_li-heart',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'linecons',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'linecons',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_monosocial',
                'value' => 'vc-mono vc-mono-fivehundredpx',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'monosocial',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'monosocial',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( 'Icon', 'js_composer' ),
                'param_name' => 'icon_material',
                'value' => 'vc-material vc-material-cake',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'type' => 'material',
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'type',
                    'value' => 'material',
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Services extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $title = $description = $color_scheme = $design = $type = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypo = $icon_linecons = $icon_monosocial = $icon_material = $css_animation = "";
            //$css_animation = "";
            extract(shortcode_atts(array(
                'title' => '',
                'description' => '',
                'color_scheme' => 'service-dark',
                'design' => 'left',
                'type' => 'fontawesome',
                'icon_fontawesome' => 'fa fa-heart',
                'icon_openiconic' => 'vc-oi vc-oi-dial',
                'icon_typicons' => 'typcn typcn-adjust-brightness',
                'icon_entypo' => 'entypo-icon entypo-icon-note',
                'icon_linecons' => 'vc_li vc_li-heart',
                'icon_monosocial' => 'vc-mono vc-mono-fivehundredpx',
                'icon_material' => 'vc-material vc-material-cake',
                'css_animation' => '',
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            // Enqueue needed icon font.
            vc_icon_element_fonts_enqueue( $type );

            $iconClass = isset( ${'icon_' . $type} ) ? esc_attr( ${'icon_' . $type} ) : 'fa fa-adjust';

            $output = '';
            $output .= '<div class="service '.esc_attr($color_scheme).' '.esc_attr($css_class).'">';
            $output .= '<div class="row padding-bottom-30">';
            if ('top' === $design) {
                $output .= '<div class="col-md-12 col-sm-12 col-xs-12 padding-bottom-30">';
            } else {
                $output .= '<div class="col-md-3 col-sm-3 col-xs-3">';
            }
            $output .= '<div class="service-icon">';
            $output .= '<i class="'.esc_attr($iconClass).'"></i>';
            $output .= '</div>';
            $output .= '</div>';
            if ('top' === $design) {
                $output .= '<div class="col-md-12 col-sm-12 col-xs-12 align-center">';
            } else {
                $output .= '<div class="col-md-9 col-sm-9 col-xs-9">';
            }
            $output .= '<h4 class="padding-bottom-30">'.esc_attr($title).'</h4>';
            $output .= '<p>'.esc_attr($description).'</p>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
