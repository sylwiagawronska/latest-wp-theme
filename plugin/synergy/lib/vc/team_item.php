<?php

add_action( 'vc_before_init', 'synergy_teamitem_integrateWithVC' );

function synergy_teamitem_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Team member', 'synergy' ),
        'base' => 'synergy_teamitem',
        "as_child" => array('only' => 'synergy_teamcarousel'),
        'show_settings_on_create' => true,
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'params' => array(

            array(
                'type' => 'textfield',
                'heading' => __( 'Name', 'synergy' ),
                'param_name' => 'name',
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Title', 'synergy' ),
                'param_name' => 'title',
            ),
            array(
                'type' => 'attach_image',
                'heading' => __( 'Image', 'synergy' ),
                'param_name' => 'image',
            ),
            array(
                'type' => 'textarea_html',
                'heading' => __( 'Description', 'synergy' ),
                'param_name' => 'content',
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => "Color Scheme",
                "param_name" => "color_scheme",
                "value" => array(
                    "For light backgrounds" => "dark-bg",
                    "For dark backgrounds" => "white-icon",
                )
            ),
            array(
                "type" => "checkbox",
                "class" => "",
                "heading" => "Enable Social Icons",
                "param_name" => "social",
                "value"       => array(
                    'First Icon'=>'first',
                    'Second Icon'=>'second',
                    'Third Icon'=>'third',
                    'Fourth Icon'=>'fourth',
                    'Fifth Icon'=>'fifth',
                    'Sixth Icon'=>'sixth'
                ), //value
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( '1st Social Icon', 'js_composer' ),
                'param_name' => 'icon_one',
                'value' => 'fa fa-glass',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( '1st Social Url', 'synergy' ),
                'param_name' => 'url_one',
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __( '2nd Social Icon', 'js_composer' ),
                'param_name' => 'icon_two',
                'value' => 'fa fa-glass',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( '2nd Social Url', 'synergy' ),
                'param_name' => 'url_two',
            ),

            array(
                'type' => 'iconpicker',
                'heading' => __( '3rd Social Icon', 'js_composer' ),
                'param_name' => 'icon_three',
                'value' => 'fa fa-glass',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( '3rd Social Url', 'synergy' ),
                'param_name' => 'url_three',
            ),

            array(
                'type' => 'iconpicker',
                'heading' => __( '4th Social Icon', 'js_composer' ),
                'param_name' => 'icon_four',
                'value' => 'fa fa-glass',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( '4th Social Url', 'synergy' ),
                'param_name' => 'url_four',
            ),

            array(
                'type' => 'iconpicker',
                'heading' => __( '5th Social Icon', 'js_composer' ),
                'param_name' => 'icon_five',
                'value' => 'fa fa-glass',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( '5th Social Url', 'synergy' ),
                'param_name' => 'url_five',
            ),

            array(
                'type' => 'iconpicker',
                'heading' => __( '6th Social Icon', 'js_composer' ),
                'param_name' => 'icon_six',
                'value' => 'fa fa-glass',
                // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                    // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'description' => __( 'Select icon from library.', 'js_composer' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( '6th Social Url', 'synergy' ),
                'param_name' => 'url_six',
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Teamitem extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $name = $title = $image = $color_scheme = $social = "";
            $icon_one = $icon_two = $icon_three = $icon_four = $icon_five = $icon_six = "";
            $url_one = $url_two = $url_three = $url_four = $url_five = $url_six = "";

            //$css_animation = "";
            extract(shortcode_atts(array(
                'name' => '',
                'title' => '',
                'image' => '',
                'color_scheme' => 'dark-bg',
                'social' => '',
                'icon_one' => 'fa fa-facebook',
                'url_one' => '#',
                'icon_two' => '',
                'url_two' => '#',
                'icon_three' => '',
                'url_three' => '#',
                'icon_four' => '',
                'url_four' => '#',
                'icon_five' => '',
                'url_five' => '#',
                'icon_six' => '',
                'url_six' => '#'
            ), $atts));

            $img = wp_get_attachment_image_src($image, "full");
            $imgSrc = $img[0];

            $socials = "";

            if ((strpos($social, 'first') !== false) && $icon_one != "") {
                $socials .= '<a href="'.esc_attr($url_one).'" class="social-icon large-icon '.esc_attr($color_scheme).'"><i class="'.esc_attr($icon_one).'"></i></a>';
            }
            if ((strpos($social, 'second') !== false) && $icon_two != "") {
                $socials .= '<a href="'.esc_attr($url_two).'" class="social-icon large-icon '.esc_attr($color_scheme).'"><i class="'.esc_attr($icon_two).'"></i></a>';
            }
            if ((strpos($social, 'third') !== false) && $icon_three != "") {
                $socials .= '<a href="'.esc_attr($url_three).'" class="social-icon large-icon '.esc_attr($color_scheme).'"><i class="'.esc_attr($icon_three).'"></i></a>';
            }
            if ((strpos($social, 'fourth') !== false) && $icon_four != "") {
                $socials .= '<a href="'.esc_attr($url_four).'" class="social-icon large-icon '.esc_attr($color_scheme).'"><i class="'.esc_attr($icon_four).'"></i></a>';
            }
            if ((strpos($social, 'fifth') !== false) && $icon_five != "") {
                $socials .= '<a href="'.esc_attr($url_five).'" class="social-icon large-icon '.esc_attr($color_scheme).'"><i class="'.esc_attr($icon_five).'"></i></a>';
            }
            if ((strpos($social, 'sixth') !== false) && $icon_six != "") {
                $socials .= '<a href="'.esc_attr($url_six).'" class="social-icon large-icon '.esc_attr($color_scheme).'"><i class="'.esc_attr($icon_six).'"></i></a>';
            }


            $output = '<div class="row team">
                <div class="col-md-2 col-sm-1 col-xs-1">

                </div>
                <div class="col-md-4 col-sm-10 col-xs-10 align-right">

                    <div class="team-description padding-right-35">
                        <h1>'.esc_attr($name).'</h1>
                        <h2 class="padding-bottom-40 thin-font">'.esc_attr($title).'</h2>
                        <p>'.wpb_js_remove_wpautop($content).'</p>

                    </div>
                    <div class="padding-right-35">
                        '.$socials.'
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 align-center">
                    <img src="'.esc_attr($imgSrc).'" alt="'.esc_attr($name).'">
                </div>

                <div class="col-md-2 col-sm-1 col-xs-1">

                </div>
            </div>';

            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
