<?php

add_action( 'vc_before_init', 'synergy_button_integrateWithVC' );

function synergy_button_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Button', 'synergy' ),
        'base' => 'synergy_button',
        'show_settings_on_create' => true,
        'icon' => 'icon-wpb-ui-button',
        'category' => __( 'Synergy Shortcodes', 'synergy' ),

        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __( 'Title', 'synergy' ),
                'param_name' => 'title',
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Url', 'synergy' ),
                'param_name' => 'url',
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Button Size', 'synergy' ),
                'param_name' => 'size',
                'value'      => array(
                    __( 'Small', 'synergy' )       => 'btn-md',
                    __( 'Large', 'synergy' )        => 'btn-lg',
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Button Color', 'synergy' ),
                'param_name' => 'color',
                'value'      => array(
                    __( 'Light', 'synergy' )       => 'btn-light',
                    __( 'Dark', 'synergy' )       => 'btn-dark',
                    __( 'Transparent', 'synergy' )       => 'btn-transparent',
                    __( 'Custom', 'synergy' )       => 'custom',

                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Backgroung type on which the button will be displayed', 'synergy' ),
                'param_name' => 'bg_scheme',
                'value'      => array(
                    __( 'Dark Background', 'synergy' )       => 'btn-dark-bg',
                    __( 'Light Background', 'synergy' )       => 'btn-light-bg',

                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Box Style', 'synergy' ),
                'param_name' => 'icon',
                'value'      => array(
                    __( 'Thin Arrow', 'synergy' )       => 'btn-thin-arrow',
                    __( 'Bold Arrow', 'synergy' )       => 'btn-bold-arrow',
                    __( 'Shopping Cart', 'synergy' )       => 'btn-shopping',
                    __( 'Forward arrow', 'synergy' )       => 'btn-forward',
                    __( 'Resize', 'synergy' )       => 'btn-resize',
                    __( 'Check circle', 'synergy' )       => 'btn-check',

                ),
            ),

            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __( "Custom background color", "synergy" ),
                "param_name" => "bg_color",
                "value" => '#ffffff'
            ),
            array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => __( "Custom text color", "synergy" ),
                "param_name" => "text_color",
                "value" => '#000000'
            ),

            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Top Margin", "synergy" ),
                "param_name" => "top_margin",
                "value" => getMargins()
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Bottom Margin", "synergy" ),
                "param_name" => "bottom_margin",
                "value" => getMargins()
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Left Margin", "synergy" ),
                "param_name" => "left_margin",
                "value" => getMargins()
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __( "Right Margin", "synergy" ),
                "param_name" => "right_margin",
                "value" => getMargins()
            ),

            array(
                'type' => 'dropdown',
                'heading' => __( 'CSS Animation', 'synergy' ),
                'param_name' => 'css_animation',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Top to bottom', 'synergy' ) => 'top-to-bottom',
                    __( 'Bottom to top', 'synergy' ) => 'bottom-to-top',
                    __( 'Left to right', 'synergy' ) => 'left-to-right',
                    __( 'Right to left', 'synergy' ) => 'right-to-left',
                    __( 'Appear from center', 'synergy' ) => "appear",
                    __( 'Fade In', 'synergy' ) => "fadeIn"
                ),
                'description' => __( 'Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'synergy' )
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Button extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $css_animation = $title = $url = $size = $color = $bg_scheme = $icon = $bg_color = $text_color = "";
            $top_margin = $bottom_margin = $left_margin = $right_margin = "";
            extract(shortcode_atts(array(
                'title' => '',
                'url' => '',
                'size' => 'btn-md',
                'color' => 'btn-light',
                'bg_scheme' => 'btn-dark-bg',
                'icon' => '',
                'bg_color' => '#aaaaaa',
                'text_color' => '#ffffff',
                'top_margin' => '',
                'bottom_margin' => '',
                'left_margin' => '',
                'right_margin' => '',
                'css_animation' => '',
            ), $atts));

            if ($css_animation == 'fadeIn') { $css_class = 'wow fadeIn';} else {
                $css_class = $this->getCSSAnimation($css_animation);
            }

            $styles = "";
            $custom_styles = "";

            if ($right_margin != 0) {
                $styles .= "margin-right:".$right_margin;
            }
            if ($left_margin != 0) {
                $styles .= ";margin-left:".$left_margin;
            }
            if ($top_margin != 0) {
                $styles .= ";margin-top:".$top_margin;
            }
            if ($bottom_margin != 0) {
                $styles .= ";margin-bottom:".$bottom_margin;
            }

            if ($styles !== "") {
                $custom_styles = 'style='.$styles.'';
            }

            $custom_colors = "";
            if ($color == "custom") {
                $custom_colors .= 'style="background-color:'.$bg_color.';border:1px solid '.$bg_color.';color:'.$text_color.'""';
            }

            $output = '<a href="'.esc_url($url).'" '.esc_attr($custom_styles).' '.$custom_colors.' role="button" class="btn '.esc_attr($color).' '.esc_attr($size).' '.esc_attr($bg_scheme).' '.esc_attr($icon).' '.esc_attr($css_class).'">READ MORE</a>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}

