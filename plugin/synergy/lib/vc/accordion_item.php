<?php

add_action( 'vc_before_init', 'synergy_accordionitem_integrateWithVC' );


function synergy_accordionitem_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Toggle Section', 'synergy' ),
        'base' => 'synergy_accordionitem',
        "as_child" => array('only' => 'synergy_accordion'),
        'show_settings_on_create' => true,
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'icon' => 'icon-wpb-ui-accordion',
        'params' => array(

            array(
                'type' => 'textfield',
                'heading' => __( 'Section Title', 'synergy' ),
                'param_name' => 'name',
            ),

            array(
                'type' => 'textfield',
                'heading' => __( 'Section ID', 'synergy' ),
                'param_name' => 'id',
                'description' => __( 'Must be unique', 'synergy' )
            ),

            array(
                'type' => 'textarea_html',
                'heading' => __( 'Section Content', 'synergy' ),
                'param_name' => 'content',
            ),

            array(
                'type' => 'colorpicker',
                'heading' => __( 'Section Heading background Color', 'synergy' ),
                'param_name' => 'bg',
                'value' => '#1e1f20'
            ),
            array(
                'type' => 'colorpicker',
                'heading' => __( 'Section Heading font Color', 'synergy' ),
                'param_name' => 'color',
                'value' => '#ffffff'
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Initially open?', 'synergy' ),
                'param_name' => 'open',
                'admin_label' => true,
                'value' => array(
                    __( 'No', 'synergy' ) => '',
                    __( 'Yes', 'synergy' ) => 'in',
                ),
            ),


        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Accordionitem extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $name = $id = $bg = $color = $open = "";

            //$css_animation = "";
            extract(shortcode_atts(array(
                'name' => '',
                'id' => '',
                'bg' => '#1e1f20',
                'color' => '#ffffff',
                'open' => ''
            ), $atts));


            if ($id == "") {
                $id = str_replace(' ', '-', $name);
                $id = preg_replace('/[^A-Za-z0-9\-]/', '', $id);
                $id .= rand(0,9999);
            }


            $output = '';
            $output .= '<div class="panel panel-default">';
            $output .= '<div class="panel-heading" style="background:'.esc_attr($bg).'">';
            $output .= '<h4 class="panel-title">';
            $output .= '<a data-toggle="collapse" href="#'.esc_attr($id).'" style="color: '.esc_attr($color).'"><span class="medium-thin-font">';
            $output .= $name;
            $output .= '</span></a>';
            $output .= '</h4>';
            $output .= '</div>';
            $output .= '<div id="'.esc_attr($id).'" class="panel-collapse collapse '.esc_attr($open).'">';
            $output .= '<div class="panel-body">';
            $output .= '<p>';
            $output .= wpb_js_remove_wpautop($content);
            $output .= '</p>';
            $output .= '</div></div></div>';


            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
