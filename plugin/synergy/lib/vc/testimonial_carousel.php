<?php

add_action( 'vc_before_init', 'synergy_testimonialcarousel_integrateWithVC' );

function synergy_testimonialcarousel_integrateWithVC() {
    vc_map( array(
        "name" => __( 'Testimonial', 'synergy' ),
        'base' => 'synergy_testimonialcarousel',
        "as_child" => array('only' => 'synergy_testimonialscarousel'),
        'show_settings_on_create' => true,
        'category' => __( 'Synergy Shortcodes', 'synergy' ),
        'params' => array(

            array(
                'type' => 'textfield',
                'heading' => __( 'Name', 'synergy' ),
                'param_name' => 'name',
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Company', 'synergy' ),
                'param_name' => 'company',
            ),
            array(
                'type' => 'textarea',
                'heading' => __( 'Content', 'synergy' ),
                'param_name' => 'text',
            ),

            array(
                'type' => 'attach_image',
                'heading' => __( 'Image', 'synergy' ),
                'param_name' => 'image',
            ),

        ),
    ));
}


if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_synergy_Testimonialcarousel extends WPBakeryShortCode {

        protected function content($atts, $content = null) {

            $name = $company = $text = $image = "";

            //$css_animation = "";
            extract(shortcode_atts(array(
                'name' => '',
                'company' => '',
                'text' => '',
                'image' => '',
            ), $atts));

            $img = wp_get_attachment_image_src($image, "avatars");
            $imgSrc = $img[0];

            $output = '';

            $output .= '<div class="testimonial clearfix">';
            $output .= '<div class="ttm-content">';
            $output .= '<p>';
            $output .= esc_attr($text);
            $output .= '</p>';
            $output .= '</div>';
            $output .= '<div class="ttm-footer">';
            $output .= '<img src="'.esc_attr($imgSrc).'" alt="'.esc_attr($name).'">';
            $output .= '<h5 class="ttm-name padding-top-20">'.esc_attr($name).'</h5>';
            $output .= '<span class="ttm-company footnote">'.esc_attr($company).'</span>';
            $output .= '</div>';
            $output .= '<div class="clearfix"></div>';
            $output .= '</div>';

            return $output;
        }

        public function __construct( $settings ) {
            parent::__construct( $settings );
        }

    }
}
