<?php

/**
 * Margin values for icons
 * @return array
 */
function getMargins()
{
    $margins = array("0" => "None", "1" => "1px", "2" => "2px", "3" => "3px", "4" => "4px", "5" => "5px");
    return $margins;
}

/**
 * Percentage values for progress bars
 * @return array
 */
function getPercentage()
{
    $percentage = array();
    for ($i=1;$i<=100;$i++) {
        $percentage[$i] = $i;
    }

    return $percentage;
}


/**
 * Remove unused icons parameters
 */
function vc_after_init_actions() {

    // Remove Params
    if( function_exists('vc_remove_param') ){
        vc_remove_param( 'vc_icon', 'color' );
        vc_remove_param( 'vc_icon', 'custom_color' );
        vc_remove_param( 'vc_icon', 'custom_color' );
        vc_remove_param( 'vc_icon', 'background_color' );
        vc_remove_param( 'vc_icon', 'custom_background_color' );
    }
    // Remove Shortcodes
    if( function_exists('vc_remove_element') ){
        vc_remove_element('vc_tabs');
        vc_remove_element('vc_tab');
        vc_remove_element('vc_accordion');
        vc_remove_element('vc_accordion_tab');
        vc_remove_element('vc_button');
        vc_remove_element('vc_button2');
        vc_remove_element('vc_btn');
        vc_remove_element('vc_tta_tabs');
        vc_remove_element('vc_tta_accordion');
        vc_remove_element('vc_toggle');
        vc_remove_element('vc_tour');
    }


}
add_action( 'vc_after_init', 'vc_after_init_actions' );

/**
 * Add content align to inner columns
 */
vc_add_param("vc_column_inner", array(
    "type" => "dropdown",
    "group" => __( 'Align content', 'synergy' ),
    "class" => "",
    "heading" => __( 'Align', 'synergy' ),
    "param_name" => "column_align",
    "value" => array(
        __( 'Default', 'synergy' ) => "default",
        __( 'Align left', 'synergy' ) => "align_left",
        __( 'Align center', 'synergy' ) => "align-center",
        __( 'Align right', 'synergy' ) => "align_right"
    )
));


/**
 * Add spacer option to row
 */
$row_params = array(
    array(
        "type" => "dropdown",
        "group" => __( 'Include Spacer', 'synergy' ),
        "class" => "",
        "heading" => __( 'Include Row Spacer', 'synergy' ),
        "param_name" => "row_spacer",
        "value" => array(
            __( 'No', 'synergy' ) => "no",
            __( 'Yes', 'synergy' ) => "yes",
        )
    ),
    array(
        "type" => "dropdown",
        "group" => __( 'Include Spacer', 'synergy' ),
        "class" => "",
        "heading" => __( 'Spacer Position', 'synergy' ),
        "param_name" => "spacer_position",
        'dependency' => array(
            'element' => 'row_spacer',
            'value' => 'yes',
        ),
        "value" => array(
            __( 'Bottom', 'synergy' ) => "bottom",
            __( 'Top', 'synergy' ) => "top",
        )
    ),
    array(
        'type' => 'colorpicker',
        "group" => __( 'Include Spacer', 'synergy' ),
        'heading' => __( 'Background Color', 'synergy' ),
        'param_name' => 'bg_color',
        'value' => '#1e1f20',

        'dependency' => array(
            'element' => 'row_spacer',
            'value' => 'yes',
        )
    ),

    array(
        'type' => 'colorpicker',
        "group" => __( 'Include Spacer', 'synergy' ),
        'heading' => __( 'Content Color', 'synergy' ),
        'param_name' => 'content_color',
        'value' => '#ffffff',

        'dependency' => array(
            'element' => 'row_spacer',
            'value' => 'yes',
        )
    ),

    array(
        'type' => 'textfield',
        "group" => __( 'Include Spacer', 'synergy' ),
        'heading' => __( 'Caption #1', 'synergy' ),
        'param_name' => 'caption1',

        'dependency' => array(
            'element' => 'row_spacer',
            'value' => 'yes',
        )
    ),

    array(
        'type' => 'textfield',
        "group" => __( 'Include Spacer', 'synergy' ),
        'heading' => __( 'Caption #2', 'synergy' ),
        'param_name' => 'caption2',

        'dependency' => array(
            'element' => 'row_spacer',
            'value' => 'yes',
        )
    ),

    array(
        "type" => "dropdown",
        "group" => __( 'Include Spacer', 'synergy' ),
        "class" => "",
        "heading" => __( 'Include Link?', 'synergy' ),
        "param_name" => "link",
        "value" => array(
            __( 'No', 'synergy' ) => "no",
            __( 'Yes', 'synergy' ) => "yes",
        ),
        'dependency' => array(
            'element' => 'row_spacer',
            'value' => 'yes',
        ),
    ),
            array(
                'type' => 'textfield',
                "group" => __( 'Include Spacer', 'synergy' ),
                'heading' => __( 'Link url', 'synergy' ),
                'param_name' => 'url',
                'dependency' => array(
                    'element' => 'link',
                    'value' => 'yes',
                ),
            ),


);
vc_add_params( 'vc_row', $row_params );




// synergy icons
include_once dirname(__FILE__).'/vc/icons.php';
// synergy buttons
include_once dirname(__FILE__).'/vc/buttons.php';
// progress bars - regular
include_once dirname(__FILE__).'/vc/progress_bar.php';
// progress bars - round
include_once dirname(__FILE__).'/vc/progress_bar_round.php';
// step boxes
include_once dirname(__FILE__).'/vc/steps.php';
// team carousel container
include_once dirname(__FILE__).'/vc/team_carousel.php';
// team members for team carousel
include_once dirname(__FILE__).'/vc/team_item.php';
// slider of portfolio items
include_once dirname(__FILE__).'/vc/portfolio_slider.php';
// recent posts display
include_once dirname(__FILE__).'/vc/recent_posts.php';
// dropcaps
include_once dirname(__FILE__).'/vc/dropcaps.php';
// accordion container
include_once dirname(__FILE__).'/vc/accordion.php';
// accordion item
include_once dirname(__FILE__).'/vc/accordion_item.php';
// tabs container
include_once dirname(__FILE__).'/vc/tabs.php';
// tabs item
include_once dirname(__FILE__).'/vc/tabs_item.php';
// service boxes
include_once dirname(__FILE__).'/vc/services.php';
// quotes
include_once dirname(__FILE__).'/vc/quotes.php';
// list container
include_once dirname(__FILE__).'/vc/lists.php';
// list items
include_once dirname(__FILE__).'/vc/lists_item.php';
// testimonials (grid) container
include_once dirname(__FILE__).'/vc/testimonials.php';
// single testimonial for grid container
include_once dirname(__FILE__).'/vc/testimonial.php';
// testimonials (carousel) container
include_once dirname(__FILE__).'/vc/testimonials_carousel.php';
// single testimonial for carousel container
include_once dirname(__FILE__).'/vc/testimonial_carousel.php';
// team member - standalone
include_once dirname(__FILE__).'/vc/team.php';
// counter
include_once dirname(__FILE__).'/vc/counter.php';


